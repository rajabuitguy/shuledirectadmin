const bigGroupGift = require('./big-group-gift.jpg');
const bigGroupGift2 = require('./bigGroupGift.png');
const groupGift = require('./group-gift.png');
const gift = require('./hanging-gifts.png');
const gold1 = require('./gold1.png');
const silver1 = require('./silver1.png');
const bronze1 = require('./bronze1.png');
const gold2 = require('./gold2.png');
const silver2 = require('./silver2.png');
const bronze2 = require('./bronze2.png');

export {gift, groupGift, bigGroupGift, bigGroupGift2, gold1, gold2, silver1, silver2, bronze1, bronze2}
