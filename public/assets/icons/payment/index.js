const AirtelMoney = require('./Airtelmoney.png');
const Mpesa = require('./Voda-Mpesa.png');
const TigoPesa = require('./tigopesa.png');
const MasterCard = require('./Mastercard.png');

export {AirtelMoney, MasterCard, TigoPesa, Mpesa};
