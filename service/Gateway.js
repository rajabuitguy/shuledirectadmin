
class Gateway {

	constructor() {
		this.data = null
		this.serverError = null
		this.isLoading = true
		this.networkError = null
		this.url = "https://www.gateway.shuledirect.co.tz/"
		// this.testUrl = "https://sandbox.shuledirect.co.tz/requests/data"
		this._url = "https://www.gateway2.shuledirect.co.tz/sd_analytics_api/api/"
		// this.testUrl = "https://gateway2.shuledirect.co.tz/gateway_api/requests/data"
		this.testUrl = "https://sandbox.shuledirect.co.tz/gateway_api/requests/data"
		this.mediaUrl = "https://www.content.shuledirect.co.tz/mediaapi/api/mediaUploads/fileContent"
	}

	async get(path) {
		let data = this.data
		let serverError = this.serverError
		let isLoading = this.isLoading
		let networkError = this.networkError

		try {
			let url = this.url + path
			let response = await fetch(url)
			if (response.ok) {
				this.data = await response.json()
				this.isLoading = false
			}
			else {
				this.serverError = true
			}
			data = this.data
			serverError = this.serverError
			isLoading = this.isLoading

		} catch (error) {
			networkError = true
		}

		return { data, isLoading, serverError, networkError }

	}

	async post(data_) {
		// let url = this.url+path
		let url = this.testUrl
		let data = this.data
		let serverError = this.serverError
		let isLoading = this.isLoading
		let networkError = this.networkError
		try {
			let response = await fetch(url, {
				method: 'POST',
				headers: { "Content-Type": "application/x-www-form-urlencoded" },
				body: JSON.stringify(data_)
			})
			if (response.ok) {
				this.data = await response.json()
				this.isLoading = false
			}
			else {
				this.data = null
				this.serverError = true
			}

			data = this.data
			serverError = this.serverError
			isLoading = this.isLoading

		} catch (error) {
			this.data = null
			networkError = true
		}

		return { data, isLoading, serverError, networkError }

	}

	async postPayment(profileId, role) {
		this.data = null
		let url = `https://www.payment.shuledirect.co.tz/api/account/subscription/info/${profileId}/${role}`
		let data = this.data
		let serverError = this.serverError
		let isLoading = this.isLoading
		let networkError = this.networkError

		try {
			let response = await fetch(url)
			let responseObject = await response.json()
			const { data } = responseObject
			if (data && data.length > 0) {
				this.data = data
				this.isLoading = false
			} else {
				this.data = null
				this.serverError = true
			}
			data = this.data
			serverError = this.serverError
			isLoading = this.isLoading

		}
		catch (err) {
			this.data = null;
			networkError = true

		}
		return { data, isLoading, serverError, networkError }
	}

	async _post(path) {
		let url = this._url
		let data = this.data
		let serverError = this.serverError
		let isLoading = this.isLoading
		let networkError = this.networkError

		try {
			let response = await fetch(url, {
				method: 'POST',
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(data_)
			})

			if (response.ok) {
				this.data = await response.json()
				this.isLoading = false
			}
			else {
				this.data = null
				this.serverError = true
			}

			data = this.data
			serverError = this.serverError
			isLoading = this.isLoading

		} catch (error) {
			this.data = null
			networkError = true
		}

		return { data, isLoading, serverError, networkError }

	}

	async __post(data_, path) {
		let url = this._url + path
		let serverError = this.serverError
		let isLoading = this.isLoading
		let networkError = this.networkError
		let data = this.data
		try {
			let response = await fetch(url, {
				method: 'POST',
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(data_)
			})

			if (response.ok) {
				this.data = await response.json()
				this.isLoading = false
			}
			else {

				this.data = null
				this.serverError = true
			}

			data = this.data
			serverError = this.serverError
			isLoading = this.isLoading

		} catch (error) {
			this.data = null
			networkError = true
		}

		return { data, isLoading, serverError, networkError }

	}

	async ____post(data_) {
		let url = this.mediaUrl
		let serverError = this.serverError
		let isLoading = this.isLoading
		let networkError = this.networkError
		let data = this.data
		try {
			let response = await fetch(url, {
				method: 'POST',
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(data_)
			})

			if (response.ok) {
				this.data = await response.json()
				this.isLoading = false
			}
			else {

				this.data = null
				this.serverError = true
			}

			data = this.data
			serverError = this.serverError
			isLoading = this.isLoading

		} catch (error) {
			this.data = null
			networkError = true
		}

		return { data, isLoading, serverError, networkError }

	}



}

export default new Gateway()
