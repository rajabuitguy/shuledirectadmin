import { Container,Typography } from '@mui/material'
import Link from 'next/link'
import Image from 'next/image'

// Begin: Custom
import PlaySotreLogo from '../../public/assets/logos/google-play.png'
import AppSotreLogo from '../../public/assets/logos/apple-store.png'
import FbLogo from '../../public/assets/logos/fb.png'
import TwitterLogo from '../../public/assets/logos/twitter.png'
import YoutubeLogo from '../../public/assets/logos/youtube.png'

import styles from './styles/Footer.module.scss'
// End: Custom

const FooterPage = ()=> {

	return <div className={styles.footerWrapper}>
			<Container className={styles.footerInnerWrapper}>
			  <div className={styles.menuList}>
			  	  <Typography variant="h6">Study Notes</Typography>
				  <ul>
				  	 <li><Link href="/" passHref>Form 1</Link></li>
				  	 <li><Link href="/" passHref>Form 2</Link></li>
				  	 <li><Link href="/" passHref>Form 3</Link></li>
				  	 <li><Link href="/" passHref>Form 4</Link></li>
				  </ul>
			  </div>

			   <div className={styles.menuList}>
			  	  <Typography variant="h6">Extra Curricular</Typography>
				  <ul>
				  	 <li><Link href="/" passHref>Life Skills Discussions</Link></li>
				  	 <li><Link href="/" passHref>Sports & Games</Link></li>
				  	 <li><Link href="/" passHref>Entertainment</Link></li>
				  </ul>
			  </div>

			  <div className={styles.menuList}>
			  	  <Typography variant="h6">Revisions</Typography>
				  <ul>
				  	 <li><Link href="/" passHref>Past papers</Link></li>
				  	 <li><Link href="/" passHref>Discussions</Link></li>
				  	 <li><Link href="/" passHref>Makini Quizzes</Link></li>
				  	 <li><Link href="/" passHref>Practical Videos</Link></li>
				  	 <li><Link href="/" passHref>Periodic table</Link></li>
				  </ul>
			  </div>

			  <div className={styles.menuList}>
			  	  <Typography variant="h6">More</Typography>
				  <ul>
				  	 <li><Link href="/" passHref>About Shule Direct</Link></li>
				  	 <li><Link href="/" passHref>Conctact</Link></li>
				  	 <li><Link href="/" passHref>Privacy policy</Link></li>
				  	 <li><Link href="/" passHref>Terms of use</Link></li>
				  	 <li><Link href="/" passHref>Testimonials</Link></li>
				  	 <li><Link href="/" passHref>Mkali wa mapindi</Link></li>
				  	 <li><Link href="/" passHref>Makini quizzes terms & conditions</Link></li>
				  	 <li><Link href="/" passHref>Ticha Kidevu shop</Link></li>
				  	 <li><Link href="/" passHref>Elimika LMS</Link></li>
				  </ul>
			  </div>
			  <div className={styles.menuList}>

			  	  <div className={styles.downloadWrapper}>
			  	  	 <Typography variant="h6">Get on mobile</Typography>
			  	  	 <Link href="/" className={styles.imgLink} passHref>
			  	  	 	<Image src={AppSotreLogo} alt="Download app store" />
			  	  	 </Link>
			  	  	 <Link href="/" className={styles.imgLink} passHref>
			  	  	 	<Image src={PlaySotreLogo} alt="Download app store" />
			  	  	 </Link>
			  	  </div>

			  </div>


			</Container>

			<div className={styles.socialMediaWrapper}>
			  	<Typography variant="h6">Follow on social media</Typography>
				<ul>
					<li>
						<Link href="/" passHref>
							<Image src={FbLogo} alt="Facebook" />
						</Link>
					</li>
					<li>
						<Link href="/" passHref>
							<Image src={TwitterLogo} alt="Twitter" />
						</Link>
					</li>
					<li>
						<Link href="/" passHref>
							<Image src={YoutubeLogo} alt="Youtube" />
						</Link>
					</li>
				</ul>
			</div>

		</div>
}
export default FooterPage
