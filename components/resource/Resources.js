import { Typography,Container,Button } from '@mui/material'
import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

// Custom
import styles from './styles/Resources.module.scss'
import Logo from '../../public/assets/logos/sd_logo.png'
import KidsLogo from '../../public/assets/logos/kids_app_logo.png'
// End Custom


/**
 * [description]
 * @param  {[type]} options.size [description]
 * @param  {[type]} options.name [description]
 * @param  {[type]} options.url  [description]
 * @return {[type]}              [description]
 */
export const RoundImage = ({size, name, url})=>{
	return <div className={styles.roundImageContainer} style={{
					"width": `${size}px`,
					"height": `${size}px`,
					"borderRadius": `${size}px`,
					"border":"1px solid hsla(0, 0%, 100%, .15)"
				}}>
				<Image  width={size}
						height={size}
			    		src={url}
						alt={name} />
			</div>
}
//---------------------------------------------------------------------------------


/**
 * [description]
 * @return {[type]} [description]
 */
export const FullScreenLoader = ()=> {
	return <div className={styles.fullScreenLoaderContainer}>
				<Container>
					<div className={styles.imageLoaderContainer}>Loading .....</div>
				</Container>
		   </div>
}
//-------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @return {[type]} [description]
 */
export const FullScreenError = ()=> {
	return <div className={styles.fullScreenErroContainerr}>
				<Container>
					<div className={ styles.errorMessageContainer}>
						<Typography variant="h4">Error while processing data, <br />Check your internet connection</Typography>
					</div>
				</Container>
		   </div>
}
//--------------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @return {[type]} [description]
 */
export const smallLoader = ()=> {
	return <div>
		  <span>Loading...</span>
	</div>
}
//--------------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @return {[type]} [description]
 */
export const SdLogo = ()=> {
	  return <Image src={Logo} alt="Shule Direct Logo" />
}
//-------------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @return {[type]} [description]
 */
export const SdKidsLogo = ()=> {
	  return <Image src={KidsLogo} alt="Shule Direct Logo" />
}
//-------------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @return {[type]} [description]
 */
export const CarouselImage = ()=> {
		return <div className={styles.carouselImageContainer}>
				<div className={styles.overlayContainer}></div>
		</div>
}
//------------------------------------------------------------------------------------------------------------
