import { TextField,Button } from '@mui/material'
import styles from './styles/Forms.module.scss'

const SearchWithSuggestions = ({
			value,
			fieldName,
			label,
			handleFormSearch,
			isSearchResultOpen,
			searchItems,
			handleSearchResultsDataClick,
			form,
			error
		})=> {

	return <div className={styles.searchFormWrapper}>
	       { !error ? (
				<TextField
	                fullWidth
		            onChange = { handleFormSearch(`${fieldName}`) }
		            value= {form.school_name}
		            label={label}
		            type="text"
		            variant="standard" />

	            ):(
	            	<TextField
		                fullWidth
		                error
		                focused
			            onChange = { handleFormSearch(`${fieldName}`) }
			            value= {form.school_name}
			            label={label}
			            type="text"
			            variant="standard" />
	            )}

	            {isSearchResultOpen && (
	            	<div>
		            	{/* {isLoading && (<div>Searching .....</div>)} */}
			            <div className={styles.searchResultWrapper}>
			            	<ul>
			            	    {searchItems && searchItems.map((data,index)=> (
			            			<li key={index}>
			            				<Button onClick= { ()=> handleSearchResultsDataClick(`${data.id}`,`${data.name}`) }>{data.name}, {data.region}- {data.council}</Button>
			            			</li>
			            		))}
			            	</ul>
			            </div>
		            </div>
	            )}
            </div>
}

export default SearchWithSuggestions