import { alpha, styled } from '@mui/material/styles'
import { TextField } from '@mui/material'

export const StyledTextField = styled(TextField)({
      '& label.Mui-focused': {
        color: '#871619',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#871619',
        color: '#871619',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#871619',
          color: '#871619',
        },
        '&:hover fieldset': {
          borderColor: '#871619',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#871619',
        },
      }
})

export const StyledPasswordField = styled(TextField)({
      '& label.Mui-focused': {
        color: '#871619',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#871619',
        color: '#871619',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#871619',
          color: '#871619',
        },
        '&:hover fieldset': {
          borderColor: '#871619',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#871619',
        },
      }
})
