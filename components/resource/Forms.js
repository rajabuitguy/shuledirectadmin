
import React from 'react'

// Begin: Materia UI
import { alpha, styled } from '@mui/material/styles'
import { FormControl,InputLabel,
		 OutlinedInput,
		 Input,
		 InputAdornment,
		 MenuItem,
		 IconButton,TextField,Select } from '@mui/material'
import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'
// End: Materia UI

// Begin: custom
import styles from './styles/Forms.module.scss'
import { StyledTextField } from './styles/Form.overideDefault'
// End: Custom


export const HiddenForm = ({handleOnChange,value,fieldName})=> {
      return <input
                onChange = { handleOnChange(`${fieldName}`) }
                value={value}
                type="hidden" />
}

export const TextForm = ({handleOnChange,value,fieldName,label,error,index})=> {
	return <>
            { !error ? (
              <TextField
                fullWidth
                onChange = { handleOnChange(`${fieldName}`,index) }
                className={styles.textField}
                value={value}
                label={label}
                type="text"
                id="standard-basic"
                variant="standard" />
            ):(
              <TextField
                fullWidth
              	error
              	focused
                onChange = { handleOnChange(`${fieldName}`) }
                className={styles.textField}
                value={value}
                label={label}
                type="text"
                id="standard-basic"
                variant="standard" />
          )}
        </>

}

export const PasswordForm = ({handleOnChange,value,label,error})=> {
	  const [ form, setForm] = React.useState({showPassword: false })

    const handleClickShowPassword = () => {
        setForm({ ...form, showPassword: !form.showPassword })
    }
    const handleMouseDownPassword = (event) => {
        event.preventDefault()
    }

   return <>
   			 		{ !error ? (
			   				<FormControl variant="standard" fullWidth>
				          <InputLabel htmlFor="standard-adornment-password">{label}</InputLabel>
				          <Input
				            id="standard-adornment-password"
				            type={form.showPassword ? 'text' : 'password'}
				            value={value}
				            onChange={handleOnChange('password')}
				            endAdornment={
				              <InputAdornment position="end">
				                <IconButton
				                  aria-label="toggle password visibility"
				                  onClick={handleClickShowPassword}
				                  onMouseDown={handleMouseDownPassword}
				                >
				                  {form.showPassword ? <VisibilityOff /> : <Visibility />}
				                </IconButton>
				              </InputAdornment>
				            }
				          />
			        	</FormControl>
			     ):(
			     			<FormControl variant="standard" error focused fullWidth>
				          <InputLabel htmlFor="standard-adornment-password">{label}</InputLabel>
				          <Input
				            id="standard-adornment-password"
				            type={form.showPassword ? 'text' : 'password'}
				            value={value}
				            onChange={handleOnChange('password')}
				            endAdornment={
				              <InputAdornment position="end">
				                <IconButton
				                  aria-label="toggle password visibility"
				                  onClick={handleClickShowPassword}
				                  onMouseDown={handleMouseDownPassword}
				                >
				                  {form.showPassword ? <VisibilityOff /> : <Visibility />}
				                </IconButton>
				              </InputAdornment>
				            }
				          />
			        	</FormControl>
			     )}
   			</>
}


export const SelectForm = ({handleOnChange,value,options,label,fieldName,error})=> {
	  return <>
	  					{ !error ? (
			        	<FormControl variant="standard" fullWidth>
					        	<InputLabel id="demo-simple-select-standard-label">{label}</InputLabel>
						        <Select
							          labelId="demo-simple-select-standard-label"
							          id="demo-simple-select-standard"
							          value={value}
							          onChange={handleOnChange(`${fieldName}`)}
							          label={label}
						        	>
						          <MenuItem value="">
					          		<em>None</em>
							        </MenuItem>
						          {
						          	options && options.map( data => (
		                  		<MenuItem key={data.id} name={data.name} value={data.id}>{data.name}</MenuItem>
		              			))
						        	}
					        	
					        	</Select>
		      			</FormControl>
			        ):(
			        		<FormControl variant="standard" error fullWidth focused>
					        	<InputLabel id="demo-simple-select-standard-label">{label}</InputLabel>
				        		<Select
						          labelId="demo-simple-select-standard-label"
						          id="demo-simple-select-standard"
						          value={value}
						          onChange={handleOnChange(`${fieldName}`)}
						          label={label}
					        	>
					          <MenuItem value="">
				          		<em>None</em>
						        </MenuItem>
					          {
					          	options && options.map( data => (
	                  		<MenuItem key={data.id} name={data.name} value={data.id}>{data.name}</MenuItem>
	              			))
					        	}
					        	</Select>
				        	</FormControl>
				        )}
			    </>

}

// export const SelectForm = ({handleOnChange,value,options,label,fieldName,error})=> {
// 	  return <>
// 	  					{ !error ? (
// 			        	<FormControl variant="standard" fullWidth>
// 					        	<InputLabel id="demo-simple-select-standard-label">{label}</InputLabel>
// 						        <Select
// 							          labelId="demo-simple-select-standard-label"
// 							          id="demo-simple-select-standard"
// 							          value={value}
// 							          onChange={handleOnChange(`${fieldName}`)}
// 							          label={label}
// 						        	>
// 						          <MenuItem value="">
// 					          		<em>None</em>
// 							        </MenuItem>
// 						          {
// 						          	options && options.map( data => (
// 		                  			<MenuItem
// 										  	key={
// 											  	fieldName.toLowerCase() === "class_name" ?
// 											  	data.classs ? data.classs.id ?
// 											  	data.classs.id:null:null:data.id
// 											}
// 									  	name={
// 												fieldName.toLowerCase() === "class_name" ?
// 												data.classs ? data.classs.roman ?
// 												data.classs.roman:null:null:data.name
// 											  	// data.name
// 											}
// 									  	value={
// 												fieldName.toLowerCase() === "class_name" ?
// 												data.classs ? data.classs.id ?
// 												data.classs.id:null:null:data.id
// 											}
// 									>
// 										{
// 											fieldName.toLowerCase() === "class_name" ?
// 											data.classs ? data.classs.roman ?
// 											data.classs.roman:null:null:data.name
// 										}
// 										{/* {data.name} */}
// 									</MenuItem>
// 		              			))
// 						        	}
// 					        	</Select>
// 		      			</FormControl>
// 			        ):(
// 			        		<FormControl variant="standard" error fullWidth focused>
// 					        	<InputLabel id="demo-simple-select-standard-label">{label}</InputLabel>
// 				        		<Select
// 						          labelId="demo-simple-select-standard-label"
// 						          id="demo-simple-select-standard"
// 						          value={value}
// 						          onChange={handleOnChange(`${fieldName}`)}
// 						          label={label}
// 					        	>
// 					          <MenuItem value="">
// 				          		<em>None</em>
// 						        </MenuItem>
// 					          {
// 					          	options && options.map( data => (
// 	                  		<MenuItem key={data.id} name={data.name} value={data.id}>{data.name}</MenuItem>
// 	              			))
// 					        	}
// 					        	</Select>
// 				        	</FormControl>
// 				        )}
// 			    </>
//
// }
