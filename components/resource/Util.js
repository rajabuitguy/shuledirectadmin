/* eslint-disable react/display-name */
import storage from '../../helper/LocalData'
import publicIp from 'public-ip'
import * as deviceInfo from 'react-device-detect'
import React from 'react'

export const Journey = React.memo(props => {

    const [ipv4,setIpv4] = React.useState("unknown")

    let object = props.pageType ? props.pageType.toLowerCase() === "notes" ? 
                    {
                        className:props.className,
                        subjectId:props.subjectId,
                        topicId:props.topicId,
                        subtopicId:props.subtopicId,
                        conceptId:props.conceptId,
                    }:null:null

    React.useEffect(()=>{

        const caller = async()=>{
            let ipv4 
            try{
                ipv4 = await publicIp.v4()
            }
            catch(err){
                ipv4 = null
            } 
            setIpv4(ipv4)
        }
        caller()
    },[ipv4])

    React.useEffect(()=>{
        if(props.page){
            let journ = {
                data:{
                    page:{
                        id:props.id,
                        key :props.keyName,
                        name:props.page,
                        action:props.action,
                        startTime:new Date(),
                        endTime :props.end ? new Date().getTime():null
                    },
                    syllabus:object,
                    features:[
                        {
                            id :props.id,
                            key :props.keyName,
                            name : props.feature,
                            action :props.action,
                            startTime : new Date().getTime(),
                            endTime : null
                        }
                    ],
                    device:{
                        host:"unknown",
                        os:deviceInfo.osName,
                        buildVersion:deviceInfo.engineVersion,
                        systemVersion:deviceInfo.osVersion,
                        model:deviceInfo.browserName,
                        brand:deviceInfo.browserName,
                        manufacturer:deviceInfo.browserName,
                        deviceId:"unknown",
                        users_mid:"guest",
                        device_type_id:2,
                        userAgent:deviceInfo.getUA,
                        ip_address:ipv4
                    }
                }
            }
            storage.createJourney(journ);
        }
    },[props.page])

    React.useEffect(()=>{
        if(props.feature){
            const featureJourn = {
                id:props.id,
                key:props.keyName,
                name: props.feature,
                action: props.action,
                startTime:new Date().getTime(),
                endTime:props.end ? new Date().getTime():null
            }
            storage.addJourney(featureJourn);
        }
    },[props.feature])

    return null;
})

Journey.displayName = "Journey"

Journey.defaultProps = {
    pageType:null,
    page:null,
    feature:null,
    keyName:null,
    action:null
}



