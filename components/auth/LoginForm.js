import React from 'react'
import Link from 'next/link'
import { useSession, signIn } from 'next-auth/client'
import { Container, Typography, Button, Alert } from '@mui/material'
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew'
import SupervisedUserCircleOutlinedIcon from '@mui/icons-material/SupervisedUserCircleOutlined'
import RemoveCircleOutlineSharpIcon from '@mui/icons-material/RemoveCircleOutlineSharp'
import { GoogleLogin } from 'react-google-login'
import { useRouter } from 'next/router'

// Begin: Custom
import styles from './style/Auth.module.scss'
import { TextForm, PasswordForm, HiddenForm } from '../resource/Forms'
import User from '../../process/User'
import LocalData from '../../helper/LocalData'
import { RoundImage } from '../../components/resource/Resources'
import userImage from '../../public/assets/images/sampleimage.png'
const clientId = "113362832624-ap1f9oipfd5a0kv549a34v0c5tqbsmqv.apps.googleusercontent.com"
import useAuth from '../common/useAuth'
// End: Custom

const LoginForm = (props) => {

	const [session, loading] = useSession()
	const router = useRouter()
	const { user } = useAuth()

	let accountArray = LocalData.findAllLoggedAccounts()
	const [isLoading, setIsLoading] = React.useState(false)
	const [shakeForm, setShakeForm] = React.useState('')
	const [formError, setFormError] = React.useState({ email: false, password: false })
	const [form, setForm] = React.useState({ password: '', email: '', csrfToken: props.csrfToken })
	const [errorAlert, setErrorAlert] = React.useState(false)
	const [initialLoginHeader, setInitialLoginHeader] = React.useState(true)
	const [accountDeleteMode, setAccountDeleteMode] = React.useState({ clicked: false })
	const [loggedAccountsData, setLoggedAccountsData] = React.useState(accountArray)
	const [singleAccountData, setSingleAccountData] = React.useState(false)
	const [singleLoginFormstate, setSingleLoginFormstate] = React.useState(false)

	const handleFormChange = (prop) => (event) => {
		setForm({ ...form, [prop]: event.target.value })
	}

	const userSession = () => {
		return user
	}

	const handleFormClick = async () => {
		setErrorAlert(false)
		setFormError({ email: false, password: false })
		setIsLoading(true)

		if (form.email === "") {
			setFormError({ email: true })
			setIsLoading(false)
			return false
		}
		else if (form.password === "") {
			setFormError({ password: true })
			setIsLoading(false)
			return false
		}

		const email = form.email
		const password = form.password
		const res = await signIn('credentials', {
			email,
			password,
			callbackUrl: `${window.location.origin}/redirect`,
			redirect: false,
		}
		)

		if (res?.error) {
			setErrorAlert(true)
			setFormError({ email: true, password: true })
			setIsLoading(false)
			return false
		}

		if (res.url) {
			let _user = userSession()
			let bool = LocalData.processLoggedAccounts([_user.data])
			router.push(`${res.url}`)
			//   const { data: userArray } = await User._findById(email,'username')
			//   if (userArray && userArray.data.length>0)
			//   {
			//   	    let __userData = userArray.data.filter((data,index) => index === 0)
			// Begin: Create logged in accounts
			// End: Create logged in accounts

			//add user login activity
			// LocalData.processSetUserActivity('LOGINACTIVITY')
			//end User login activity
			// console.log("Host url",window.location.host," - Hostname url",window.location.hostname)
			// return false
			// router.push(`http://${window.location.host}/redirect`)
			//   }
		}
	}

	const hideErrorAlert = () => {
		setErrorAlert(false)
	}

	const handleGoogleLoginAuth = async (res) => {
		let data = res.profileObj
		setForm({ password: data.googleId, email: data.email })
		await handleFormClick()
	}

	/**
	   * [description]
	   * @param  {[type]} userId [description]
	   * @return {[type]}        [description]
	   */
	const handleSingleAccount = (userId = null) => {
		setFormError({ password: false })
		setErrorAlert(false)

		if (userId && typeof userId !== "number") {
			setForm({ email: form.email, password: "" })
		}

		if (userId && typeof userId === "number") {
			let accountArray = LocalData.findLoggedAcctountById(userId)
			setForm({ email: accountArray[0].user_token.token_id, passowrd: "" })
			setSingleAccountData(accountArray)
		}

		setSingleLoginFormstate(!singleLoginFormstate)

	}
	//----------------------------------------------------------------------------------

	/**
	   * [description]
	   * @param  {[type]} id [description]
	   * @return {[type]}    [description]
	   */
	const removeAccounts = (id) => {
		LocalData.removeUserAccount(id)
		let result = LocalData.findAllLoggedAccounts()
		setLoggedAccountsData(result)
	}
	//----------------------------------------------------------------------------------

	/**
	   * [description]
	   * @return {[type]} [description]
	   */
	const showRemoveAccountMode = () => {
		if (accountDeleteMode.clicked === false) {
			setAccountDeleteMode({
				clicked: !accountDeleteMode.clicked
			})
		}
		else if (accountDeleteMode.clicked === true) {
			setAccountDeleteMode({
				clicked: !accountDeleteMode.clicked
			})
		}
	}
	//------------------------------------------------------------------------------------

	/**
	  * [description]
	  * @return {[type]} [description]
	  */
	const showHideLoginForm = () => {
		setForm({ password: "", email: "" })
		if (loggedAccountsData) {
			setInitialLoginHeader(false)
			setLoggedAccountsData(false)
		}
		else if (loggedAccountsData === false) {
			setInitialLoginHeader(true)
			setLoggedAccountsData(accountArray)
		}
	}
	//-------------------------------------------------------------------------------------


	/**
	   * [description]
	   * @param  {[type]} data [description]
	   * @return {[type]}      [description]
	   */
	const accountList = (data) => {
		return <div key={data.id}>
			{!accountDeleteMode.clicked ? (
				<Button className={styles.buttonListWrapper} onClick={() => handleSingleAccount(data.id)}>
					<div className="sd-flex">
						<div>
							<RoundImage size="40" url={userImage} name={data.first_name} />
						</div>
						<div className="sd-flex-column p-10">
							<span className="fullname">{data.first_name} {data.last_name}</span>
							<div><span className="belowtext">{data.username}</span></div>
						</div>
					</div>
				</Button>
			) : (
				<Button className={styles.buttonListWrapper} onClick={() => removeAccounts(data.id)} key={data.id}>
					<div className="sd-flex">
						<div>
							<RoundImage size="40" url={userImage} name={data.first_name} />
						</div>
						<div className="sd-flex-column p-10" style={{ width: "200px" }}>
							<span className="fullname">{data.first_name} {data.last_name}</span>
							<div><span className="belowtext">{data.email}</span></div>
						</div>
						<div>
							<span style={{ fontSize: "13px", color: "red", float: "right" }}>Remove</span>
						</div>
					</div>
				</Button>
			)}
		</div>
	}
	//------------------------------------------------------------------------------------------------------

	/**
   * [description]
   * @return {[type]} [description]
   */
	const loggedInAccount = () => {
		return <div className={styles.sdAccountsWrapper}>

			{!accountDeleteMode.clicked ? (
				<Container>
					<Typography variant="h5">Choose an account to login</Typography>
					<div style={{ fontSize: "15px" }}>These are the accounts previous logged in with this browser</div>
				</Container>
			) : (
				<div className="sd-flex">
					<Button onClick={showRemoveAccountMode}><ArrowBackIosNewIcon /></Button>
					<div className="sd-flex-column">
						<Typography variant="h5">Remove an account</Typography>
						<div>From this browser</div>
					</div>
				</div>
			)}

			<Container>
				<div className="mt-30"></div>
				{loggedAccountsData && loggedAccountsData.map(data => (
					accountList(data)
				))}

				<Button onClick={showHideLoginForm} className={styles.buttonListWrapper}>
					<div className="sd-flex">
						<div style={{ width: "30px" }}>
							<SupervisedUserCircleOutlinedIcon className={styles.authActionButton} />
						</div>
						<div className="sd-flex-column p-10">
							<span className="fullname">Use another account</span>
						</div>
					</div>
				</Button>

				{!accountDeleteMode.clicked && loggedInAccount && (
					<Button className={styles.buttonListWrapper}>
						<div className="sd-flex" onClick={showRemoveAccountMode}>
							<div style={{ width: "30px" }}>
								<RemoveCircleOutlineSharpIcon className={styles.authActionButton} />
							</div>
							<div className="sd-flex-column p-10">
								<span className="fullname">Remove an account</span>
							</div>
						</div>
					</Button>
				)}
			</Container>
		</div>
	}
	//------------------------------------------------------------------------------------------------------

	/**
   * [description]
   * @return {[type]} [description]
   */
	const singleAuthForm = () => {
		return <div noValidate autoComplete="off">
			<div className="mb-30 sd-flex">
				<Button onClick={handleSingleAccount} >
					<ArrowBackIosNewIcon className={styles.cancelButton} />
				</Button>
				<div className='sd-flex-column'>
					<Typography variant="h5">Login to continue</Typography>
				</div>
			</div>

			<div className={styles.sdSingleFormWrapper}>
				<Container>

					{singleAccountData && singleAccountData.map(data => (
						<div className="sd-flex" key={data.id}>
							<div>
								<RoundImage url={userImage} size="80" name={data.first_name} />
							</div>
							<div className="sd-flex-column p-10">
								<Typography variant="h5">Hi, {`${data.first_name}`}</Typography>
								<div><span className="">{data.email}</span></div>
								<HiddenForm
									handleOnChange={handleFormChange}
									value={form.email}
									fieldName="email" />
							</div>
						</div>
					))}

					{errorAlert && (
						<div className={`${styles.alert} mt-10`}>
							<Alert
								variant="outlined"
								severity="error"
								onClose={hideErrorAlert}>Wrong password. Please try again</Alert>
						</div>
					)}
					<div className="mt-10">
						<PasswordForm
							handleOnChange={handleFormChange}
							error={formError.password}
							value={form.password}
							label="Enter your password" />
					</div>

					<div className="mt-30">
						{!isLoading ? (
							<Button
								onClick={handleFormClick}
								className={styles.authActionButton} variant="contained" color="primary">
								Login
							</Button>
						) : (
							<Button
								disabled
								onClick={handleFormClick}
								className={styles.authActionButton} variant="contained" color="primary">
								Loading
							</Button>
						)}
					</div>

				</Container>
			</div>
		</div>
	}
	//---------------------------------------------------------------------------------------------------------

	return <div className={styles.authFormWrapper}>

		{singleLoginFormstate && (
			<div className="">
				{singleAuthForm()}
			</div>
		)}

		{!singleLoginFormstate && (
			<div>
				{loggedAccountsData && loggedInAccount()}
			</div>
		)}

		{!loggedAccountsData &&
			<div>

				{!initialLoginHeader ? (
					<div className="sd-flex">
						<Button onClick={showHideLoginForm}>
							<ArrowBackIosNewIcon className={styles.cancelButton} />
						</Button>
						<div className='sd-flex-column'>
							<Typography variant="h5">Login to continue</Typography>
						</div>
					</div>
				) : (
					<Container><Typography variant="h5">Login to continue</Typography></Container>
				)}

				<Container>
					{errorAlert && (
						<div className={`${styles.alertError} mt-10`}>
							<Alert variant="outlined" severity="error" onClose={hideErrorAlert}>Wrong login credentials. Please try again</Alert>
						</div>
					)}
				</Container>

				<Container>
					<form>

						<div className="mt-30">
							<TextForm
								handleOnChange={handleFormChange}
								value={form.email}
								fieldName="email"
								error={formError.email}
								label="Username or email" />
						</div>

						<div className="mt-20">
							<PasswordForm
								handleOnChange={handleFormChange}
								error={formError.password}
								value={form.password}
								label="Enter your password" />
						</div>

						<div className="mt-30">
							<HiddenForm
								handleOnChange={handleFormChange}
								value={form.csrfToken}
								fieldName="csrfToken" />
							{!isLoading ? (
								<Button
									onClick={handleFormClick}
									className={styles.authActionButton} variant="contained" color="primary">
									Login
								</Button>
							) : (
								<Button
									disabled
									onClick={handleFormClick}
									className={styles.authActionButton} variant="contained" color="primary">
									Loading
								</Button>
							)}
						</div>

						{/* <div className="mt-20"> */}
						{/*   Not registered? <Link href="/auth/register">Create account</Link> */}
						{/* </div> */}

					</form>
					{/* <div className={styles.separatorContainer}> */}
					{/*   <span className={styles.innerSeparator}> */}
					{/*     <span>OR</span> */}
					{/*   </span> */}
					{/* </div> */}
					{/*  <div className="mt-30"> */}
					{/*   <GoogleLogin */}
					{/*         className={styles.googleButton} */}
					{/*         clientId={clientId} */}
					{/*         onSuccess={handleGoogleLoginAuth} */}
					{/*         buttonText="Continue with Google" */}
					{/*         cookiePolicy={'single_host_origin'} */}
					{/*         isSignedIn={false} */}
					{/*     /> */}
					{/* </div> */}
				</Container>
			</div>
		}
	</div>
}
export default LoginForm
