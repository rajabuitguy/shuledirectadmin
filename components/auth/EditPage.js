import { Container,Typography,Button,Grid,Alert } from '@mui/material'
import React from 'react'
import { useRouter } from 'next/router'

// Begin: Custom
import { RoundImage } from '../resource/Resources'
import userImage from '../../public/assets/images/sampleimage.png'
import styles from './style/Auth.module.scss'
import { TextForm,PasswordForm,SelectForm } from '../resource/Forms'
import SearchWithSuggestionsForm from '../resource/SearchWithSuggestionsForm'
import General from '../../process/General'
import User from '../../process/User'
// End: Custom

const EditPage = ({user})=> {
	let timeout = null
    const router = useRouter()

	const [ isLoading,setIsLoading ] = React.useState(false)
	const [ activeGroupName, setActiveGroupName] = React.useState(user.profile_type.name)
	const [ groupArray,setGroupArray ] = React.useState(false)
	const [	errorMessage,setErrorMessage] = React.useState(false)
	const [	classesArray,setClassesArray ] = React.useState(false)
    const [ searchItems, setSearchIterms] = React.useState(null)
    const [ isSearchResultOpen, setIsSearchResultOpen ]  = React.useState(null)
    const [ genderArray, setGenderArray ] = React.useState([
    	 	 { id:"Male",name:"Male" },
			 { id:"Female",name:"Female"},
    ])
    const [ form, setForm] = React.useState({
            id:user.id,
            fname: user.first_name,
            lname:user.last_name,
            email: user.email,
            phone: user.phone,
            gender: user.sex,
            username:user.username,
            source:"web",
            school_name:(user.schools) ? user.schools.name:'',
            school_id:(user.schools) ? user.schools.id:'',
            image:user.image,
            forms_mid:user.forms_mid,
            create_form:false,
            group:user.profile_type.id
        })
    const [ formError, setFormError] = React.useState({
				fname: false,
				lname:false,
				email: false,
				username: false,
				group: false,
				gender:false,
				phone: false,
				password:false,
				class_name:false
			})

	React.useEffect(()=> {
			getClasses()
	},[])

	/**
	 * @param  {[type]}
	 * @return {[type]}
	 */
    const handleFormChange = (prop) => (event) => {
    	setForm({ ...form, [prop]: event.target.value })
	}
	//-------------------------------------------------------------------------------------------------------

	/**
	 * @param  {[type]}
	 * @return {[type]}
	 */
	const handleFormSearch = (prop) => async (event) =>{
        setForm({ ...form, [prop]: event.target.value })
        
        clearTimeout(timeout)
        timeout = setTimeout(async ()=> {
            let searchName = event.target.value
            if (searchName==="")
            {
                setIsSearchResultOpen(false)
                return false
            }
            setIsSearchResultOpen(true)
            let resultArray = await General._searchForSchool(searchName)
            setSearchIterms(resultArray)
    	}, 1000)
    }

    /**
     * @param  {[type]}
     * @param  {[type]}
     * @return {[type]}
     */
    const handleSearchResultsDataClick = (id,name) => {
        setIsSearchResultOpen(false)
        setForm({...form, school_id:id,school_name: name})
    }

	/**
	 * [description]
	 * @param  {[type]} prop) [description]
	 * @return {[type]}       [description]
	 */
	const handleSelectFormChange = (prop) => (event) => {

			if (prop==="group")
			{
				 let defaultGroup = groupArray.filter(data=> data.id=== event.target.value)
				 let groupName = defaultGroup[0].name
				 setActiveGroupName(groupName)
			}

			setForm({ ...form, [prop]: event.target.value })
	}
	//--------------------------------------------------------------------------------

	/**
	 * @return {[type]}
	 */
	const getClasses = async ()=>{
   	 const resultArray = await General.findClasses()
   	 setClassesArray(resultArray)
    }
	//-------------------------------------------------------------------------------------------------------

    
    /**
     * @return {[type]}
     */
    const handleFormClick = async ()=> {
    	setFormError({ school_name:false})
        setIsLoading(true)
        const { data: resultArray } = await User.processRegisterData(form)
        router.push('/redirect')
    }
	//-------------------------------------------------------------------------------------------------------

    return <Container>
    	 <div className={styles.completePageWrapper}>

    	 	    <Typography variant="h4">Edit Profile</Typography>
                <div className="sd-flex mt-20">
                    <div>
                        <RoundImage size={100} name="image" url={userImage}/>
                    </div>
                    <div className="p-20">
                         <Typography variant="h5">{form.fname} {form.lname} </Typography>
                         <div variant="h6">Form one Student</div>
                    </div>
                </div>

               <div className="mt-30">
				    <Grid container spacing={2}>
						<Grid item xs={12} sm={6} md={6} lg={6}>
						<TextForm
								handleOnChange={handleFormChange}
								value={form.fname}
								fieldName="fname"
								error = {formError.fname}
								label="First name" />

						</Grid>
						<Grid item xs={12} sm={6} md={6} lg={6}>
							<TextForm
								handleOnChange={handleFormChange}
								value={form.lname}
								fieldName="lname"
							  error = {formError.lname}
								label="Last name" />
						</Grid>
					</Grid>
				</div>

				<div className="mt-20">
					 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
									<SelectForm
										 handleOnChange = {handleSelectFormChange}
										 value={form.gender}
										 options = {genderArray ? genderArray : null}
										 label= "What is your gender?"
										 error = {formError.gender}
										 fieldName = 'gender' />
							 </Grid>
					 </Grid>
				</div>

				<div className="mt-20">
					 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
									<TextForm
										handleOnChange={handleFormChange}
										value={form.phone}
								    error = {formError.phone}
										fieldName="phone"
										label="Phone" />
					 		</Grid>
					 </Grid>
				</div>

				{activeGroupName !== 'Student' && (
					<div className="mt-20">
						 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
								<TextForm
									handleOnChange={handleFormChange}
									value={form.email}
									fieldName="email"
					        		error = {formError.email}
									label="Email" />
						   </Grid>
						 </Grid>
					</div>
				)}

				{activeGroupName === 'Student' && (
					<>
						<div className="mt-20">
							<Grid container spacing={2}>
									<Grid item xs={12} sm={12} md={12} lg={12}>
										<SelectForm
											handleOnChange = {handleSelectFormChange}
											value={form["forms_mid"]}
											options = { classesArray? classesArray : null }
											label= "Your class?"
											error = {formError.class_name}
											fieldName = 'forms_mid' />
									</Grid>
							</Grid>
						</div>
					</>
				)}
				<div className="mt-20">
					<SearchWithSuggestionsForm 
	                    value={form.school_name}
	                    fieldName="school_name"
	                    label="Add school name"
	                    handleFormSearch = { handleFormSearch }
	                    isSearchResultOpen = { isSearchResultOpen }
	                    searchItems = { searchItems }
	                    form = { form }
	                    error = { formError.school_name }
	                    handleSearchResultsDataClick = { handleSearchResultsDataClick } />
                </div>
                <div className="mt-30">
                    { !isLoading ? (
                        <Button
                        onClick = { handleFormClick }
                        className={styles.authActionButton} variant="contained" color="primary">
                            Save
                        </Button>
                    ) : (
                        <Button
                        disabled
                        onClick = { handleFormClick }
                        className={styles.authActionButton} variant="contained" color="primary">
                            Loading
                        </Button>
                    )}
                </div>

    	 </div>
    </Container>
    
}
export default EditPage