import { Container,Typography,Button } from '@mui/material'
import React from 'react'
import { useRouter } from 'next/router'


// Begin:Custom
import { RoundImage } from '../resource/Resources'
import userImage from '../../public/assets/images/sampleimage.png'
import styles from './style/Auth.module.scss'
import { TextForm,HiddenForm } from '../resource/Forms'
import SearchWithSuggestionsForm from '../resource/SearchWithSuggestionsForm'
import General from '../../process/General'
import User from '../../process/User'
// End Custom

const CompletePage = ({user})=> {
	let timeout = null
    const router = useRouter()

	const [ isLoading,setIsLoading ] = React.useState(false)
    const [ isSearchResultOpen, setIsSearchResultOpen ]  = React.useState(null)
    const [ searchItems, setSearchIterms] = React.useState(null)
    const [ form, setForm] = React.useState({
            id:user.id,
            fname: user.first_name,
            lname:user.last_name,
            email: user.email,
            phone: user.phone,
            gender: user.sex,
            username:user.username,
            source:"web",
            school_name:(user.schools) ? user.schools.name:'',
            school_id:(user.schools) ? user.schools.id:'',
            image:user.image,
            forms_mid:user.forms_mid,
            create_form:false,
            group:user.profile_type.id 
        })
    const [ formError, setFormError] = React.useState({
            school_name: false
        })

    const handleFormChange = (prop) => (event) => {
        setForm({ ...form, [prop]: event.target.value })
    }

    const handleFormClick = async ()=> {
        if (form.school_name==='')
        {
            setFormError({ school_name:true})
            return false
        }
        setFormError({ school_name:false})
        setIsLoading(true)
        const { data: resultArray } = await User.processRegisterData(form)
        router.push('/redirect')
    }

    const handleFormSearch = (prop) => async (event) =>{
        setForm({ ...form, [prop]: event.target.value })
        
        clearTimeout(timeout)
        timeout = setTimeout(async ()=> {
            let searchName = event.target.value
            if (searchName==="")
            {
                setIsSearchResultOpen(false)
                return false
            }
            setIsSearchResultOpen(true)
            let resultArray = await General._searchForSchool(searchName)
            setSearchIterms(resultArray)
            
        }, 1000)
    }

    const handleSearchResultsDataClick = (id,name) => {
        setIsSearchResultOpen(false)
        setForm({...form, school_id:id,school_name: name})
    }

    return <Container>
           <div className={styles.completePageWrapper}>
                <Typography variant="h3">Welcome</Typography>
                <Typography variant="h6">Add your school details to complete registration</Typography>
                <div className="sd-flex mt-20">
                    <div>
                        <RoundImage size={130} name="image" url={userImage}/>
                    </div>
                    <div className="p-20">
                         <Typography variant="h4">{form.fname} {form.lname} </Typography>
                         <div variant="h6">Form one Student</div>
                    </div>
                </div>
                <div className="mt-30">
                <SearchWithSuggestionsForm 
                    value={form.school_name}
                    fieldName="school_name"
                    label="Add school name"
                    handleFormSearch = { handleFormSearch }
                    isSearchResultOpen = { isSearchResultOpen }
                    searchItems = { searchItems }
                    form = { form }
                    error = { formError.school_name }
                    handleSearchResultsDataClick = { handleSearchResultsDataClick } />
                </div>
                <div className="mt-30">
                    { !isLoading ? (
                        <Button
                        onClick = { handleFormClick }
                        className={styles.authActionButton} variant="contained" color="primary">
                            Finish
                        </Button>
                    ) : (
                        <Button
                        disabled
                        onClick = { handleFormClick }
                        className={styles.authActionButton} variant="contained" color="primary">
                            Loading
                        </Button>
                    )}
                </div>
           </div>
    </Container>
}

export default CompletePage