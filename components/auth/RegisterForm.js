import React from 'react'
import { Container,Typography,Button,Grid,Alert } from '@mui/material'
import { GoogleLogin } from 'react-google-login'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import Stack from '@mui/material/Stack'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import Check from '@mui/icons-material/Check'
import SettingsIcon from '@mui/icons-material/Settings'
import GroupAddIcon from '@mui/icons-material/GroupAdd'
import VideoLabelIcon from '@mui/icons-material/VideoLabel'
import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector'
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew'
import Link from 'next/link'
import {connect} from 'react-redux'
import { signIn } from 'next-auth/client'
import { useRouter } from 'next/router'

// Bein: Custom
import styles from './style/Auth.module.scss'
import { TextForm,PasswordForm,SelectForm } from '../resource/Forms'
import User from '../../process/User'
import LocalData from '../../helper/LocalData'
import General from '../../process/General'
// End: Custom

import {calculatingPoints} from '../common/PointCollector'

const QontoConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 10,
    left: 'calc(-50% + 16px)',
    right: 'calc(50% + 16px)',
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: '#784af4',
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: '#784af4',
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    borderColor: theme.palette.mode === 'dark' ? theme.palette.grey[800] : '#eaeaf0',
    borderTopWidth: 3,
    borderRadius: 1,
  },
}));

const QontoStepIconRoot = styled('div')(({ theme, ownerState }) => ({
  color: theme.palette.mode === 'dark' ? theme.palette.grey[700] : '#eaeaf0',
  display: 'flex',
  height: 22,
  alignItems: 'center',
  ...(ownerState.active && {
    color: '#784af4',
  }),
  '& .QontoStepIcon-completedIcon': {
    color: '#784af4',
    zIndex: 1,
    fontSize: 18,
  },
  '& .QontoStepIcon-circle': {
    width: 8,
    height: 8,
    borderRadius: '50%',
    backgroundColor: 'currentColor',
  },
}))

function QontoStepIcon(props) {
  const { active, completed, className } = props;
  return (
    <QontoStepIconRoot ownerState={{ active }} className={className}>
      {completed ? (
        <Check className="QontoStepIcon-completedIcon" />
      ) : (
        <div className="QontoStepIcon-circle" />
      )}
    </QontoStepIconRoot>
  )
}

QontoStepIcon.propTypes = {
  /**
   * Whether this step is active.
   * @default false
   */
  active: PropTypes.bool,
  className: PropTypes.string,
  /**
   * Mark the step as completed. Is passed to child components.
   * @default false
   */
  completed: PropTypes.bool,
}

const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 22,
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    height: 3,
    border: 0,
    backgroundColor:
      theme.palette.mode === 'dark' ? theme.palette.grey[800] : '#eaeaf0',
    borderRadius: 1,
  },
}))

const ColorlibStepIconRoot = styled('div')(({ theme, ownerState }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.grey[700] : '#ccc',
  zIndex: 1,
  color: '#fff',
  width: 50,
  height: 50,
  display: 'flex',
  borderRadius: '50%',
  justifyContent: 'center',
  alignItems: 'center',
  ...(ownerState.active && {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  }),
  ...(ownerState.completed && {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
  }),
}))

function ColorlibStepIcon(props) {
  const { active, completed, className } = props;

  const icons = {
    1: <GroupAddIcon />,
    2: <SettingsIcon />,
    3: <VideoLabelIcon />,
  }

  return (
    <ColorlibStepIconRoot ownerState={{ completed, active }} className={className}>
      {icons[String(props.icon)]}
    </ColorlibStepIconRoot>
  );
}

ColorlibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   * @default false
   */
  active: PropTypes.bool,
  className: PropTypes.string,
  /**
   * Mark the step as completed. Is passed to child components.
   * @default false
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
}


const clientId = "113362832624-ap1f9oipfd5a0kv549a34v0c5tqbsmqv.apps.googleusercontent.com"

/**
 * [getSteps description]
 * @type {Array}
 */
const steps = ['Personal Details', 'Login Details']
//--------------------------------------------------------------------------------

/**
 * [description]
 * @return {[type]} [description]
 */
const getStepContentForm = (
							step,
							groupArray,
							genderArray,
							handleFormChange,
							handleSelectFormChange,
							activeGroupName,
							formError,
							form,
							classes,
						)=> {
	switch (step) {
    case 0:
      return <div>
				<div className="mt-20">
					 <Grid container spacing={2}>
							 <Grid item xs={12} sm={12} md={12} lg={12}>
								<SelectForm
							 		handleOnChange = {handleSelectFormChange}
									value={form.group}
									options = {groupArray}
									label= "What is your role?"
									error = {formError.group}
									fieldName = 'group' />
							 </Grid>
					 </Grid>
				</div>
				<div className="mt-20">
				    <Grid container spacing={2}>
						<Grid item xs={12} sm={6} md={6} lg={6}>
						<TextForm
								handleOnChange={handleFormChange}
								value={form.fname}
								fieldName="fname"
								error = {formError.fname}
								label="First name" />

						</Grid>
						<Grid item xs={12} sm={6} md={6} lg={6}>
							<TextForm
								handleOnChange={handleFormChange}
								value={form.lname}
								fieldName="lname"
							  error = {formError.lname}
								label="Last name" />
						</Grid>
					</Grid>
				</div>

				<div className="mt-20">
					 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
									<SelectForm
										 handleOnChange = {handleSelectFormChange}
										 value={form.gender}
										 options = {genderArray}
										 label= "What is your gender?"
										 error = {formError.gender}
										 fieldName = 'gender' />
							 </Grid>
					 </Grid>
				</div>

			</div>
    case 1:
    return <div>



    		<div className="mt-20">
					 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
									<TextForm
										handleOnChange={handleFormChange}
										value={form.phone}
								    error = {formError.phone}
										fieldName="phone"
										label="Phone" />
					 		</Grid>
					 </Grid>
				</div>

				{activeGroupName !== 'Student' && (
					<div className="mt-20">
						 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
								<TextForm
										handleOnChange={handleFormChange}
										value={form.email}
										fieldName="email"
						        error = {formError.email}
										label="Email" />
						   </Grid>
						 </Grid>
					</div>
				)}

				{activeGroupName === 'Student' && (
					<>
					<div className="mt-20">
						<Grid container spacing={2}>
								<Grid item xs={12} sm={12} md={12} lg={12}>
										<SelectForm
											handleOnChange = {handleSelectFormChange}
											value={form["class_name"]}
											options = {classes}
											label= "Your class?"
											error = {formError.class_name}
											fieldName = 'class_name' />
								</Grid>
						</Grid>
					</div>
					<div className="mt-20">
						 <Grid container spacing={2}>
						 	 <Grid item xs={12} sm={12} md={12} lg={12}>
									<TextForm
											handleOnChange={handleFormChange}
											value={form.username}
											fieldName="username"
							        		error = {formError.username}
											label="Username/Nickname" />
							 </Grid>
						 </Grid>
					</div>
					</>
				)}

				<div className="mt-30">
						<Grid container spacing={2}>
							<Grid item xs={12} sm={12} md={12} lg={12}>
							<PasswordForm
									handleOnChange={handleFormChange}
									error = {formError.password}
									label="Password"
									value={form.password}/>
							</Grid>
						</Grid>
				</div>

			</div>
    default:
      return 'Unknown step'
  }

}
//------------------------------------------------------------------------------------------------------------------


const RegisterForm = (props)=> {
  	const router = useRouter()

  	const [ activeStep, setActiveStep ] = React.useState(0)
  	const [ isLoading, setIsLoading ] = React.useState(false)
  	const [ sdFormstate,setSdFormstate ] = React.useState(false)
		const [ activeGroupName, setActiveGroupName] = React.useState('Student')
		const [ groupArray,setGroupArray ] = React.useState(false)
		const [ regHeading,setRegHeading ] = React.useState('Create an account')
		const [ serverError,setServerError ] = React.useState(false)
		const [ form, setForm] = React.useState({
						fname: '',
						lname:'',
						email: '',
						phone: '',
						group: '1',
						gender: '',
						password_form:true,
						password: '',
						username:'',
						source:"web",
						school_name:'',
						region:'',
						country:'',
						occupation:'',
						class_name:'',
						create_form:true 
					})
  	const [ formError, setFormError] = React.useState({
					fname: false,
					lname:false,
					email: false,
					username: false,
					group: false,
					gender:false,
					phone: false,
					password:false,
					class_name:false
				})
		const [errorMessage,setErrorMessage] = React.useState(false)
		const [classesArray,setClassesArray ] = React.useState(false)


  	/**
	 * [description]
	 * @return {[type]} [description]
	 */

  	const handleNext = async () => {
  			setErrorMessage(false)
				if (form.group==="")
				{
					 setFormError({group:true})
						return false
				}

				if (form.fname==="")
				{
					  setFormError({fname:true})
						return false
				}

				if (form.lname==="")
				{
					setFormError({lname:true})
					return false
				}

				if (form.gender==="")
				{
					setFormError({gender:true})
					return false
				}

				//got to the next step
				if (activeStep<1)
				{
					setActiveStep((prevActiveStep) => prevActiveStep + 1)
					return false
				}

				if (activeStep===1)
				{

					if (form.phone==="")
					{
						setFormError({phone:true})
						return false
					}

					if (activeGroupName !=="Student" && form.email==="")
					{
						setFormError({email:true})
						return false
					}

					if (activeGroupName === "Student" && form.username==="")
					{
						setFormError({username:true})
						return false
					}

					if (activeGroupName === "Student" && form.class_name === ""){
						setFormError({class_name:true})
						return false
					}

					if (form.password==="")
					{
						setFormError({password:true})
						return false
					}

					setIsLoading(true)
					let { data, serverError } = await User.processRegisterData(form,activeGroupName)
					setServerError(serverError)
					if (serverError)
					{
							return false
					}

					//Begin: Process user session
					if (data && data.data.length>0)
					{
						 //Begin: creating user session
						 	const email = (activeGroupName==="Student") ? form.username : form.email
	    				const password = form.password
						  const res = await signIn('credentials',{
					        email,
					        password,
					        callbackUrl: `${window.location.origin}/redirect` ,
					        redirect: false,
					      }
					    )
						let object = new Object()
						object.userId = data.data.id ?? null;
						object.featureId = "registration"
						object.features = "registration02"
						// object.directionId = 1
						// object.directionName = "register"
						calculatingPoints(object)
						//Begin: creating user session

						// End: Create login accounts
						 LocalData.processLoggedAccounts(data.data)
						// End: Create login accounts

						router.push(res.url)

					}else{
							if (activeGroupName === "Student" && form.username==="")
							{
								setErrorMessage("The username already taken please try another")
					     setIsLoading(false)
								setFormError({username:true})
								return false
							}
							else {
								setErrorMessage("The email address already registered please try another")
								setFormError({email:true})
					      setIsLoading(false)
								return false
							}

						return false
					}
					//End: Process user session

					// window.location.reload()
				}
  	}
  	//-----------------------------------------------------------------------------

  	const hideErrorAlert = () => {
	    setErrorMessage(false)
		}
  /**
	   * [description]
	   * @return {[type]} [description]
	   */
	  const handleBack = () => {
	    setActiveStep((prevActiveStep) => prevActiveStep - 1)
	  }
	//------------------------------------------------------------------------------

	/**
	 * [description]
	 * @return {[type]} [description]
	 */
	const handleSdAccountForm = ()=> {
		setActiveStep(0)
		setSdFormstate(!sdFormstate)
	}
	//------------------------------------------------------------------------------

	/**
	 * [description]
	 * @param  {[type]} prop) [description]
	 * @return {[type]}       [description]
	 */
	const handleSelectFormChange = (prop) => (event) => {

			if (prop==="group")
			{
				 let defaultGroup = groupArray.filter(data=> data.id=== event.target.value)
				 let groupName = defaultGroup[0].name
				 setActiveGroupName(groupName)
			}

			setForm({ ...form, [prop]: event.target.value })
	}
	//--------------------------------------------------------------------------------

	/**
	 * [description]
	 * @param  {[type]} prop) [description]
	 * @return {[type]}       [description]
	 */
	const handleFormChange = (prop) => (event) => {
    	setForm({ ...form, [prop]: event.target.value })
	}
	//--------------------------------------------------------------------------------

	/**
	 * [genderArray description]
	 * @type {Array}
	 */
	const genderArray = [
		 { id:"Male",name:"Male" },
		 { id:"Female",name:"Female"},
	]

	/**
    * [description]
    * @return {[type]} [description]
    */
    const getClasses = async ()=>{
   	 const resultArray = await General.findClasses()
   	 setClassesArray(resultArray)
    }
    //-----------------------------------------------------------------------------------------------------------

	/**
	 * [description]
	 * @return {[type]} [description]
	 */
	const findUserGroups = async ()=> {
			const {data} = await User.findAllGroups()
			if (!data)
			{
				return false
			}

			let dataArray = data.data.filter(val=> val.name!=='Admin' && val.name!=='Staff')
			setGroupArray(dataArray)
	}
	//--------------------------------------------------------------------------------

	React.useEffect(()=> {
			findUserGroups()
			getClasses()
	},[])

	return <div className={styles.authFormWrapper}>

		{ sdFormstate && (
		  <div>
				<div className="mb-30 sd-flex">
				  	<div>
						<Button onClick={handleSdAccountForm}>
							<ArrowBackIosNewIcon className={styles.backButton}/>
						</Button>
					</div>
					<div className='sd-flex-column'>
						<Typography variant="h5">{regHeading}</Typography>
					</div>
				</div>
		       	<Stepper alternativeLabel activeStep={1} connector={<ColorlibConnector />}>
			        {steps.map((label) => (
			          <Step key={label}>
			            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
			          </Step>
			        ))}
		        </Stepper>

	        	<Container>
				        {errorMessage && (
					    		<div className={`${styles.alert} mt-10`}>
					             <Alert
					                variant="outlined"
					                severity="error"
					                onClose={ hideErrorAlert }> { errorMessage }</Alert>
					         </div>
					       )}

				         <div className="">
												{ getStepContentForm(
																	activeStep,
																	groupArray,
																	genderArray,
																	handleFormChange,
																	handleSelectFormChange,
																	activeGroupName,
																	formError,
																	form,
																	classesArray
																)
												}
								</div>
		            <div className={
											!errorMessage ?
												styles.registerFormNextButtonWrapper:
												styles.errorContainer
											}
										>
						  			<div style={{width:"100%"}}>
						  				<div style={{float:"right"}}>
							  				<Button disabled={activeStep === 0} onClick={handleBack}>Previous</Button>
	              				<Button
		                			variant="contained"
		                			color="primary"
		                			onClick={ handleNext }
		                			className=""
	              				>
	                				{activeStep === steps.length - 1 ? isLoading ? 'sending' : 'Register' : 'Next'}
	              				</Button>
											</div>
										</div>
		            </div>
	          	</Container>
           </div>
      	)}

		{!sdFormstate && (
			<div>
				<Container>
		        <div className="">
						<Typography variant="h5">Register</Typography>
							<div className="mt-20">
								Do have an account? <Link href="/auth/login">Login here</Link>
							</div>
		          <div className="mt-20">
								<Button onClick={handleSdAccountForm}
										className={styles.authActionButton}
										variant="contained"
										color="primary">Create an account</Button>
							</div>
							<div className={styles.separatorContainer}>
								<span className={styles.innerSeparator}>
									<span>OR</span>
								</span>
							</div>

						<div className="mt-30">
							<GoogleLogin
			                  className={styles.googleButton}
			                  clientId={clientId}
			                  buttonText="Continue with Google"
			                  cookiePolicy={'single_host_origin'}
			                  isSignedIn={false}
			              />
						</div>

					</div>
				</Container>
				<div className={styles.registerFormButtonWrapper}>

				</div>
			</div>

		)}

	</div>
}

function mapStateToProps(state){

	return{
		classes:state.classes
	}
}

export default connect(mapStateToProps)(RegisterForm)
