import { Divider } from '@mui/material'
import SkeletonElement from './SkeletonElement'
import Shimmer from './Shimmer'
import styles from './styles/Skeleton.module.scss'


/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const ProfileSkeleton  = ({ theme,height,width,padding  })=>{
	const thmeClass = theme || styles.light

	return <div className={ `${styles.skeletonWrapper} ${thmeClass}`} 
		style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
		 <div className="sd-flex">
		 		<div className="">
		 			<SkeletonElement width="90px" height="90px" borderRadius="90px"/>
		 		</div>
		 		<div className="sd-flex-column p-10 mt-20">
		 			<SkeletonElement width="20%" height="12px" />
		 			<SkeletonElement width="30%" height="12px" />
		 			<SkeletonElement width="60%" height="30px" />
		 		</div>
		 </div>
		 
		 <Shimmer />
	</div>
}
//----------------------------------------------------------------------------------------


/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const BarGraphSkeleton = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light

	return <div className={`${styles.skeletonWrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
			<div className={styles.barGraphSkeleton}>
				<ul className="">
				 	<li><SkeletonElement width="30px" height="200px" /></li>
				 	<li><SkeletonElement width="30px" height="150px" /></li>
				 	<li><SkeletonElement width="30px" height="300px" /></li>
				 	<li><SkeletonElement width="30px" height="250px" /></li>
				 	<li><SkeletonElement width="30px" height="200px" /></li>
				 	<li><SkeletonElement width="30px" height="90px" /></li>
				</ul>
			</div>
			<Shimmer />
	</div>
}
//----------------------------------------------------------------------------------


/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const UserSummaryCardSkeleton = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light

	return <div className={ `${syles.skeletonWrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
		<div className={styles.userSummaryCardSkeleton}>
			<center>
				<div className="p-10">
					<SkeletonElement width="100px" height="15px"/>
				</div>
				<div className="p-10 mb-30 mt-20">
					<SkeletonElement width="90px" height="90px" borderRadius="90px"/>
				</div>

				{[1,2].map(n => (
					<div className="sd-flex p-10" key={n}>
						 <div className="">
							<SkeletonElement width="100px" height="15px"/>
						 </div>
						 <div className="sd-flex-column">
							<SkeletonElement width="100px" height="15px"/>
						 </div>
					</div>
				))}

			</center>

		</div>
	    <Shimmer />
	</div>
}
//----------------------------------------------------------------------------------------


/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const PieChartSkeleton = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonWrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
		<div className="pie-chart-skeleton">
			<center>
				<div className="p-10">
					<SkeletonElement width="100px" height="15px"/>
				</div>
				<div className="p-10 mb-30 mt-20">
					<SkeletonElement width="200px" height="200px" borderRadius="200px"/>
				</div>
			</center>
		</div>
	    <Shimmer />
	</div>
}
//-------------------------------------------------------------------------------------


/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const QuizAttemptsSummaryCardSkeleton = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonWrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
		<div className="sd-flex p-10">
            <div>
				<SkeletonElement width="100px" height="100px" borderRadius="100px"/>
            </div>
            <div className="sd-flex-column p-10">
				<SkeletonElement width="100px" height="15px"/>
                <Divider />
                <div className="mt-10"></div>
				<SkeletonElement width="100px" height="15px"/>
            </div>
        </div>
	    <Shimmer />
	</div>

}
//------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const QuizAttemptsListSkeleton = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light
	return <div className={ `${skeleton-wrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
		<div className="sd-flex p-10">
            <div className="sd-flex-column p-10">
				<SkeletonElement width="150px" height="10px"  />
				<SkeletonElement width="100%" height="50px"  />
            </div>
        </div>
	    <Shimmer />
	</div>
}
//-------------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const ChildListSkeleton = ({ theme,height,width,padding })=> {

	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonWrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
        <center>
			<SkeletonElement width="90px" height="90px" borderRadius="90px" className="mt-20" />
			<SkeletonElement width="100px" height="10px"  />
        </center>
	    <Shimmer />
	</div>

}


export const NotesSkeleton = ({theme,height,width,padding}) =>{
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonwrapper} ${thmeClass}`} style={
		{
			width: `${width}`,
			height: `${height}`,
			padding:`${padding}`
		}
   }>
	   <center>
		   {
			   [...Array(5)].map((item,k)=>
			   		<div key = {k}>
						<div className = {styles.notesShimer}></div>
					</div>
				)
		   }
	   		
	   </center>
   </div>
}

export const SubjectDetailsSkeleton = (theme)=>{
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonwrapper} ${thmeClass}`}>
	   <center>
		 	<div>
				<span className = {styles.imageContainerSkeleton}></span>
				<span className = {styles.imageSkeletonLoader}></span>
			</div>
		
	   		
	   </center>
   </div>
}

export const SubjectDetailsContents = ()=>{
	return(
		<div  className = {styles.subjectDetailsContents}></div>
	)
}

export const SyllabusContents = (props)=>{
	return(
		<div style = {{marginLeft:props.left}} key = {props.index}>
			<div 
				className = {styles.syllabusContents} 
				style = {{width:props.width,height:props.height}}
			>
			</div>
		</div>
	)
}

SyllabusContents.defaultProps = {
	left:0,
	index:0,
	width:"80%",
	height:40
}

export const SubjectSkeleton = ({theme,height,width,padding}) =>{
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonwrapper} ${thmeClass}`} style={
		{
			width: `${width}`,
			height: `${height}`,
			padding:`${padding}`
		}
   }>
	   <center>
		  <SkeletonElement width="100%" height="50px" />
			<div className = {styles.notesShimer}></div>
		</center>
	   {/* <Shimmer /> */}
   </div>
}



//-------------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const HorizontalBarSkaleton = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonwrapper} ${thmeClass}`} style={
			 {
			 	width: `${width}`,
			 	height: `${height}`,
			 	padding:`${padding}`
			 }
		}>
		<SkeletonElement width="40%" height="20px" />
	    <Shimmer />
	</div>
}
//--------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const QuestionsAttemptsListSkeleton  = ({ theme,height,width,padding })=> {
	const thmeClass = theme || styles.light
	return <div className={ `${styles.skeletonWrapper} ${thmeClass}`} style={
		 {
		 	width: `${width}`,
		 	height: `${height}`,
		 	padding:`${padding}`
		 }
	}>
     	<div className="sd-flex">
          <div className="question-number"></div>
          <div className="sd-flex-column">
              <SkeletonElement width="100%" height="20px" />
              <div style={{ marginLeft:"20px"}} className="mt-10">
                  {[1,2,3].map((n,k) => (
									  <div key = {`${k}` + "skeleton"}>
											<SkeletonElement width="70%" height="20px" />
									  </div>
                  ))}
              </div>
          </div>
     	</div>
	    <Shimmer />
	</div>
}
//-------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const MediaGalleryListSkeleton = ({ theme,height,width,padding })=> {
	  const thmeClass = theme || styles.light
		return <div className={ `${styles.skeletonwrapper} ${thmeClass}`} style={
				 {
				 	width: `${width}`,
				 	height: `${height}`,
				 	padding:`${padding}`
				 }
			}>
			{[1,2,3,4,5,6].map((n) => (
			  <div className={styles.mediaGalleryListWrapprt} key={n}>
					<SkeletonElement width="100%" height="100px" />
			  </div>
			))}
	    <Shimmer />
	</div>
}
//-------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const SubjectsListSkeletonLoader = ({ theme,height,width,padding })=> {
	 const thmeClass = theme || styles.light
		return <div className={ `${styles.skeletonwrapper} ${thmeClass}`} style={
				 {
				 	width: `${width}`,
				 	height: `${height}`,
				 	padding:`${padding}`
				 }
			}>
			{[1,2,3,4].map((n) => (
			  <div className={styles.subjectsListWrapper} key={n}>
					<SkeletonElement width="100%" height="150px" />
			  </div>
			))}
	    <Shimmer />
		</div>
}
//-------------------------------------------------------------------------------------------------------------

/**
 * [description]
 * @param  {[type]} options.theme   [description]
 * @param  {[type]} options.height  [description]
 * @param  {[type]} options.width   [description]
 * @param  {[type]} options.padding [description]
 * @return {[type]}                 [description]
 */
export const PrimaryTopicsLoader = ({ theme,height,width,padding })=> {
   const thmeClass = theme || styles.light
		return <div className={ `${styles.skeletonwrapper} ${thmeClass}`} style={
				 {
				 	width: `${width}`,
				 	height: `${height}`,
				 	padding:`${padding}`
				 }
			}>
			{[1,2,3,4].map((n) => (
					<SkeletonElement width="100%" height="40px" />
			))}
	    <Shimmer />
		</div>
}