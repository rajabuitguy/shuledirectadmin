const SkeletonElement = ({ width,height,borderRadius })=> {
	 borderRadius = (typeof borderRadius==='string') ? borderRadius : '4px';
	return <div style={
		{
			width:`${width}`,
			height:`${height}`,
			borderRadius:`${borderRadius}`,
			backgroundColor:`#ddd`,
			margin:`10px 0px`
		}
	}></div>

}

export default SkeletonElement
