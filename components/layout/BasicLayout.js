
import BasicHeader from '../header/basic/BasicHeader'
import FooterPage from '../footer/FooterPage'
import styles from './styles/Layout.module.scss'
import { useWindowScroll } from 'react-use'
import React from 'react'

const BasicLayout = ({ children, transparent,backgroundColor,disableFooter})=> {
    let transparentStatus = (transparent) ? "transparent-header" : ""
    const { y: pageYOffset } = useWindowScroll()
    const [ transparent_,setTransparent_ ] =  React.useState(transparentStatus)



    React.useEffect(()=> {

        if (!transparent)
        {
            setTransparent_("")
            return false
        }

        if (pageYOffset > 100)
        {
            setTransparent_("")
        }
        else{
            setTransparent_(transparentStatus)
        }
    },[pageYOffset,transparent])



    return  <div
                className={`basic-layout ${ transparent_ } ${styles.basicLayoutRoot}`}
                // style = {{backgroundColor:backgroundColor ?? "transparent"}}
            >
                <BasicHeader />
                <main className={styles.mainContent}>
                     <div className={styles.layoutToolbar} />
  	   			         { children }
                </main>

              {!disableFooter && (
                  <FooterPage />
              )}

   		    </div>
}

export default BasicLayout
