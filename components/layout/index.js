import { useRouter } from 'next/router';
import BasicLayout from './BasicLayout'
import AuthLayout from './AuthLayout'
import SecondaryAdminLayout from './SecondaryAdminLayout'
import PrimaryAdminLayout from './PrimaryAdminLayout'
import OtherLayout from './OtherLayout'

const Layout = ({children})=>{

	const router = useRouter()
	const pathArray = router.pathname.split('/')
	if (pathArray[0]==="" && pathArray[1]===""){
		return <AuthLayout>{children}</AuthLayout>
	}
	else if ((pathArray.includes('auth') && pathArray.includes('login')) || (pathArray.includes('auth') && pathArray.includes('register')) )
	{
		return <AuthLayout>{children}</AuthLayout>
	}
	else if ((pathArray.includes('auth') && pathArray.includes('edit')))
	{
		return <OtherLayout showCloseIcon={true}>{children}</OtherLayout>
	}
	else if ((pathArray.includes('auth') && pathArray.includes('complete')) ){
		return <OtherLayout showCloseIcon={false}>{children}</OtherLayout>
	}  
	else if (pathArray.includes('admin') || pathArray.includes('secondary'))
	{
		return <SecondaryAdminLayout>{children}</SecondaryAdminLayout>
	}
	else if (pathArray.includes('primary')){
		return <PrimaryAdminLayout>{children}</PrimaryAdminLayout>
	}
	else{
		return <SecondaryAdminLayout>{children}</SecondaryAdminLayout>
	}


}

export { Layout }
