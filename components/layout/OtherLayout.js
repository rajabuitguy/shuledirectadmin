import OtherHeader from '../header/other/OtherHeader'
import styles from './styles/Layout.module.scss'
import { Container } from '@mui/material'
import { CarouselImage } from '../resource/Resources'

const AuthLayout = ({ children,showCloseIcon })=> {

    return  <div className={ styles.otherRootLayout }>
	                <OtherHeader showCloseIcon = {showCloseIcon}/>
	                <main className={styles.otherMainContent}>
	  	   			  	{ children }
	                </main>
				<CarouselImage />
   		    </div>
}
export default AuthLayout
