
import AdminHeader from '../header/admin/PrimaryAdminHeader'
import styles from './styles/Layout.module.scss'
import { Container } from '@mui/material'

const PrimaryAdminLayout = ({ children })=> {
    return  <div className={`primary-admin-header ${styles.layoutRoot}`}>
                <AdminHeader />
                <main className={styles.mainContent}>
                     <div className={styles.layoutToolbar} />
                     { children }
                </main>
   		    </div>
}
export default PrimaryAdminLayout
