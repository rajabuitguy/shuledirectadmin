
import SecondaryAdminHeader from '../header/admin/SecondaryAdminHeader'
import styles from './styles/Layout.module.scss'
import { Container } from '@mui/material'

const SecondaryAdminLayout = ({ children })=> {
    return  <div className={`secondary-admin-header ${styles.layoutRoot}`}>
                <SecondaryAdminHeader />
                <main className={styles.mainContent}>
                     <div className={styles.layoutToolbar} />
                     <Container>{ children }</Container>
                </main>
   		    </div>
}
export default SecondaryAdminLayout
