import AuthHeader from '../header/auth/AuthHeader'
import styles from './styles/Layout.module.scss'
import { Container } from '@mui/material'
import { CarouselImage } from '../resource/Resources'

const AuthLayout = ({ children })=> {

    return  <div className={ styles.authRootLayoutWrapper }>
    			<div className={ styles.authLayoutWrapper }>
					<Container>
		                <AuthHeader />
		                <main className={styles.mainContent}>
		  	   			  	{ children }
		                </main>
					</Container>
					<CarouselImage />
   		    	</div>
   		    </div>
}
export default AuthLayout
