import React from 'react'
import { Typography } from '@mui/material'
import styles from './styles/GalleryForm.module.scss'
import MultipleFileUploadField from './MultipleFileUploadField'


const GalleryFormPage = ()=> {

    return <div>
    	<MultipleFileUploadField/>
    </div>

}

export default GalleryFormPage