import React from 'react'
import { Typography,Button } from '@mui/material'
import Box from '@mui/material/Box'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import PropTypes from 'prop-types';
import Session from 'react-session-api'
import { useDispatch, useSelector } from 'react-redux'

// Custom
import style from './styles/GalleryForm.module.scss'
import MultipleFileUploadField from './MultipleFileUploadField'
import MediaGallery from './MediaGallery'
import {
				setDetailsData,
				setImageAudioContent,
				clearImageAudioCoontent,
				addNewImageAudioForm,
				clearVideoContent,
				updateAudioContentData,
				updateAudioCoverData,
				updateVideoContentData,
				updateContentDetailsCoverImage,
				updateVideoContentCover
			} from '../../redux/reducers/primary/LearningContentSlice'
import {
		  	updateNumberRangeCover,
		  	updateGameSettingMedia,
		  	updateResourceStepsQuestionsBlock,
		  	updateResourceStepsAnswerBlock,
		  	clearNumberRangeForm,
		  	clearImageResourceForm,
		  	setNumberRangeForm,
		  	updateGamesDetailsCoverImage,
		  	updateResourceStepsAudioBlock,
		  	updateResourceStepsSettings,
			updateResourceOptionProperties,
			setSaveResourceProperties
} from '../../redux/reducers/primary/GameResourcesSlice'

import { 
				showMediaGallery,
				activeSegmentToUploadMedia } from '../../redux/reducers/MediaGallerySlice'
// End custom


const TabPanel = (props) => {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
}

const a11yProps = (index)=> {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const Gallery = ({handleShowGallery})=> {

	const dispatch = useDispatch()
	const detailsData = useSelector(state=> state.learning_content.details_data)
	const videoContent = useSelector(state => state.learning_content.video_content)
	const imageAudiContent = useSelector(state=> state.learning_content.image_audio_content)

	//
	const { show_media_gallery:showMediaGalleryStatus,
					active_segment_to_upload_media:activeSegmentToUpload } = useSelector(state=> state.media_gallery)
	const [ value, setValue] = React.useState(0)
	const [ selectedMedia, setSelectedMedia ] = React.useState(false)
	const [ showSaveBotton, setShowSaveBotton ] = React.useState(false)

	const handleChange = (event, newValue) => {
	    setValue(newValue)
	}

	const handleSelectMedia = (mediaData)=> {
		  setSelectedMedia(mediaData)
		  setShowSaveBotton(true)
	}

	const handleSaveSelectedFile = ()=> {
console.log("selectedMedia",selectedMedia)
		    if (activeSegmentToUpload.source_name==="learning_content_details_cover_upload"){
          	dispatch(updateContentDetailsCoverImage({
							index:activeSegmentToUpload.index,media:selectedMedia,active_status:activeSegmentToUpload.active_status
						}))
        }else if (activeSegmentToUpload.source_name==="learning_content_video_file_upload"){
        	  dispatch(updateVideoContentData({
							index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="learning_content_imageaudio_cover_upload"){
        		dispatch(updateAudioCoverData({
								index:activeSegmentToUpload.index,media:selectedMedia,active_status:activeSegmentToUpload.active_status
						}))
		}else if(activeSegmentToUpload.source_name==="learning_content_video_cover_upload"){
			dispatch(updateVideoContentCover({
				index:activeSegmentToUpload.index,
				media:selectedMedia,
				active_status:activeSegmentToUpload.active_status
			}))

        }else if (activeSegmentToUpload.source_name==="learning_content_audio_file_upload_button"){
        	 dispatch(updateAudioContentData({
								index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="game_resource_details_cover_upload"){
        		dispatch(updateGamesDetailsCoverImage({
								index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="game_resource_step_question_resource_upload"){
        		dispatch(updateResourceStepsQuestionsBlock({
								index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="game_resource_step_answer_resource_upload"){
        		dispatch(updateResourceStepsAnswerBlock({
								index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="game_resource_number_range_form_cover_upload"){
        		dispatch(updateNumberRangeCover({
								index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="game_number_resource_media_settings_upload"){
        	  dispatch(updateGameSettingMedia({
								index:activeSegmentToUpload.index,media:selectedMedia
						}))
        }else if (activeSegmentToUpload.source_name==="game_resource_step_audio_resource_upload"){
        	   dispatch(updateResourceStepsAudioBlock({
								index:activeSegmentToUpload.index,media:selectedMedia
						 }))
        }else if(activeSegmentToUpload.source_name==="game_resource_step_block_media_setting_upload"){
        		  dispatch(updateResourceStepsSettings({
								index:activeSegmentToUpload.index,media:selectedMedia
						 }))
        }else if (activeSegmentToUpload.source_name==="properties_answer_resource_upload"){
			dispatch(updateResourceOptionProperties({
				index:activeSegmentToUpload.index,media:selectedMedia
		 	}))
			dispatch(setSaveResourceProperties())
		}else if (activeSegmentToUpload.source_name==="properties_audio_resource_upload"){
			dispatch(updateResourceOptionProperties({
				index:activeSegmentToUpload.index,
				media:selectedMedia
		 	}))
			dispatch(setSaveResourceProperties())
		}

		   dispatch(showMediaGallery())
	}
  
	return <div className={style.galleryModalWapprt}>
		 <div className={style.galleryFormWrapper}>

		 	<div className={style.galleryFormHeader}>
				<div className={style.headerUpperWrapper}>
					 <div className={`${style.headerUpperLeft}`}>
					 	 <Typography variant="h5"><b>Files Gallery</b></Typography>
					 </div>
					 <div className={`${style.headerCenterWrapper}`}>
					 	<Box>
					      <Tabs value={value} onChange={handleChange} centered>
					        <Tab label="Choose File" {...a11yProps(0)} />
					        <Tab label="Upload New File" {...a11yProps(1)} />
					      </Tabs>
					    </Box>
					 </div>
					 <div className={`${style.headerUpperRight}`}>
					 	<button className="cancel-button"  onClick = { () => handleShowGallery('cancel_modal') }>X</button>
					 </div>
				</div>
			</div>
			<div className={style.galleryFormBody}>
				  <TabPanel value={value} index={0}>
			          Media gallery
			          <MediaGallery selectedMedia = { selectedMedia } handleSelectMedia = {handleSelectMedia} />
			      </TabPanel>
			      <TabPanel value={value} index={1}>
			          Upload new
			          <MultipleFileUploadField />
			      </TabPanel>
			</div>
			<div className={style.galleryFormBottom}>
			   	{showSaveBotton && (
						<Button variant="contained" style={{ textTransform:"capitalize" }} onClick = { handleSaveSelectedFile } >Save file</Button>
					)}
			</div>

		 </div>
	</div>
}

export default Gallery
