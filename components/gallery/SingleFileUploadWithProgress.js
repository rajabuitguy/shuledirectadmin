import React from 'react'
import LinearProgress from '@mui/material/LinearProgress'
import style from './styles/GalleryForm.module.scss'
import GalleryProcess from '../../process/GalleryProcess'
import useAuth from '../common/useAuth'

const SingleFileUploadWithProgress = (props)=> {
    const reader = new FileReader()
	const [ progress, setProgress ] = React.useState(0)
	const { user } = useAuth()
	const userId = user.isLoggedIn ? user.data.id: 0

	React.useEffect(()=> {
		const upload = async ()=>{
			const uplodedData = await uploadFileToServer(props.file,setProgress)
		}
		upload()
	},[])

	const uploadFileToServer = (file,onPoregress)=> {
		// const url = 'https://api.cloudinary.com/v1_1/demo/image/upload'
		// const keyPreset = 'docs_upload_example_us_preset'
		
		const url = "https://content.shuledirect.co.tz/mediaapi/api/mediaUploads/file"
		return new Promise((res, rej)=> {

			const xhr = new XMLHttpRequest()
			xhr.open('POST', url)
			// xhr.setRequestHeader('Content-type', 'multipart/form-data')

			xhr.onload = ()=> {
				const response = JSON.parse(xhr.responseText)
				res(response)
			}

			xhr.onerror = (evt) => rej(evt)

			xhr.upload.onprogress = (event)=> {
				if (event.lengthComputable){
					const percentage = Math.round((event.loaded/event.total)*100)
					onPoregress(percentage)
				}
			} 

			const formData = new FormData()
			formData.append('userId', userId)
			formData.append('userFile', file)
			formData.append('projectType', 'Primary')
			// formData.append('file', file)
			// formData.append('upload_preset', keyPreset)

			xhr.send(formData)
		})
	}

   return <div className={style.mediaUploadProgressBarWrapper}>
   			<div className={style.fileheaderWrapper}>{props.file.name}</div>
   			<LinearProgress variant="determinate" value={progress} />
   		</div>

}

export default SingleFileUploadWithProgress