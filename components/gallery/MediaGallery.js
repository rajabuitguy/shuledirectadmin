import React from 'react'
import { Button } from '@mui/material'
import PictureAsPdfSharpIcon from '@mui/icons-material/PictureAsPdfSharp';
import AudioFileIcon from '@mui/icons-material/AudioFile'
import VideoLibraryIcon from '@mui/icons-material/VideoLibrary'
import { useSelector } from 'react-redux'

// Custom
import GalleryProcess from '../../process/GalleryProcess'
import style from './styles/GalleryForm.module.scss'
import useAuth from '../common/useAuth'
import { MediaGalleryListSkeleton } from '../skeleton/SkeletonShapes'
// End Custom

const MediaGallery = ({handleSelectMedia,selectedMedia})=> {

	const { active_segment_to_upload_media } = useSelector(state=> state.media_gallery)
	let mediaType = active_segment_to_upload_media.media_type

	const [ gallery, setGallery ] = React.useState(null)
    const { user } = useAuth()
    const userId = user.isLoggedIn ? user.data.id: 0

	React.useEffect(()=> {
		getUserMediaGallery()
	},[])

	const getUserMediaGallery = async ()=> {
		let resultArray = await GalleryProcess.findSavedGalleryByUserId(userId)
		mediaType = (mediaType!="") ? mediaType.split('_') : null
		if(mediaType && mediaType.length===1){
			mediaType = mediaType[0]
			resultArray = (resultArray && resultArray.data && resultArray.data.length > 0) 
				? resultArray.data.filter(data=> data.media_types===mediaType) : null
		}else if (mediaType && mediaType.length === 2){
			let mediaType1 = mediaType[0]
			let mediaType2 = mediaType[1]
			let mediaType3 = mediaType[2]
			resultArray = (resultArray && resultArray.data && resultArray.data.length > 0) 
				? resultArray.data.filter(data=> data.media_types===mediaType1 || data.media_types===mediaType2 || data.media_types===mediaType3) : null
		}else{
			resultArray = (resultArray && resultArray.data && resultArray.data.length > 0) ? resultArray.data : null 
		}
		setGallery(resultArray)
	}

    return <div className={style.mediaGalleryWrapper}>
            {!gallery && (
            	<MediaGalleryListSkeleton width = "100%" height="200px" />
            )}
	    	{gallery && gallery.length > 0 && gallery.map((data, index)=> (
	    		<Button key={index} 
	    		        className={selectedMedia.id===data.id ? `${style.galleryActiveList} ${style.galleryList}` : `${style.galleryList}` } 
	    		        onClick={ ()=> handleSelectMedia(data) }>
	    		    
	    		    {data.media_types ==="image" && (
    		    		<div className={style.imageWrapper}>
	    					<img src={data.url} alt={data.file_name} className={style.galleryImage} />
    					</div>
	    		    )}

	    		    {data.file_extension ==="pdf" && (
	    				<PictureAsPdfSharpIcon style={{width:"100px",height:"80px",color:"red"}}/>
	    		    )}

	    		    {data.media_types ==="audio" && (
	    				<AudioFileIcon style={{width:"120px",height:"80px",color:"#666"}}/>
	    		    )}

	    		    {data.media_types ==="video" && (
	    				<VideoLibraryIcon style={{width:"120px",height:"80px",color:"#666"}}/>
	    		    )}

	    			<div className={style.galleryName}>
	    				{ data.file_name }
	    			</div>
	    		</Button>
	    	))}
    </div>
}
export default MediaGallery