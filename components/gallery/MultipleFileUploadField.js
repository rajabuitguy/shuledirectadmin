import React from 'react'
import { Typography,Button } from '@mui/material'
import style from './styles/GalleryForm.module.scss'
import { useDropzone,FileRejection } from 'react-dropzone'
import SingleFileUploadWithProgress from './SingleFileUploadWithProgress'
import UploadSharpIcon from '@mui/icons-material/UploadSharp'


const MultipleFileUploadField = ()=> {
    const [files, setFiles ] = React.useState([])

    const onDrop = React.useCallback((acceptedFiles, rejectedFiles ) => {
        const _acceptedFiles = acceptedFiles.map(file => ({ file, errors: [] }))
        setFiles((curr) => [...curr, ..._acceptedFiles, ...rejectedFiles ])
    }, [])

    const { getRootProps, getInputProps } = useDropzone({ onDrop })

    return <div className={style.mediaUploadWapper}> 
        
        <Button className={style.dropMediaWrapper} {...getRootProps()}>
        	<input { ...getInputProps() } />
            <div>
                <UploadSharpIcon style={{ fontSize:"60px",color:"#ccc"}} />
                <p>Drag and Drop the files here, Click to select the file </p>
            </div>
        </Button>

        {files && files.length> 0 && (
            <div className={style.uploadedImageWrapper}>
                {files.length > 0 && files.map( (fileWrapper, index) => (
                    <SingleFileUploadWithProgress key = { index } file = { fileWrapper.file }/>
                ))}
            </div>
        )}

    </div>

}

export default MultipleFileUploadField