import React from 'react';
import { styled } from '@mui/material/styles'
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import Stack from '@mui/material/Stack'
import Typography from '@mui/material/Typography'
import { useDispatch, useSelector } from 'react-redux'

// Custom
import { setActiveLangauge } from '../../redux/reducers/LanaguagesSlice'
import LocalData from '../../helper/LocalData'
// End Custom

const AntSwitch = styled(Switch)(({ theme }) => ({
  width: 28,
  height: 16,
  padding: 0,
  display: 'flex',
  '&:active': {
    '& .MuiSwitch-thumb': {
      width: 15,
    },
    '& .MuiSwitch-switchBase.Mui-checked': {
      transform: 'translateX(9px)',
    },
  },
  '& .MuiSwitch-switchBase': {
    padding: 2,
    '&.Mui-checked': {
      transform: 'translateX(12px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: theme.palette.mode === 'dark' ? '#177ddc' : '#1890ff',
      },
    },
  },
  '& .MuiSwitch-thumb': {
    boxShadow: '0 2px 4px 0 rgb(0 35 11 / 20%)',
    width: 12,
    height: 12,
    borderRadius: 6,
    transition: theme.transitions.create(['width'], {
      duration: 200,
    }),
  },
  '& .MuiSwitch-track': {
    borderRadius: 16 / 2,
    opacity: 1,
    backgroundColor:
      theme.palette.mode === 'dark' ? 'rgba(255,255,255,.35)' : 'rgba(0,0,0,.25)',
    boxSizing: 'border-box',
  },
}));


const LanguageSwitch = ()=> {
	const dispatch = useDispatch()
	const [ checked,setChecked ] = React.useState(false)
	const [ language,setLanguage ] = React.useState(null)

	const { language_data: languageArray,active_language:activeLanguage } = useSelector(state => state.language )

	React.useEffect(()=> {
		let resultArray = LocalData.getActiveLanguage() //check if language available in local
		if (resultArray){
			switchButtonToSpecificLanguageOnPageLoad()
		}else{
		    LocalData.addActiveLanguage(activeLanguage)
		    dispatch(setActiveLangauge(language))
		}
	},[])

	const switchButtonToSpecificLanguageOnPageLoad = ()=> {
		let resultArray = LocalData.getActiveLanguage()
		if (resultArray[0].name==="en"){
			setChecked(true)
		}else{
			setChecked(false)
		}
	}

	const handleChageLanguage = ()=> {
		let _language = event.target.checked 
				? languageArray.filter(data=> data.name==='en') 
				: languageArray.filter(data=> data.name==='sw')
		dispatch(setActiveLangauge(_language))
		setChecked(event.target.checked)
		LocalData.addActiveLanguage(_language)
		location.reload()
	}

	return <div>
		 <FormGroup>
		      <Stack direction="row" spacing={1} alignItems="center">
		        <Typography>Sw</Typography>
		        {/* <AntSwitch defaultChecked inputProps={{ 'aria-label': 'ant design' }} /> */}
		        <AntSwitch checked={checked} onChange={handleChageLanguage} inputProps={{ 'aria-label': 'ant design' }} />
		        <Typography>En</Typography>
		      </Stack>
		    </FormGroup>
		</div>
}

export default LanguageSwitch