import React from 'react'
import { Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'

// Custom
import {
	setDeletePublishData,
	setShowActionBar,
	clearDeletePublishData,
	clearShowActionBar,
	clearDeleteMode
} from './../../../redux/reducers/primary/DeletePublishActionBarSlice'
import style from './style/DeletePublishActionBar.module.scss'
import SyllabusProcess from '../../../process/primary/SyllabusProcess'
// End Custom

const DeletePublishActionBar = ()=> {
	const dispatch = useDispatch()
	const [ publishedStatus,setPublishedStatus ] = React.useState(false)
	const { delete_publish_data:dataArray,
			show_action_bar:showActionBar } = useSelector(state=> state.delete_publish_action)

	const cancelActionBar = ()=> {
		dispatch(clearShowActionBar())
		dispatch(clearDeleteMode())
		dispatch(clearDeletePublishData())
	}

	React.useEffect(()=> {
		if (dataArray.length===1 && dataArray[0].is_published!=='0'){
			setPublishedStatus(true)
		}else{
			setPublishedStatus(false)
		}
	},[dataArray])

	const deletePublishAction = async (status)=> {
		let _dataArray = dataArray.length > 0 && dataArray.map(data=> {
					 return {
					 	 baseId: data.baseId,
					 	 sourceId: data.sourceId,
					 	 indicator: status,
					 	 is_published: (data.is_published==="0") ? true : false
					 }
				})

		let messageData = (status==="delete") 
		? "Are you sure you want to delete the seleted resources? " 
		: (publishedStatus) ? "Are you sure you want to unpublish the seleted resource?" : "Are you sure you want to publish the seleted resource?"

		let delConf = confirm(messageData)
		if (!delConf){
            return false
		}
		const resultArray = await SyllabusProcess.deletePublishResources(_dataArray)
		location.reload()
	}

	return <> 
	    {showActionBar ? (
			<div className={style.deletePublishMainWrapper}>
				<ul className={style.actionButtonListWrapper}>
				    {dataArray && dataArray.length === 1 && (
						<li>
							<Button onClick={ () => deletePublishAction('publish-all') } className={style.button}>
								{ publishedStatus ? "UnPublish" : "Publish" }
							</Button>
						</li>
					)}
					<li><Button onClick={ () => deletePublishAction('delete-all') } className={style.button}>Delete</Button></li>
					<li><Button onClick={ cancelActionBar } className={style.button}>Cancel</Button></li>
				</ul>
			</div>
		):(<></>)}

	</>

}

export default DeletePublishActionBar