import { Typography, Grid,Button } from '@mui/material'
import { useDispatch } from 'react-redux'
import style from './style/ResourceForm.module.scss'

import {
	clearFormSumitError
} from '../../../redux/reducers/primary/ResourceFormIndexSlice'

const FormErrorLayout = ()=> {
	const dispatch = useDispatch()

	const handleClearLayoutCancelButton = ()=> {
		dispatch(clearFormSumitError())
	}

    return <div className={style.FormSubmissionErrorLayoutWrapper}>
        <div className={style.errorFormWrapper}>
    	 <Typography variant="h3">Found Error</Typography>
    	 <Typography variant="h6">We found an error during data processing. Please contact admin for fixing</Typography>
    	 <div className="mt-20">
    	 	 <Button onClick= { handleClearLayoutCancelButton } variant="contained" color="primary">Okay</Button>
    	 </div>
    	</div>
    </div>
}
export default FormErrorLayout