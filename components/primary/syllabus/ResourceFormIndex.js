import React from 'react'
import { Typography,Button } from '@mui/material'
import Box from '@mui/material/Box'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux'
import ArrowForwardSharpIcon from '@mui/icons-material/ArrowForwardSharp'
// Custom
import style from './style/ResourceForm.module.scss'
import LearningContentForm from './LearningContentForm'
import GamesResourceForm from './GamesResourceForm'
import SyllabusProcess from '../../../process/primary/SyllabusProcess'
import ResourcesHelper from '../../../process/primary/ResourcesHelper'
import {
	setResourceContentType,
	setResourceContentId,
	setResourceParentData,
	clearActiveContentToSave,
	setActiveFormInnerLayout,
	cancelActiveFormOverlay,
	clearFormSaveStatus
} from '../../../redux/reducers/primary/ResourceFormIndexSlice'
import {
	setDetailsData
} from '../../../redux/reducers/primary/LearningContentSlice'
import FormLayoutError from './FormErrorLayout'
// End Custom

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
}

const a11yProps = (index)=> {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}


const ResourceForm = ({subjectId=null,topicId=null,subtopicId=null,resourceContentId=null,classId=null,handleFormResource=null})=> {

	const dispatch = useDispatch()
	const [ isLoading,setIsLoading ] = React.useState(false)
	const { saveResourceHelper,
					getResourceByIdHeper }  = ResourcesHelper()

	const {	
		resource_content_type:resourceContentType,
		active_content_to_save: activeContentToSave,
		form_saving_status: formSavingStatus,
		submit_form_error: formSubmitErrorStatus,
		resource_parent_data,resource_content_id } = useSelector( state => state.resource_form_index )

	const [ value, setValue ] = React.useState(resourceContentType.index_value)
  const [controlOverlay, setControlOverlay] = React.useState(true)

	React.useEffect(()=> {
			resourceContentId = resourceContentId ? resourceContentId : null
			dispatch(setResourceContentId({resource_content_id: resourceContentId}))
			dispatch(setResourceParentData({
				 subject_id: subjectId,
				 classs_id: classId,
				 topic_id: topicId,
				 subtopic_id: subtopicId
			}))
	},[])

	React.useEffect(
		()=> {
		setValue(resourceContentType.index_value)
	},[resourceContentType])

	React.useEffect(()=> {
		if (resourceContentId){
		  // setControlOverlay(false)
			getResourceByIdHeper(resourceContentId)
		}
	},[])

	React.useEffect(()=> {
		if (controlOverlay){
			dispatch(setActiveFormInnerLayout())
		}
	},[controlOverlay])

	//Control form auto saving
	React.useEffect(()=> {
		if (formSavingStatus){
			 handleSaveResourceForm()
		}
	},[formSavingStatus])


	const handleChange = (event, newValue) => {
		  if (newValue===0){
		  	let confirm_ = confirm("Switching to learning content mode will erase all the game resources data. Are you sure you want to proceed?") 
		  	if (!confirm_){
		  			return false
		  	}
		  	dispatch(setResourceContentType({  index_value: 0, name: "learning_content" }))
		  }else if (newValue===1){
		  	let confirm_ = confirm("Switching to game resource mode will erase all the learning content data. Are you sure you want to proceed?") 
		  	if (!confirm_){
		  			return false
		  	}
		  	dispatch(setResourceContentType({  index_value: 1, name: "games_resources" }))
		  }
	    setValue(newValue)
	    dispatch(clearActiveContentToSave())
	    dispatch(setActiveFormInnerLayout())
	}

	const handleSaveResourceForm = async ()=> {
		  setIsLoading(true)
			await saveResourceHelper()
		  setControlOverlay(false)
		  dispatch(cancelActiveFormOverlay())
		  dispatch(clearFormSaveStatus())
		  setIsLoading(false)
	}

	return <>
	    {formSubmitErrorStatus && (
	    	<FormLayoutError />
	    )}
			<div className={style.resourceFormModalWrapper}>
				<div className={style.resourceFormWrapper}>
					<div className={style.resourceFormHeader}>
						<div className={style.headerUpperWrapper}>
							 <div className={`${style.headerUpperLeft}`}>
							 	 <Typography variant="h5"><b>Create new</b></Typography>
							 </div>
							 <div className={`${style.headerCenterWrapper}`}>
							 	<Box>
							      <Tabs value={value} onChange={handleChange} centered>
							        <Tab label="Learning Content" {...a11yProps(0)} />
							        <Tab label="Game Resources" {...a11yProps(1)} />
							      </Tabs>
							    </Box>
							 </div>
							 <div className={`${style.headerUpperRight}`}>
							 	<button className="cancel-button" onClick={ handleFormResource }>X</button>
							 </div>
						</div>
					</div>
					<div className={style.resourceFormBody}>
							
					   	{activeContentToSave.active_form_overlay && (
						  	<div className={style.resourceFoemBodyOverlay}>

						  	   {activeContentToSave.show_overlay_cancel &&( 
							  		 <div className={style.cancelInnerFormOverlay}>
							  		 	  <button title="Quit the ovelay" onClick={ ()=> dispatch(clearActiveContentToSave()) } className={style.cancelButton}>
							  		 	  	 <ArrowForwardSharpIcon style={{fontSize:"40px"}}/>
							  		 	  </button>
							  		 </div>
						  		 )}

						  	</div>
						  )}

						  <TabPanel value={value} index={0}>
					          <LearningContentForm />
					      </TabPanel>
					      <TabPanel value={value} index={1}>
					          <GamesResourceForm />
					      </TabPanel>
					</div>
					<div className={style.resourceFormBottom}>
					  	{activeContentToSave.show_form_save_button && (
					  		  <div>
			  		 					{ !isLoading ? (
		                      <Button
		                        onClick = { handleSaveResourceForm }
		                        variant="contained" color="primary">
		                         Save
		                    </Button>
		                  ) : (
		                  <Button
		                      disabled
		                      onClick = { handleSaveResourceForm }
		                      variant="contained" color="primary">
		                      Saving..
		                    </Button>
		                	)}
				          </div>
							)}
					</div>

				</div>
			</div>
	</>

}
export default ResourceForm