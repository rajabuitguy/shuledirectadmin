import React from 'react'
import AudioFileIcon from '@mui/icons-material/AudioFile'
import { Typography, Grid,Button } from '@mui/material'
import VideoLibraryIcon from '@mui/icons-material/VideoLibrary'
// Custom
import style from './style/ResourceForm.module.scss'
// Custom
const GameSettings = ({settings_data,index,handleShowGallery,actionName})=> {


	return <div className={style.gameSettingsMainWrapper}>
	        <Typography variant="h6" style={{marginBottom:"10px"}}>Settings</Typography>
	        { settings_data.map((data,_index)=> (

				<div className={style.gameSettingsWrapper} key={_index}>
					  <div className={style.leftWrapper}>

				       		{ data.type==='audio' && (
				       			<React.Fragment>
					       			{ data.media.url==="" ? ( 
								  	   <Button className={style.settingMediaWrapper} onClick={ ()=> handleShowGallery(actionName,index,_index ) }>
								  	   		<AudioFileIcon style={{fontSize:"50px",color:"#ddd"}}/>
								  	   </Button>
							  	   	):(
							  	   	   <Button className={style.settingMediaWrapper} onClick={ ()=> handleShowGallery(actionName,index,_index ) }>
								  	   		<AudioFileIcon style={{fontSize:"50px",color:"#333"}}/>
								  	   </Button>
							  	   	)}
						  	   	</React.Fragment>
					  	   	)}

					  	   	{ data.type==="video" && (
				       			<React.Fragment>
						  	   		{ data.media.url==="" ? ( 
							  	     	<Button className={style.settingMediaWrapper} onClick={ ()=> handleShowGallery(actionName,index,_index ) }>
							  	   			<VideoLibraryIcon style={{fontSize:"50px",color:"#ddd"}}/>
							  	   	  	</Button>
						  	   	  	):(
						  	   	  		<Button className={style.settingMediaWrapper} onClick={ ()=> handleShowGallery(actionName,index,_index ) }>
						  	   				<VideoLibraryIcon style={{fontSize:"50px",color:"#333"}}/>
						  	   	  		</Button>
						  	   	  	)}
						  	   	</React.Fragment>
					  	   	)}

					  </div>
					  <div className={style.rightWapper}>
					  	  <div>{ data.title }</div>
					  	  <div className={style.mediaTextWrapper}>
					  	     {data.media.url !="" ? (
					  	    	<div> { data.media.file_name }</div>
					  	     ):(
					  	    	<div>No file </div>
					  	     )}
					  	  </div>
					  </div>
				</div>

			))}
			
		</div>
}

export default GameSettings