import React from 'react'
import style from './style/Syllabus.module.scss'
import { Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'

import {
	setDeletePublishData,
	setShowActionBar,
	clearDeletePublishData,
	setDeleteMode,
	removeDataById
} from './../../../redux/reducers/primary/DeletePublishActionBarSlice'

const GamesAndLearningContentList = ({resource_data,handleFormResource})=> {
	// console.log("resource_data",resource_data)
	const dispatch = useDispatch()
	const [ selectedBlock, setSelectedBlock ] = React.useState([])
    const [ resourceArray,setResouceArray] = React.useState(null)

    const { delete_publish_data:dataArray,
    	    delete_mode:deleteMode,
			show_action_bar:showActionBar } = useSelector(state=> state.delete_publish_action)

    React.useEffect(()=> {
	    let resourceArrayData = []
	    //process learning content to the resourcesArrayData
		let learningContent = resource_data.learning_content && resource_data.learning_content.map((data)=> {
			let dataObject =  {...data,resource_content_type:'learning_content'}
			resourceArrayData.push(dataObject)
		})

		//process games resources to the resourcesArrayData
		let gamesResources = resource_data.game_resources && resource_data.game_resources.map((data)=> {
			let dataObject =  {...data,resource_content_type:'games_resources'}
			resourceArrayData.push(dataObject)
		})
		setResouceArray(resourceArrayData)

    },[])


	const handleCheckbox = (e)=> {
		let _selectedBlock = null
		if (e.target.checked) {
			_selectedBlock = resourceArray.filter(data=> data.id===e.target.value)
		    dispatch(setDeletePublishData(_selectedBlock[0]))
		    dispatch(setDeleteMode())
		}else{
		    dispatch(removeDataById(e.target.value))
		}
		dispatch(setShowActionBar())
	}

	return <div className={style.gamesAndLearningWrapper}>
		<div className={style.gamesAndLearningListWrapper}>
		   {resourceArray && resourceArray.length > 0 && resourceArray.map((data,index)=> (
				<div className={style.gamesLearningCard} key={index}>

					<div className={ (deleteMode) ? `${style.cardHeaderWrapper} ${style.activeMode}` : `${style.cardHeaderWrapper}`}>
						<input type="checkbox" name="delete" value={ data.id } onChange = { handleCheckbox } />
					</div>

					<Button className={style.cardButtonWrapper} onClick={ ()=> handleFormResource(data,null) }>
						<div className={style.cardPhotoWrapper}>
						    {data.cover_image && data.cover_image.map((_data,_index) => ( 
						    	<React.Fragment key={_index}>
						    		<img src={_data.url}  alt={data.name} />
						    	</React.Fragment>
						    ))}
						</div>
						<div className={style.cardDescriptionWrapper}>
							{ data.name } &nbsp;({data.resource_content_type ==="games_resources" ? "Game" : "Learning Content"})
						  	<div><b>{data.is_published ==="0" ? "Draft" : "Published"})</b></div>
						</div>
					</Button>
				</div>
		   ))}
		</div>
	</div>

}
export default GamesAndLearningContentList