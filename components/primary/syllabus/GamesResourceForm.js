import React from 'react'
import { Typography, Grid,Button } from '@mui/material'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import AddSharpIcon from '@mui/icons-material/AddSharp'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import AudioFileIcon from '@mui/icons-material/AudioFile'
import { useDispatch, useSelector } from 'react-redux'
import ResourcePropertiesModal from './ResourcePropertiesModal'

// Custom
import style from './style/ResourceForm.module.scss'
import Gallery from '../../gallery'
import { TextForm,PasswordForm,HiddenForm } from '../../resource/Forms'
import GameSettings from './GameSettings'
import {
		setResourceDetails,
	  	addNewStepBlock,
	  	addNewStepsOptionBlock,
	  	removeStepBlock,
	  	removeStepBlockOptionsCard,
	  	clearNumberRangeForm,
	  	clearImageResourceForm,
	  	setNumberRangeForm,
	  	updateResourceStepsTextBlock,
	  	setResourceStepsBlock,
	  	addNewNumberRangeForm,
	  	removeNumberRangeForm,
		setInitialOptionProperties,
		setOpenOptionsPropertiesModal
} from '../../../redux/reducers/primary/GameResourcesSlice'
import {
	  	clearContentDetailsCoverImage,
	  	clearImageAudioCoontent,
	  	clearVideoContent  
} from '../../../redux/reducers/primary/LearningContentSlice'
import { 
	showMediaGallery,
	activeSegmentToUploadMedia 
} from '../../../redux/reducers/MediaGallerySlice'
import {
	setResourceContentType,
	setActiveContentToSave,
	clearActiveContentToSave,
	setFormSaveStatus
} from '../../../redux/reducers/primary/ResourceFormIndexSlice'
import ResourcesHelper from '../../../process/primary/ResourcesHelper'

// End Custom

const initActiveAction = {
		index_key: { parent_index: 0,child_index: 0 },
		active_source: '',
		is_active: null
	}

const GamesResourceForm = ()=> {

	const dispatch = useDispatch()
	const { details_data:detailsData,
			number_range:numberRangeForm,
			option_properties_data:propertiesData } = useSelector(state=> state.game_resources)
	const resourcesStepBlocks = useSelector(state=> state.game_resources.resource_steps_block)
	const { show_media_gallery:showMediaGalleryStatus,
			active_segment_to_upload_media } = useSelector(state=> state.media_gallery)
	const {	resource_parent_data:resourceParentData,
			active_content_to_save: activeContentToSave,
			resource_content_type: resourceContentType,
		    resource_content_id:resourceContentId } = useSelector( state => state.resource_form_index )

	const { deleteGameResources }  = ResourcesHelper()

	const [ value_, setValue_ ] = React.useState(detailsData.game_resource_type)
	const [ showGameResourceType, setShowGameResouceType ] = React.useState(true)
	const [ activeAction,setActiveAction ] = React.useState(initActiveAction)

	// console.log("resourcesStepBlocks",resourcesStepBlocks)


    React.useEffect(()=> {
    	dispatch(setResourceDetails({ data:{
			...detailsData.data,
			subject_id: resourceParentData.subject_id,
			topic_id: resourceParentData.topic_id,
			classs_id: resourceParentData.classs_id,
			subtopic_id:resourceParentData.subtopic_id
		}}))

		// Clear the data from the learning content form
		dispatch(clearContentDetailsCoverImage())
		dispatch(clearImageAudioCoontent())
		dispatch(clearVideoContent())
		// End clear the data from the learning content form
	
	},[resourceParentData])

	React.useEffect(()=> {
		if (value_==="image_game_resource"){
			setShowGameResouceType(true)
		}
		else {
			setShowGameResouceType(false)
		}
		// dispatch(clearActiveContentToSave())
	},[value_,detailsData])

	const handleFormChange = (prop) => (event) => {
		setActiveAction({ index_key: { parent_index: 0},active_source: 'game_resources_update_details',is_active:true })
		let formValue = (prop==="unique_key_code") ? event.target.value.replace(/\s+/g, '') : event.target.value
		dispatch(setResourceDetails({ 
			game_resource_type: "content_details",
			data:{
				...detailsData.data,
				[prop]: formValue
			}
		}))
	}

	const handleOptionFormChange = (prop,index) => (e) => {
		 setActiveAction({ index_key: { parent_index: index.parent, child_index:index.child },active_source: 'game_options_block_name_update',is_active:true })
		 let updateData = {
		 	 index: {
		 	 	parent_index: index.parent,
		 	 	child_index: index.child,
		 	 	name: e.target.value 
		 	 }
		 }
		dispatch(updateResourceStepsTextBlock(updateData))
  	}

	React.useEffect(()=> {
		if (!activeAction.is_active){
			return false
		}
		processActiveGamesResourcesToSave()
	},[detailsData,numberRangeForm,resourcesStepBlocks])

	const handleNumberFormChange = (prop,index) => (e) => {
		
		setActiveAction({ index_key: { parent_index: index ,child_index: 0 }, active_source: "game_resource_number_range_form_input_data",is_active:true})
		let newFormValues = [...numberRangeForm]
		newFormValues[index] = {
			...newFormValues[index],
			data:{
				...newFormValues[index].data,
				[prop]: e.target.value
			}
		}
		dispatch(setNumberRangeForm(newFormValues))
	}

	const processActiveGamesResourcesToSave = ()=> {
		setActiveAction({
			...activeAction,
			is_active: false
		})

		let _activeConentData = null
		let contentType = ''
		let formOverlay = false
		let showSaveButton = false
        if (activeAction.active_source==="game_resource_step_question_resource_upload" || 
        	activeAction.active_source==="game_resource_step_answer_resource_upload" || 
        	activeAction.active_source==="game_resource_step_audio_resource_upload" || 
        	activeAction.active_source==="game_options_block_name_update"){
        	showSaveButton = true
        	contentType = value_
        	let _array = resourcesStepBlocks[activeAction.index_key.parent_index]
        	let __array = _array.options_array[activeAction.index_key.child_index]
        	_activeConentData = [
        	    { 
        	    	block_info: _array.block_info,
        	    	options_array: [__array]
        	    }
        	]
        }else if (activeAction.active_source==="game_resource_step_block_media_setting_upload"){
        	contentType = value_
        	showSaveButton = true
        	let _array = resourcesStepBlocks[activeAction.index_key.parent_index] 
        	let __array = _array.settings_data[activeAction.index_key.child_index]
        	_activeConentData = [
        		{ 
        			block_info: _array.block_info,
        			settings_data: [__array] 
        		}
        	]
        }else if (activeAction.active_source==="game_resource_details_cover_upload" || 
        		  activeAction.active_source==="game_resources_update_details"){
        	contentType = "content_details",
        	formOverlay = true
        	showSaveButton = true
        	_activeConentData = [detailsData.data]

        }else if (activeAction.active_source==="game_resource_number_range_form_cover_upload" || 
        	      activeAction.active_source==="game_resource_number_range_form_input_data"){
        	contentType = value_
        	showSaveButton = true
        	let _array = numberRangeForm[activeAction.index_key.parent_index] 
        	let coverArray = _array.cover_image[activeAction.index_key.child_index]
        	_activeConentData = [{ cover_image: [coverArray],data: _array.data,block_info: _array.block_info  }]
        }else if (activeAction.active_source==="game_number_resource_media_settings_upload"){
        	contentType = value_
        	showSaveButton = true
        	let _array = numberRangeForm[activeAction.index_key.parent_index] 
        	let __array = _array.settings_data[activeAction.index_key.child_index]
        	_activeConentData = [{ block_info: _array.block_info, settings_data: [__array] }]
        }
        if (!_activeConentData){
        	return false
        }

        dispatch(setActiveContentToSave({
        		resource_type: resourceContentType.name,
        		content_type: contentType,
        		active_form_overlay: formOverlay,
        		show_form_save_button: showSaveButton,
        		data: _activeConentData
        	}))
       		
        	controllingGameAutoSaving()
	}

	const controllingGameAutoSaving = ()=>{
		if (activeAction.active_source!=="game_options_block_name_update" && 
    		activeAction.active_source!=="game_resources_update_details" &&
    		activeAction.active_source!=="game_resource_number_range_form_input_data"){ 
            dispatch(setFormSaveStatus())
        }
	}

	const handleShowGallery = (source,parentIndex=null,childIndex=null)=> {
		if (source==='cancel_modal'){
    		dispatch(showMediaGallery())
			return false
		}

		if (source!='cancel_modal'){
			setActiveAction({ index_key: { parent_index: parentIndex,child_index: childIndex}, active_source: source,is_active:true })
		}

		if (source==="game_resource_details_cover_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_resource_details_cover_upload",
				media_type:"image",
				index:parentIndex
			}))
		}else if (source==="game_resource_step_question_resource_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_resource_step_question_resource_upload",
				media_type:"image",
				index:{
					parent_index: parentIndex,
					child_index: childIndex
				}
			}))
		}else if (source==="game_resource_step_answer_resource_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_resource_step_answer_resource_upload",
				media_type:"image",
				index:{
					parent_index: parentIndex,
					child_index: childIndex
				}
			}))
		}else if (source==="game_resource_number_range_form_cover_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_resource_number_range_form_cover_upload",
				media_type:"image",
				index:{
					parent_index: parentIndex,
					child_index: childIndex
				}
			}))
		}else if (source==="game_number_resource_media_settings_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_number_resource_media_settings_upload",
				media_type:"audio_video_image",
				index:{
					parent_index: parentIndex,
					child_index: childIndex
				}
			}))
		}else if (source==="game_resource_step_audio_resource_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_resource_step_audio_resource_upload",
				media_type:"audio",
				index:{
					parent_index: parentIndex,
					child_index: childIndex
				}
			}))
		}else if ("game_resource_step_block_media_setting_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "game_resource_step_block_media_setting_upload",
				media_type:"audio_video_image",
				index:{
					parent_index: parentIndex,
					child_index: childIndex
				}
			}))
		}

		dispatch(showMediaGallery())

    }

	const handleAddNewStepsBlock = () => {
		dispatch(addNewStepBlock())	
		setActiveAction(initActiveAction)
		dispatch(clearActiveContentToSave())
	}

	const removeStepBlockFields = async (index,blockInfo) => {

		if (blockInfo.id===""){
	         dispatch(removeStepBlock(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
		}else{
		    let delConf =  confirm("Are you sure you want to delete this block?")
		    if (!delConf){
		    	return false
		    }

			let params = {
				baseId:detailsData.data.baseId,
				sourceId:detailsData.data.sourceId,
				blockId:blockInfo.id,
				indicator: "delete-block"
			}
	         dispatch(removeStepBlock(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
			let delResult = await deleteGameResources(params)
		}

		
	}

	const handleAddOptionsBlock = (index) => {
		dispatch(addNewStepsOptionBlock(index))
		setActiveAction(initActiveAction)
		dispatch(clearActiveContentToSave())
	}

	const handleRemoveOptionCard = async (parentIndex,index,blockInfo=null,optiondata=null)=> {
		let index_ = {
			parent_index: parentIndex,
			child_index: index
		}
		if (optiondata.question.id===""){
			dispatch(removeStepBlockOptionsCard(index_))
			setActiveAction(initActiveAction)
			dispatch(clearActiveContentToSave())
		}else{
			let delConf =  confirm("Are you sure you want to delete this option?")
		    if (!delConf){
		    	return false
		    }

		    let params = {
				baseId:detailsData.data.baseId,
				sourceId:detailsData.data.sourceId,
				blockId:blockInfo.id,
				questionId:optiondata.question.parent_id, 
				indicator: "delete-question"
			}

			let delResult = await deleteGameResources(params)
			dispatch(removeStepBlockOptionsCard(index_))
			setActiveAction(initActiveAction)
			dispatch(clearActiveContentToSave())
		}
	}

	const handleGameResourceType = (event) => {
  		if (event.target.value==="image_game_resource"){
  			dispatch(clearNumberRangeForm())
  		}
  		else if (event.target.value==="number_range"){
  			dispatch(clearImageResourceForm())
  		}
		dispatch(setResourceDetails({ game_resource_type: event.target.value }))
    	setValue_(event.target.value)
		setActiveAction(initActiveAction)
		dispatch(clearActiveContentToSave())
  	}

  	const handleAddNewNumberRange = ()=> {
  		dispatch(addNewNumberRangeForm())	
		setActiveAction(initActiveAction)
		dispatch(clearActiveContentToSave())
  	}

  	const handelRemoveNumberRangeForm = async (index,blockInfo)=> {

  		if (blockInfo.id===""){
	         dispatch(removeNumberRangeForm(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
		}else{
		    let delConf =  confirm("Are you sure you want to delete this form?")
		    if (!delConf){
		    	return false
		    }

			let params = {
				baseId:detailsData.data.baseId,
				sourceId:detailsData.data.sourceId,
				blockId:blockInfo.id,
				indicator: "delete-block"
			}
	         dispatch(removeNumberRangeForm(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
			let delResult = await deleteGameResources(params)
		}
  	}
	
	const handleOptionProperties = (blockInfo,selectedOption,indexData,optionType)=> {
		dispatch(setOpenOptionsPropertiesModal())
		dispatch(setInitialOptionProperties({
			index: indexData,
			selected_option: selectedOption,
			option_type: optionType
		}))
	}


	return <div className={style.gamesResourcesContentWrapper}>
	      	{showMediaGalleryStatus && (
	        	<Gallery handleShowGallery = { handleShowGallery } />
	      	)}

			{propertiesData.open_properties_modal && (
				<ResourcePropertiesModal />
			)}

			<div className={style.gamesContentFormWrapper}>
				    <div className={style.gamesContentLeftWrapper}>
				           <div className={style.gamesContentInnerWapper}>
							    <Typography variant="h5"><b>Game Resources</b></Typography>
					            <div className={style.innerContentForm}>
							     	 <div className="mt-10">
						                <TextForm
						                  handleOnChange={handleFormChange}
						                  value={ detailsData.data.name }
						                  fieldName="name"
						                  error = {null}
						                  label="Game name" />
						                 
						              </div>
						               <div className="mt-10">
						                <TextForm
						                  handleOnChange={handleFormChange}
						                  value={ detailsData.data.unique_key_code }
						                  fieldName="unique_key_code"
						                  error = {null}
						                  label="Unique key code (No space)" />
						              </div>
						              <div className="mt-20">
						                <label>Game Cover</label>
						                {detailsData.data.cover_image.map((data,index)=> (
						                	<div key={index}>
								                { data.url === "" ? (
									                <Button title="Add cover image" className={style.contentCoverImageWrapper} onClick = {  ()=> handleShowGallery('game_resource_details_cover_upload',index) }>
									                	<AddPhotoAlternateIcon sx={{ fontSize: 200,color:"#ddd" }} />
									                </Button>
									            ):(
									            	<Button title="Change cover image" className={style.contentCoverImage} onClick = {  ()=> handleShowGallery('game_resource_details_cover_upload',index) }>
									                	<img src={ data.url} />
									                </Button>
									            )}
						              		</div>
							            ))}
						              </div>
						              
				                </div>
			                </div>
				    </div>
				   <div className={style.gamesContentRightWrapper}>
				   	    
				   	    <div className={style.contentResourceTypeWrapper}>
				      	 	<div className={style.contentResourceTypeLeftWrapper}>
				      	 		<div>Resource Type</div>
				      	 	</div>
				      	 	<div className={style.contentResourceTypeRightWrapper}>
					      	 	 <FormControl>
							      <RadioGroup
							        aria-labelledby="demo-controlled-radio-buttons-group"
							        name="content_type"
							        value={ value_ }
							        onChange={handleGameResourceType}
							      >
							        <FormControlLabel value="image_game_resource" control={<Radio />} label="Images game resources" />
							        <FormControlLabel value="number_range" control={<Radio />} label="Numbers range (Min-Max)" />
							      </RadioGroup>
							    </FormControl>
					      	</div>
				      	</div>

				      	{ showGameResourceType ? (
				      		<div>
						   	    {resourcesStepBlocks && resourcesStepBlocks.map((data,index) => (
						   	    	<div key={data.id} className={style.questionBlockWrapper}>
						   	    	    <div className={style.questionBlockHeader}>
						   	    	        <div className={style.headerLeft}>Question Block {index + 1}</div>
							   	    	    <div className={style.removeQuestionBlockWapper}>
										   	    <button onClick= { ()=> removeStepBlockFields(index,data.block_info) }>Remove</button>
					      		          	</div>
				      		          	</div>
								   	    <div className={style.resourceTableWrapper} >
								   	    	 <div className={style.tableLeftColumnWrapper}>

								   	    	 	<div className={style.tableCellWrapper}>
								   	    	 	  <div className={`${style.textWrapper} p-10`}>Name</div>
								   	    	 	  <div className={`${style.tableCell} p-10`}>Question resources options</div>
								   	    	 	  <div className={`${style.tableCell} p-10`}>Answer resources options</div>
								   	    	 	  <div className={`${style.tableCell} p-10`}>Audio resource options</div>
								   	    	 	</div>

								   	    	 </div>
								   	    	 <div className={style.tableRightColumnWrapper}>

								   	    	 	{data.options_array && data.options_array.length>0 && data.options_array.map((_data, _index) => (
								   	    	 		<div key={_index}>
										   	    	 	
										   	    	 	<div className={style.tableCellWrapper}>
											   	    	 	<div className={style.optionRemoveButtonWrapper}>
											   	    	 		<button title="Remove card" onClick={ ()=> handleRemoveOptionCard(index,_index,data.block_info,_data) }>x</button>
											   	    	 	</div>
											   	    	  <div className={`${style.textWrapper}`}>
										   	    	 	        <div>
										   	    	 	        	<TextForm
							   	    				  	  			  index = {{parent:index,child:_index}}
													                  handleOnChange={ handleOptionFormChange }
													                  value={ _data.content.name }
													                  fieldName="name"
													                  error = {null}
													                  label="Name" />
										   	    	 	        </div>
										   	    	 	  </div>
										   	    	 	  <div className={style.tableCell}>
										   	    	 	        { !_data.question.url || _data.question.url ==="" ? (
												   	    	 	  	<Button title="Add image" className={style.questionImageResource} onClick = {  ()=> handleShowGallery('game_resource_step_question_resource_upload',index,_index) }>
												   	    	 	  	  	  <AddPhotoAlternateIcon style={{color:"#ddd",fontSize:"80px"}} />
												   	    	 	  	</Button>
											   	    	 	  	):(
											   	    	 	  	  	 <Button title="Change image" className={style.questionImageResource} onClick = {  ()=> handleShowGallery('game_resource_step_question_resource_upload',index,_index) }>
											   	    	 	  	  	  		<img src={_data.question.url} alt={ _data.question.file_name } />
											   	    	 	  	  	 </Button>
											   	    	 	  	)}

																{ !_data.question.url || _data.question.url !="" && (
																	<div className={style.tableCellProperties}>
																		<button onClick={ ()=>handleOptionProperties(data,_data,{parent_index:index,child_index: _index},"question") }>Properties</button>
																	</div>
																)}

										   	    	 	  </div>
										   	    	 	  <div className={style.tableCell}>
										   	    	 	        { !_data.answer.url || _data.answer.url === "" ? (
												   	    	 	  	<Button title="Add image" className={style.questionImageResource} onClick = {  ()=> handleShowGallery('game_resource_step_answer_resource_upload',index,_index) }>
												   	    	 	  	  	  <AddPhotoAlternateIcon style={{color:"#ddd",fontSize:"80px"}} />
												   	    	 	  	</Button>
											   	    	 	  	):(
											   	    	 	  	  	 <Button title="Change image" className={style.questionImageResource} onClick = {  ()=> handleShowGallery('game_resource_step_answer_resource_upload',index,_index) }>
											   	    	 	  	  	  		<img src={_data.answer.url} alt={ _data.answer.file_name } />
											   	    	 	  	  	 </Button>
											   	    	 	  	)}
																
																{ !_data.answer.url || _data.answer.url != "" && (
																	<div className={style.tableCellProperties}>
																		<button onClick={ ()=> handleOptionProperties(data,_data,{parent_index: index,child_index: _index},"answer") }>Properties</button>
																	</div>
																)}
										   	    	 	  	  
										   	    	 	  </div>
										   	    	 	  <div className={style.tableCell}>
										   	    	 	        { !_data.audio.url || _data.audio.url === "" ? (
												   	    	 	  	<Button title="Add audio" className={style.questionImageResource} onClick = {  ()=> handleShowGallery('game_resource_step_audio_resource_upload',index,_index) }>
												   	    	 	  	  	  <AudioFileIcon style={{color:"#ddd",fontSize:"50px"}} />
												   	    	 	  	</Button>
											   	    	 	  	):(
											   	    	 	  	  	 <Button style={{textTransform:"capitalize"}} title="Change audio" className={style.questionImageResource} onClick = {  ()=> handleShowGallery('game_resource_step_audio_resource_upload',index,_index) }>
											   	    	 	  	  	  		<div className={style.audioResourceWrapper}>
											   	    	 	  	  	  			<div>{_data.audio.file_name}</div>
											   	    	 	  	  	  			<div className={style.audioResourceBottomWrapper}>
												   	    	 	  	  	  			<AudioFileIcon style={{color:"#333",fontSize:"30px"}} />
												   	    	 	  	  	  		</div>
											   	    	 	  	  	  		</div>
											   	    	 	  	  	 </Button>
											   	    	 	  	)}
										   	    	 	  	  
										   	    	 	  </div>
										   	    	 	</div>
									   	    	 	</div>
												))}
												<div className={style.tableCellWrapper}>
													<Button onClick={ () => handleAddOptionsBlock(index) } className={style.addChoicesButtonWrapper}>
													   <AddSharpIcon /> Add Options
													</Button>
									   	    	</div>

								   	    	 </div>
								   	    </div>
						   	    		<GameSettings 
						   	    				settings_data = { data.settings_data } 
						   	    				index = {index} 
						   	    				handleShowGallery = { handleShowGallery } 
						   	    				actionName="game_resource_step_block_media_setting_upload" />

							   	    </div>
						   	    ))}
				   	            <Button style={{textTransform:"capitalize",marginTop:"20px"}} onClick={ handleAddNewStepsBlock }> <AddSharpIcon /> Add new question block</Button>
						   	</div>

				   	    ):(
				   	    	<div className={style.numberResourceWrapper}>
				   	    		<Typography variant="h6">Numbers range(Min - Max)</Typography>
				   	    		{ numberRangeForm.map((__data,index)=> (
				   	    			<div className={style.numberRangeForm} key={index}>
				   	    			    <div className={style.numberRangeFormHeader}>
						   	    	        <div className={style.headerLeft}>Form {index + 1}</div>
							   	    	    <div className={style.removeQuestionBlockWapper}>
										   	    <button onClick= { ()=> handelRemoveNumberRangeForm(index,__data.block_info) }>Remove</button>
					      		          	</div>
				      		          	</div>
						   	    		<div key={index} className={style.numberResourceFormWrapper}>
						   	    			<div className={style.leftWrapper}>
						   	    				 
						   	    				 { __data.cover_image.map((_data,_index)=> (
						   	    				 	<div key={_index}>
								   	    			     {!_data.url || _data.url ==="" ? (
									   	    				 <Button className={style.numberImageButton} title="Add image (Optional)" onClick={ ()=> handleShowGallery("game_resource_number_range_form_cover_upload",index,_index) }>
									   	    				 	<AddPhotoAlternateIcon style={{fontSize:"140px",color:"#eee"}}/>
									   	    				 </Button>
								   	    				 ):(
								   	    				    <Button className={style.numberImageButton} title="Change image" onClick={ ()=> handleShowGallery("game_resource_number_range_form_cover_upload",index,_index) }>
									   	    				 	<img src={_data.url} alt={_data.file_name} />
									   	    				 </Button>
								   	    				 )}
							   	    				 </div>
						   	    				))}

						   	    			</div>
						   	    			<div className={style.rightWrapper}>
						   	    			    <div className="sd-flex">
						   	    			      <div className="sd-flex-column p-10">
							   	    			      <div className="mb-10"> 
								   	    				  <TextForm
								   	    				  	  index = {index}
											                  handleOnChange={ handleNumberFormChange }
											                  value={ __data.data.min }
											                  fieldName="min"
											                  error = {null}
											                  label="Min number" />
										               </div>
										               <div className="mb-10">
								   	    				  <TextForm
								   	    				  	  index = {index}
											                  handleOnChange={ handleNumberFormChange }
											                  value={ __data.data.max }
											                  fieldName="max"
											                  error = {null}
											                  label="Max number" />
										               </div>
									            	</div>
									            	<div className="sd-flex-column p-10">
									            		  <div className="mb-10">
								   	    				  <TextForm
								   	    				  	  index = {index}
											                  handleOnChange={ handleNumberFormChange }
											                  value={ __data.data.content }
											                  fieldName="content"
											                  error = {null}
											                  label="Content" />
										               </div>
										               <div className="mb-10">
								   	    				  <TextForm
								   	    				  	  index = {index}
											                  handleOnChange={ handleNumberFormChange }
											                  value={ __data.data.range_type }
											                  fieldName="range_type"
											                  error = {null}
											                  label="Range Type" />
										               </div>
									            	</div>
									            	<div className="sd-flex-column p-10">
									            		  <div className="mb-10">
								   	    				  <TextForm
								   	    				  	  index = {index}
											                  handleOnChange={ handleNumberFormChange }
											                  value={ __data.data.unlock_points }
											                  fieldName="unlock_points"
											                  error = {null}
											                  label="Unlock Points" />
										               </div>
									            	</div>
									            </div>
						   	    			</div>
						   	    		</div>
						   	    		<GameSettings 
						   	    		       settings_data = { __data.settings_data } 
						   	    		       index = {index} 
						   	    		       handleShowGallery = { handleShowGallery } 
						   	    		       actionName = "game_number_resource_media_settings_upload" />
					   	    		</div>
				   	    		))}
				   	            <Button style={{textTransform:"capitalize",marginTop:"20px"}} onClick={ handleAddNewNumberRange }> <AddSharpIcon /> Add new form</Button>
								
				   	    	</div>
				      	)}


				   </div>
			</div>
			  
	</div>
}
export default GamesResourceForm