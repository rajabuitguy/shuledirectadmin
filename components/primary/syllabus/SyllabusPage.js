import React from 'react'
import Link from 'next/link'
import Grid from '@mui/material/Grid'
import { Typography, Container,Button } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import { useDispatch, useSelector } from 'react-redux'

// Custom
import style from './style/Syllabus.module.scss'
import ResourceForm from './ResourceFormIndex'
import GamesAndLearningContentList from './GamesAndLearningContentList'
import {
	clearImageAudioCoontent,
	clearVideoContent,
	clearContentDetailsCoverImage
} from '../../../redux/reducers/primary/LearningContentSlice'
import {
	clearNumberRangeForm,
	clearImageResourceForm,
	clearResourceDetails
} from '../../../redux/reducers/primary/GameResourcesSlice'
import {
	setResourceFormOpenStatus,
    clearAllFromResourceIndex
} from '../../../redux/reducers/primary/ResourceFormIndexSlice'
import SyllabusProcess from '../../../process/primary/SyllabusProcess'
import {
	clearShowActionBar
} from './../../../redux/reducers/primary/DeletePublishActionBarSlice'
import DeletePublishActionBar from './DeletePublishActionBar'
// End Custom
// 
const SyllabusPage = ({syllabus,subjectId,classId})=> {
	// console.log("syllabus",syllabus)
	const dispatch = useDispatch()

    let _hashTopicId;
    const hash = typeof window !== "undefined" ? window.location.hash : null
	if (hash){
		const hashArray = hash.split('#')
		_hashTopicId = hashArray[1]
	}
	const [	subtopicId, setSubtopicId ] = React.useState(0)
	const [ activeTopicId, setActiveTopicId ] = React.useState(_hashTopicId)
	const [ activeTopic, setActiveTopic ] = React.useState(null)
	const [ activeSubject, setActiveSubject ] = React.useState(null)
	const [ topics, setTopics ] = React.useState(null)
	const [ subtopics, setSubtopics ] = React.useState(null)
	const [ resourceContentId, setResourceContentId ] = React.useState(null)

	const {resource_form_open_status:resourceFormOpenStatus } = useSelector( state => state.resource_form_index )
	const {details_data } = useSelector( state => state.learning_content )

	React.useEffect(()=> {
		processSyllabusToipcs()
	},[_hashTopicId])

	const processSyllabusToipcs = ()=> {
		let topicId
		const subjectArray = syllabus.data.length > 0 ? syllabus.data[0] : null
		const topicsArray = (subjectArray && subjectArray.children.length && subjectArray.children.length > 0) ? subjectArray.children.filter(data=> data.type==='Topic') : null
		setTopics(topicsArray)
		setActiveSubject(subjectArray)

		//set active topic
		if (!window.location.hash){
			const _topics = topicsArray ? topicsArray[0] : null
			topicId = _topics ? topicsArray[0].id : ''
			window.location.hash = topicId;
			processActiveTopic(topicsArray,topicId)
			return false
		}
		processActiveTopic(topicsArray,_hashTopicId)

	}

	const processActiveTopic = (topics,topicId)=> {
		const topicArray = topics ? topics.filter(data=> data.id===topicId) : null
		setActiveTopic(topicArray)
	}

	const handleFormResource = (data=null,newStatus)=> {
		if (data){
		  setSubtopicId(data.id)
		}

		if (newStatus){
			setResourceContentId(null)
		}else{
			setResourceContentId(data)
		}
		
		//clear learning content form
		dispatch(clearImageAudioCoontent())
		dispatch(clearVideoContent())
		dispatch(clearContentDetailsCoverImage())

		//clear game resource form
		dispatch(clearResourceDetails())
		dispatch(clearNumberRangeForm())
		dispatch(clearImageResourceForm())

		//Clear from resource index form
		dispatch(clearAllFromResourceIndex())
		
		dispatch(clearShowActionBar())

		// Open resource form
		dispatch(setResourceFormOpenStatus())

	}

	return <div className={style.syllabusWrapper}>
	    <DeletePublishActionBar />
		{/* Resource form */}
			{resourceFormOpenStatus && (
				<ResourceForm 
					subjectId={ subjectId } 
					classId={ classId } 
					topicId={ _hashTopicId } 
					subtopicId={ subtopicId }
					resourceContentId = {resourceContentId}
					handleFormResource = { handleFormResource } />
			)}
		{/* End import resource form */}

		<div className={style.syllabusLayoutWapper}>

		  	  <div className={style.syllabusLayoutLeftWrapper}>

		  	  	 <div className={style.headerWrapper}>
		  	  	 	<Typography variant="h5"><b>{activeSubject ? activeSubject.content : ''}</b></Typography>
		  	  	 	<div>{activeSubject ? activeSubject.classs.name : ''}</div>
		  	  	 </div>
		  	  	 <div className={style.topicWrapper}>Topics</div>
		  	  	 <ul className={style.topicsListWrapper}>
		  	  	    {topics && topics.map(data => (
			  	  	 	<li key={data.id} className={(_hashTopicId===data.id) ? `${style.activeLink}` : ``}>
			  	  	 		<Link href={`/primary/syllabus/${subjectId}/${classId}#${data.id}`}>
			  	  	 			{data.content}
			  	  	 		</Link>
			  	  	 	</li>
		  	  	   	))}
		  	  	 </ul>

		  	  </div>
		  	  <div className={style.syllabusLayoutRightWrapper}>

		  	  	  {activeTopic && activeTopic.map(data => (
		  	  	  	<div key={data.id} className={style.topicWrapper}>
		  	  	  		<Typography variant="h5" className={style.topicHeader}><b>{data.content}</b></Typography>
		  	  	  		{data.children && data.children.map(_data=> (

				  	  	  		<div key={_data.id} className={`${style.subtopicWrapper} sd-flex-column`}>
				  	  	  			<div className={style.subtopicHeaderWrapper}>
				  	  	  				 <div className="sd-flex-column">
				  	  	  				 	<b>{_data.content}</b>
				  	  	  				 	{_data.children && _data.children.map(__data=> (
				  	  	  				 		<div className={style.conceptHeaderWapper} key={__data.id}>{__data.content}</div>
				  	  	  				 	))}
				  	  	  				 </div>
				  	  	  				 <div className={style.addButtonWrapper}>
				  	  	  				 	 <Button style={{ textTransform:"capitalize"}} variant="contained" onClick={ ()=> handleFormResource(_data,true) }>
				  	  	  				 	 	<AddIcon /> Add
				  	  	  				 	 </Button>
				  	  	  				 </div>
				  	  	  			</div>
				  	  	  		{/* Show games and resources */}
				  	  	  			<GamesAndLearningContentList resource_data = { _data.content_resource } handleFormResource = {handleFormResource }/>
				  	  	  		{/* End games and resources */}
				  	  	  		</div>
		  	  	  		))}
		  	  	  	</div>
		  	  	  ))}
		  	  </div>
		</div>

	</div>
}

export default SyllabusPage