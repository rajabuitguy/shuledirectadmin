import React from 'react'
import { Typography, Grid,Button } from '@mui/material'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import VideoLibraryIcon from '@mui/icons-material/VideoLibrary'
import AudioFileIcon from '@mui/icons-material/AudioFile'
import Session from 'react-session-api'
import { useDispatch, useSelector } from 'react-redux'
import AddSharpIcon from '@mui/icons-material/AddSharp'


// Custom
import {
	setDetailsData,
	setImageAudioContent,
	clearImageAudioCoontent,
	addNewImageAudioForm,
	clearVideoContent,
	updateAudioContentData,
	updateAudioCoverData,
	updateVideoContentData,
	updateContentDetailsCoverImage,
	clearContentDetailsCoverImage,
	removeImageAudioBlock,
	addNewVideoResource,
	removeVideoResource,
	addNewVideoContentCover
	} from '../../../redux/reducers/primary/LearningContentSlice'
import {
	  	clearNumberRangeForm,
	  	clearImageResourceForm,
	  	clearResourceDetails,
} from '../../../redux/reducers/primary/GameResourcesSlice'
import { 
	showMediaGallery,
	activeSegmentToUploadMedia 
} from '../../../redux/reducers/MediaGallerySlice'
import {
	setResourceContentType,
	setActiveContentToSave,
	clearActiveContentToSave,
	setFormSaveStatus
} from '../../../redux/reducers/primary/ResourceFormIndexSlice'
import style from './style/ResourceForm.module.scss'
import { TextForm,PasswordForm,HiddenForm } from '../../resource/Forms'
import Gallery from '../../gallery'
import ResourcesHelper from '../../../process/primary/ResourcesHelper'

// End Custom

const initActiveAction = {
		index_key: { parent_index: 0,child_index: 0 },
		active_source: '',
		is_active: null
	}

const LearningContentForm = ()=> {

	const dispatch = useDispatch()
	const { deleteLearningResource }  = ResourcesHelper()

	const detailsData = useSelector(state=> state.learning_content.details_data)
	const videoContent = useSelector(state => state.learning_content.video_content)
	const imageAudiContent = useSelector(state=> state.learning_content.image_audio_content)
	const { show_media_gallery:showMediaGalleryStatus,
			active_segment_to_upload_media } = useSelector(state=> state.media_gallery)

	const {	resource_parent_data:resourceParentData,
			active_content_to_save: activeContentToSave,
			resource_content_type: resourceContentType,
			resource_content_id:resourceContentId } = useSelector( state => state.resource_form_index )

	const [ value, setValue] = React.useState(detailsData.content_type)
	const [ showVideoUploadForm, setVideoUploadForm] = React.useState(true)
	const [ activeAction,setActiveAction ] = React.useState(initActiveAction)

	React.useEffect(()=> {
		dispatch(setDetailsData({ data:{
			...detailsData.data,
			subject_id: resourceParentData.subject_id,
			topic_id: resourceParentData.topic_id,
			classs_id: resourceParentData.classs_id,
			subtopic_id:resourceParentData.subtopic_id
		}}))

		// Clear content from game resource form
		dispatch(clearNumberRangeForm())
		dispatch(clearImageResourceForm())
		dispatch(clearResourceDetails())
		// End clear content from learning content form

	},[resourceParentData])

	React.useEffect(()=> {
		setValue(detailsData.content_type)
	},[detailsData])

	React.useEffect(()=> {
		if(value==="direct_video_upload"){
			setVideoUploadForm(true)
		}else{
			setVideoUploadForm(false)
		}
	},[value])

	React.useEffect(()=> {
		if (!activeAction.is_active){
			return false
		}
		processActiveLearningContentToSave()
	},[imageAudiContent,videoContent,detailsData])

  	const handleContentType = (event) => {
  		if (event.target.value==="direct_video_upload"){
		    dispatch(clearImageAudioCoontent())
  		    setVideoUploadForm(true)
  		}
  		else{
		    dispatch(clearVideoContent())
  		}
    	setValue(event.target.value)
		dispatch(setDetailsData({ content_type: event.target.value }))
		dispatch(clearActiveContentToSave())
		//reset the active action
		setActiveAction(initActiveAction)
  	}

	const handleFormChange = (prop) => (event) => {
		setActiveAction({
				index_key: { parent_index: 0, child_index: 0 },
				active_source: 'learning_content_update_details',
				is_active: true
			})
		let formValue = (prop==="unique_key_code") ? event.target.value.replace(/\s+/g, '') : event.target.value
		dispatch(setDetailsData({ data:{
			...detailsData.data,
			[prop]: formValue
		}}))

	}

	const processActiveLearningContentToSave = ()=> {
		let _activeConentData = null
		let contentType = ''
		let formOverlay = false
		let showSaveButton = false
        if (activeAction.active_source==="learning_content_imageaudio_cover_upload" || activeAction.active_source==="learning_content_audio_file_upload_button"){
        	contentType = value
        	showSaveButton = true
        	_activeConentData = [
        	    {
        			block_info: imageAudiContent.data[activeAction.index_key.parent_index].block_info,
        			content:  [imageAudiContent.data[activeAction.index_key.parent_index].content[activeAction.index_key.child_index]]
        	    }
        	]
        }else if (activeAction.active_source==="learning_content_video_file_upload"){
        	contentType = value
        	showSaveButton = true
        	_activeConentData = [
        		{
        			block_info: videoContent[activeAction.index_key.parent_index].block_info,
        		    media:videoContent[activeAction.index_key.parent_index].media 
        		}
        	]
		}else if (activeAction.active_source==="learning_content_video_cover_upload"){
			contentType = value
        	showSaveButton = true
        	_activeConentData = [
        		{
        			block_info: videoContent[activeAction.index_key.parent_index].block_info,
        		    media:{ 
						...videoContent[activeAction.index_key.parent_index].media,
						cover_resource: [videoContent[activeAction.index_key.parent_index].media.cover_resource[activeAction.index_key.child_index]]
					} 
        		}
        	]
        }else if (activeAction.active_source==="learning_content_details_cover_upload" || activeAction.active_source==="learning_content_update_details"){
        	contentType = "content_details",
        	formOverlay = true
        	showSaveButton = true
        	_activeConentData = [detailsData.data]
        }

        if (!_activeConentData){
        	return false
        }

        dispatch(setActiveContentToSave({
        		resource_type: resourceContentType.name,
        		content_type: contentType,
        		active_form_overlay: formOverlay,
        		show_form_save_button: showSaveButton,
        		data: _activeConentData
        	}))

        	controllingLearingAutoSaving()
	}

	const controllingLearingAutoSaving = ()=>{
		if (activeAction.active_source!=="learning_content_update_details"){ 
            dispatch(setFormSaveStatus())
        }
	}

	const handleShowGallery = (source,uniqueKeyId=null,childIndex=null)=> {

		if (source==='cancel_modal'){
			dispatch(showMediaGallery())
			return false
		}

		if (source!='cancel_modal'){
			setActiveAction({ index_key: { parent_index: uniqueKeyId,child_index: childIndex}, active_source: source,is_active: true })
			dispatch(clearActiveContentToSave())
		}

		if (source==="learning_content_details_cover_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "learning_content_details_cover_upload",
				media_type:"image",
				index:uniqueKeyId,
				active_status: true
			}))
		}
		else if (source==="learning_content_video_file_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "learning_content_video_file_upload",
				media_type:"video",
				index:uniqueKeyId,
				active_status: true
			}))
		}
		else if (source==="learning_content_imageaudio_cover_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "learning_content_imageaudio_cover_upload",
				media_type:"image",
				index:{
					parent_index: uniqueKeyId,
					child_index: childIndex
				},
				active_status: true
			}))
		}
		else if (source==="learning_content_audio_file_upload_button"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "learning_content_audio_file_upload_button",
				media_type:"audio",
				index:{
					parent_index: uniqueKeyId,
					child_index: childIndex
				},
				active_status: true
			}))
		}
		else if (source==="learning_content_video_cover_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "learning_content_video_cover_upload",
				media_type:"image",
				index:{
					parent_index: uniqueKeyId,
					child_index: childIndex
				},
				active_status: true
			}))
		}

		dispatch(showMediaGallery())

    }

    const removeImageAudioFormFields = async (index,blockInfo) => {

    	if (blockInfo.id===""){
	         dispatch(removeImageAudioBlock(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
		}else{
		    let delConf =  confirm("Are you sure you want to delete this?")
		    if (!delConf){
		    	return false
		    }

			let params = {
				baseId:detailsData.data.baseId,
				sourceId:detailsData.data.sourceId,
				blockId:blockInfo.id,
				indicator: "delete-block"
			}
	         dispatch(removeImageAudioBlock(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
			let delResult = await deleteLearningResource(params)
		}

  //   	dispatch(removeImageAudioBlock(index))
		// dispatch(clearActiveContentToSave())
		// setActiveAction(initActiveAction)
	}

	const _addNewImageAudioForm = ()=>{
		dispatch(addNewImageAudioForm())
		dispatch(clearActiveContentToSave())
		setActiveAction(initActiveAction)
	}

	const handleAddNewVideo = ()=> {
		dispatch(addNewVideoResource())
		dispatch(clearActiveContentToSave())
		setActiveAction(initActiveAction)
	}
	const handleNewVideoCover = (index)=> {
		dispatch(addNewVideoContentCover(index))
		dispatch(clearActiveContentToSave())
		setActiveAction(initActiveAction)
	 }

	const handleRemoveVideoResource = async (index,blockInfo )=> {

		if (blockInfo.id===""){
	         dispatch(removeVideoResource(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
		}else{
		    let delConf =  confirm("Are you sure you want to delete this Video?")
		    if (!delConf){
		    	return false
		    }

			let params = {
				baseId:detailsData.data.baseId,
				sourceId:detailsData.data.sourceId,
				blockId:blockInfo.id,
				indicator: "delete-block"
			}
	         dispatch(removeVideoResource(index))
	         setActiveAction(initActiveAction)
		     dispatch(clearActiveContentToSave())
			let delResult = await deleteLearningResource(params)
		}

	}

	

	return <div className={style.LearningContentRwapper}>
			{showMediaGalleryStatus && (
	        	<Gallery handleShowGallery = { handleShowGallery } />
	      	)}
		 
			<Grid className={style.learningContentFormWrapper} container spacing={2}>
			  <Grid item xs={12} md={4}>
			     <div className={style.contentLeftWrapper}>
					    <Typography variant="h5"><b>Learning Content</b></Typography>
			            <div className={style.innerContentForm}>
					     	 <div className="mt-10">
				                <TextForm
				                  handleOnChange={handleFormChange}
				                  value={ detailsData.data.name }
				                  fieldName="name"
				                  error = {null}
				                  label="Content name" />
				              </div>
				              <div className="mt-10">
				                <TextForm
				                  handleOnChange={handleFormChange}
				                  value={ detailsData.data.unique_key_code }
				                  fieldName="unique_key_code"
				                  error = {null}
				                  label="Unique key code (No space)" />
				                 
				              </div>
				              <div className="mt-20">
				                <label>Content Cover</label>
				                {detailsData.data.cover_image.map((data,index)=> (
				                	<React.Fragment key={index}>
					                	{ data.url === "" ? (
						                	<Button title="Add cover image" className={style.contentCoverImageWrapper} onClick = {  ()=> handleShowGallery('learning_content_details_cover_upload',index) }>
						                		<AddPhotoAlternateIcon sx={{ fontSize: 200,color:"#ddd" }} />
							                </Button>
							            ):(
							            	<Button title="Change cover image" className={style.contentCoverImage} onClick = {  ()=> handleShowGallery('learning_content_details_cover_upload',index) }>
							                	<img src={ data.url } />
							                </Button>
							            )}
				                	</React.Fragment>
				                ))}
				              </div>
				              
		                </div>
			     </div>
			  </Grid>
			  <Grid item xs={12} md={8}>
			      <div className={style.contentRightWrapper}>
			        
			        <div className={style.contentResourceTypeWrapper}>
			      	 	<div className={style.contentResourceTypeLeftWrapper}>
			      	 		<div>Content Type</div>
			      	 	</div>
			      	 	<div className={style.contentResourceTypeRightWrapper}>
				      	 	 <FormControl>
						      <RadioGroup
						        aria-labelledby="demo-controlled-radio-buttons-group"
						        name="content_type"
						        value={ value }
						        onChange={handleContentType}
						      >
						        <FormControlLabel value="direct_video_upload" control={<Radio />} label="Direct Video Upload" />
						        <FormControlLabel value="image_sound_upload" control={<Radio />} label="Image with respective sound" />
						      </RadioGroup>
						    </FormControl>
				      	</div>
			      	</div>

			      	<div className={style.contentRsourceFormTypeWrapper}>
			      	    
			      	    {showVideoUploadForm ? (
				      	    <div className={style.videoUploadFormWrapper}>
					      	    <div>Upload video</div>
					      	    {videoContent.map((data,index)=> (
					      	    	<div className={style.videoResourceListWrapper} key={index}>

					      	    	    <div className={style.videoResourceHeader}>
						   	    	        <div className={style.headerLeft}>{ data.media.file_name }</div>
							   	    	    <div className={style.removeVideoResourceWapper}>
										   	    <button onClick= { ()=> handleRemoveVideoResource(index,data.block_info) }>Remove</button>
					      		          	</div>
				      		          	</div>

										<div className={style.videoCoverWrapper}>
											<div>Cover</div>
										   <ul className={style.videoCoverListWrapper}>
											   {data.media.cover_resource && data.media.cover_resource.length > 0 && data.media.cover_resource.map((_data,_index)=> (
													<li key={_index}>
														 { !_data.url || _data.url ==="" ? (
															<Button title ="Add Cover" className={style.coverButton} onClick = { ()=> handleShowGallery('learning_content_video_cover_upload',index,_index) }>
																<AddPhotoAlternateIcon style={{color:"#ddd",fontSize:"50px"}} />
															</Button>
														 ):(
															<Button title ="Change Cover" className={style.coverButton} onClick = { ()=> handleShowGallery('learning_content_video_cover_upload',index,_index) }>
																<img src={_data.url} alt={ _data.file_name }  />
															</Button>
														 )}
												  	</li>
											   ))}

											   <li>
												   <Button className={style.addCoverButton} onClick={() => handleNewVideoCover(index)}>
												   		<AddSharpIcon />
												   </Button>
											   </li>

										   </ul>
										</div>
										<div className={style.videoButtonWrapper}>
											<div>{ data.media.url == "" ? "Add video" : "Change video" }</div>
											{ data.media.url ==="" ?(
												<Button title="Add video" className={style.contentResourceVideoWapper} onClick = { ()=> handleShowGallery('learning_content_video_file_upload',index) }>
													<VideoLibraryIcon sx={{ fontSize: 100,color:"#ddd" }} />
												</Button>
											):(
												<Button title="Change video" className={style.contentVideoPlayerWapper} onClick = { ()=> handleShowGallery('learning_content_video_file_upload',index) }>
													<div className={style.videoPlayer}>Video Player {index+1}</div>
												</Button>
											)}
										</div>
						      		</div>
					      	    ))}

					      	    <div>
				   	            	<Button style={{textTransform:"capitalize",marginTop:"20px"}} onClick={ handleAddNewVideo }> <AddSharpIcon /> Add new video</Button>
				   	            </div>
				      		</div>
			      		):(
			      		   <div>

			      		       <div>Image respective sound/audio</div>
				      		   { imageAudiContent.data && imageAudiContent.data.map((_element, index) => (
				      		   	  <React.Fragment key={index}>
					      		   { _element.content && _element.content.map((element, _index) => (
					      		   	    
						      		   	<div key={_index} className={ (imageAudiContent.active_label !="" && imageAudiContent.active_label === element.audio_cover.active_label) ? `${style.imageAudioUpoladFormLayout} ${style.activeLabel}` : style.imageAudioUpoladFormLayout }>
						      		         <div className={style.imageAudioUpoladFormLeftLayout}>
						      		         	 { element.audio_cover.url === "" ? (
								      		      	 <Button title="Add audio image" className={style.imageUploadWrapper} onClick = { ()=> handleShowGallery('learning_content_imageaudio_cover_upload',index,_index) }>
							                	      <AddPhotoAlternateIcon sx={{ fontSize: 90,color:"#ddd" }} />
								      		   	  	 </Button>
							      		   	  	 ):(
							      		   	  	   	 <Button title="Change audio image" className={style.imageUploadWrapper} onClick = { ()=> handleShowGallery('learning_content_imageaudio_cover_upload',index,_index) }>
							                	      	<img src={element.audio_cover.url} alt={element.audio_cover.file_name} />
								      		   	  	 </Button>
							      		   	  	 )}
						      		   	  	 </div>
							      		     <div className={style.imageAudioUpoladFormRightLayout}>
							      		          <div className={style.imageAudiRemoveForm}>
	  												  <button onClick={ ()=> removeImageAudioFormFields(index,_element.block_info) }>Remove</button>
							      		          </div>
							      		           { element.audio_content.file_name !=""
				      		      	  		        	? <div>Audio/Sound</div>
				      		      	  		        	: <div>Upload audio/sound</div>
				      		      	  		        }
							      		      	  <div className={style.audioUploadMainWapper}>
							      		      	  		<div className={style.audioUploadLeftWrapper}>

							      		      	  			
							      		      	  		   { element.audio_content.url === "" ? (
								      		      	  			<Button title="Add audio file" className={style.audioUploadIcon} onClick = { ()=> handleShowGallery('learning_content_audio_file_upload_button',index,_index) }>
								      		      	  				<AudioFileIcon sx={{ fontSize: 50,color:"#ddd" }} />
								      		      	  			</Button>
							      		      	  			):(
							      		      	  				<Button title="Change audio file" className={style.audioUploadIcon} onClick = { ()=> handleShowGallery('learning_content_audio_file_upload_button',index,_index) }>
								      		      	  				<AudioFileIcon sx={{ fontSize: 50,color:"#222" }} />
								      		      	  			</Button>
							      		      	  			)}
							      		      	  		</div>
							      		      	  		<div className={style.audioUploadRightWrapper}>
							      		      	  		    
							      		      	  		        { element.audio_content.file_name !=""
							      		      	  		        	? <div>{ element.audio_content.file_name }</div>
							      		      	  		        	: <div>No audio file</div>
							      		      	  		        }
							      		      	  		</div>
							      		      	  </div>

							      		     </div>
						      		   	</div>
					      		   ))}
				      		   	  </React.Fragment>
				      		   ))}

				      		   <div className={style.addNewFormWrapper}>
				      		   	  <Button style={{textTransform: 'capitalize'}} onClick={ _addNewImageAudioForm }>
				      		   	  	Add Form
				      		   	  </Button>
				      		   </div>

			      		   </div>
			      		)}
			      	</div>

			      </div>
			  </Grid>
			</Grid>
	</div>
}
export default LearningContentForm