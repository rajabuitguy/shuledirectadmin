import style from './style/Syllabus.module.scss'
import Link from 'next/link'
import {Typography } from '@mui/material'
import { Container } from '@mui/material'
import { SubjectsListSkeletonLoader } from '../../skeleton/SkeletonShapes'

const SubjectsPage = (props)=> {
	return <Container>
	    <div className={style.subjectWrapper}>
	    
	        {!props.subjects && (
	    		<SubjectsListSkeletonLoader width = "100%" height="200px" />
	    	)}

			<div className={style.subjectsListWrapper}>
				 {props.subjects && props.subjects.map((data,index)=> (
				 	<div key={data.id} className={style.subjectCardWrapper}> 
				 		 <Typography variant="h6">{data.name}</Typography>
		 		  		<ul>
                            {data.data.map((_data, index) => (
                                <li key={index}><a href={`/primary/syllabus/${_data.subject.id}/${_data.form.id}`}>{_data.form.name}</a></li>
                            ))}
                        </ul>
				 	</div>
				 ))}
			</div>
		</div>
	</Container>

}

export default SubjectsPage