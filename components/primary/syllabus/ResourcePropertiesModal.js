import React from 'react'
import { Button,Typography } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import style from './style/ResourceForm.module.scss'
import ArrowBackIosOutlinedIcon from '@mui/icons-material/ArrowBackIosOutlined'
import GameResourceOptionsAndProperties from './GameResourceOptionsAndProperties'
import {
    setInitialOptionProperties,
    setOpenOptionsPropertiesModal
} from '../../../redux/reducers/primary/GameResourcesSlice'

const ResourcePropertiesModal = ()=> {
    const { option_properties_data:propertiesData } = useSelector(state=> state.game_resources)
    const dispatch = useDispatch()
    const handleBackButton = () => {
        dispatch(setOpenOptionsPropertiesModal())
    }

    // console.log("propertiesData",propertiesData)

    return <div className={style.resourcePropertiesModalWrapper}>
         <div className={style.handleResourcePropertiesWrapper}>
             <div className={style.headerWrapper}>
                  <Button onClick={ handleBackButton } style={{ textTransform: "capitalize",color:"#000",width:"100px"}}>
                    <ArrowBackIosOutlinedIcon/>Back
                  </Button>
                  <Typography variant="h6">Properties</Typography>
             </div>
             <div className={style.propertiesInnerWrapper}>
                 <GameResourceOptionsAndProperties data={ propertiesData }/>
             </div>
         </div>
    </div>

}
export default ResourcePropertiesModal