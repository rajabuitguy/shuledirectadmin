import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Button } from '@mui/material'
import AddSharpIcon from '@mui/icons-material/AddSharp'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import AudioFileIcon from '@mui/icons-material/AudioFile'
import style from './style/ResourceForm.module.scss'
import { TextForm } from '../../resource/Forms'
import Gallery from '../../gallery'
import { 
	showMediaGallery,
	activeSegmentToUploadMedia 
} from '../../../redux/reducers/MediaGallerySlice'
import {
    addResourceNewProperties,
    removeResourcePropertyCard,
    setSaveResourceProperties
} from '../../../redux/reducers/primary/GameResourcesSlice'
import {
	setActiveContentToSave,
	setFormSaveStatus
} from '../../../redux/reducers/primary/ResourceFormIndexSlice'

const initActiveAction = {
    index: { 
        block_index: 0,
        option_index: 0,
        child_index: 0,
        option_type: "" 
    },
    option_type: "",
    is_active: null
}
const GameResourceOptionsAndProperties = (props)=> {
    const { show_media_gallery:showMediaGalleryStatus,
        active_segment_to_upload_media } = useSelector(state=> state.media_gallery)
        const { option_properties_data:propertiesData } = useSelector(state=> state.game_resources)
        const [ activeAction,setActiveAction ] = React.useState(initActiveAction)
        const dispatch = useDispatch()

        const {	active_content_to_save: activeContentToSave,
                resource_content_type: resourceContentType 
              } = useSelector( state => state.resource_form_index )
        const resourcesStepBlocks = useSelector(state=> state.game_resources.resource_steps_block)

        React.useEffect(()=> {

            if (!activeAction.is_active){
                return false
            }

            if (!propertiesData.save_properties){
                return false
            }

            processTheSelectedPropertiesToSave()
        },[propertiesData.save_properties])



    const handleOptionFormChange = (prop) => (e) => {
       
    }
    
     const handlePropertiesShowGallery = (source,index=null)=> {
		if (source==='cancel_modal'){
    		dispatch(showMediaGallery())
			return false
		}

		if (source!='cancel_modal'){
			setActiveAction({ 
                index: { 
                    block_index: index.block_index,
                    option_index: index.option_index,
                    child_index: index.child_index,
                    option_type: index.option_type
                }, 
                active_source: source,
                is_active:true 
            })
		}

		if (source==="properties_answer_resource_upload"){
			dispatch(activeSegmentToUploadMedia({
				source_name: "properties_answer_resource_upload",
				media_type:"image",
				index:index
			}))

		}else if (source==="properties_audio_resource_upload"){
            dispatch(activeSegmentToUploadMedia({
				source_name: "properties_audio_resource_upload",
				media_type:"audio",
				index:index
			}))
        }
		dispatch(showMediaGallery())
       
    }

    const processTheSelectedPropertiesToSave = ()=> {
            let index = activeAction.index
            if (resourcesStepBlocks[index.block_index].options_array.length === 0){
                return false
            }

            const blockArray = {
                block_info: resourcesStepBlocks[index.block_index].block_info,
                options_array: []
            }

            resourcesStepBlocks[index.block_index].options_array.forEach((data, _index)=> {
              if (_index===index.option_index){
                    let _dataArray = null
                    if (index.option_type==="question"){
                        _dataArray = {
                            ...data,
                            question: {
                                ...data.question,
                                properties: [data.question.properties[index.child_index]]
                            }
                        }
                    }else if (index.option_type==="answer"){
                        _dataArray = {
                            ...data,
                            answer: {
                                ...data.answer,
                                properties: [data.answer.properties[index.child_index]]
                            }
                        }
                    }
                    blockArray.options_array.push(_dataArray)
              }
            })

            // console.log("blockArray",blockArray)
            // return false


            dispatch(setActiveContentToSave({
                ...activeContentToSave,
                resource_type: resourceContentType,
                content_type: "image_game_resource",
                data: [blockArray]
            }))
            dispatch(setFormSaveStatus())
			dispatch(setSaveResourceProperties())
          
    }


    const handleAddNewPropertyOption = ()=> {
        dispatch(addResourceNewProperties({
            block_index: props.data.parent_data.block_index,
            option_index: props.data.parent_data.option_index,
            option_type: props.data.parent_data.option_type
        }))
    }
    const handleRemovePropertCard = (index)=> {
        dispatch(removeResourcePropertyCard({
            block_index: props.data.parent_data.block_index,
            option_index: props.data.parent_data.option_index,
            option_type: props.data.parent_data.option_type,
            child_index: index
        }))
    }


    return <div className={style.resourceTableWrapper} >
            {showMediaGalleryStatus && (
                <Gallery handleShowGallery = { handlePropertiesShowGallery } />
            )}
            <div className={style.tableLeftColumnWrapper}>

            <div className={style.tableCellWrapper}>
                <div className={`${style.textWrapper} p-10`}>Name</div>
                <div className={`${style.tableCell} p-10`}>Image property resources options</div>
                <div className={`${style.tableCell} p-10`}>Audio property resource options</div>
            </div>

            </div>
            <div className={style.tableRightColumnWrapper}>

            {props.data.data && props.data.data.length>0 && props.data.data.map((_data, _index) => (
                <div key={_index}>
                    
                    <div className={style.tableCellWrapper}>
                        <div className={style.optionRemoveButtonWrapper} onClick={ ()=> handleRemovePropertCard(_index) }>
                            <button title="Remove card">x</button>
                        </div>
                        <div className={`${style.textWrapper}`}>
                            <div>
                                <TextForm
                                    index =  ""
                                    handleOnChange={ handleOptionFormChange }
                                    value={ _data.content.name }
                                    fieldName="name"
                                    error = {null}
                                    label="Name" />
                            </div>
                        </div>
                        <div className={style.tableCell}>
                            { !_data.answer.url || _data.answer.url === "" ? (
                                <Button title="Add image" className={style.questionImageResource} onClick = {  ()=> handlePropertiesShowGallery('properties_answer_resource_upload',{
                                    block_index: props.data.parent_data.block_index,
                                    option_index: props.data.parent_data.option_index,
                                    child_index: _index,
                                    option_type: props.data.parent_data.option_type,
                                    property_source: "answer"
                                }) }>
                                        <AddPhotoAlternateIcon style={{color:"#ddd",fontSize:"80px"}} />
                                </Button>
                            ):(
                                <Button title="Change image" className={style.questionImageResource} onClick = {  ()=> handlePropertiesShowGallery('properties_answer_resource_upload',{
                                    block_index: props.data.parent_data.block_index,
                                    option_index: props.data.parent_data.option_index,
                                    child_index: _index,
                                    option_type: props.data.parent_data.option_type,
                                    property_source: "answer"
                                }) }>
                                    <img src={_data.answer.url} alt={ _data.answer.file_name } />
                                </Button>
                            )}
                            
                        </div>
                        <div className={style.tableCell}>
                            { !_data.audio.url || _data.audio.url === "" ? (
                                <Button title="Add audio" className={style.questionImageResource} onClick = {  ()=> handlePropertiesShowGallery('properties_audio_resource_upload',{
                                    block_index: props.data.parent_data.block_index,
                                    option_index: props.data.parent_data.option_index,
                                    child_index: _index,
                                    option_type: props.data.parent_data.option_type,
                                    property_source: "audio"
                                }) }>
                                        <AudioFileIcon style={{color:"#ddd",fontSize:"50px"}} />
                                </Button>
                            ):(
                                <Button style={{textTransform:"capitalize"}} title="Change audio" className={style.questionImageResource} onClick = {  ()=> handlePropertiesShowGallery('properties_audio_resource_upload',{
                                    block_index: props.data.parent_data.block_index,
                                    option_index: props.data.parent_data.option_index,
                                    child_index: _index,
                                    option_type: props.data.parent_data.option_type,
                                    property_source: "audio"
                                }) }>
                                    <div className={style.audioResourceWrapper}>
                                        <div>{_data.audio.file_name}</div>
                                        <div className={style.audioResourceBottomWrapper}>
                                            <AudioFileIcon style={{color:"#333",fontSize:"30px"}} />
                                        </div>
                                    </div>
                                </Button>
                            )}
                            
                        </div>
                    </div>
                </div>
            ))}
            <div className={style.tableCellWrapper}>
                <Button className={style.addChoicesPropertiesButtonWrapper} onClick={ handleAddNewPropertyOption }>
                    <AddSharpIcon /> Add Options
                </Button>
            </div>

            </div>
        </div>

}
export default GameResourceOptionsAndProperties