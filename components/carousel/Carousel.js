import React from 'react'
import { Typography, Container, Button } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Link from 'next/link'
import LocalData from '../../helper/LocalData';
import Subjects from '../../process/Subjects';
// Begin: Custom
import styles from './styles/Carousel.module.scss'
import { CarouselImage } from '../resource/Resources'
// End: Custom

const Carousel = (props,{scrollToSection})=> {

    const [subjects, setSubjects] = React.useState(null)
    React.useEffect(() => {
        if (!subjects) {
            getSubjects()
        }
    }, [subjects])

    const getSubjects = async () => {
        const subjectsArray = await Subjects.findSubjects(null)
        const arrayData = []
        for (let subject in subjectsArray) {
            let __default = subjectsArray[subject][0].subject.subject_default
            let image = (__default.length !== 0) ? __default.image : null

            let subjectDiv = subject.replace(/\s+/g, '')
            let result = {
                image: image,
                style_name: subjectDiv,
                name: (subject === 'Information and Computer Studies') ? "ICS" : subject,
                data: subjectsArray[subject],
            }
            arrayData.push(result)
        }
        setSubjects(arrayData)
        LocalData.setClasses(arrayData)
    }

    return <div className={styles.mainContainer}>
        <Container>
            <div className={styles.carouselWrapper}>
                <div className="mb-10">
                    <Typography className="" variant="h4">Learn. Revise. Discuss.</Typography>
                </div>
                <div className={styles.minTitleContainer}>
                    <Typography variant="h6">What would you like to learn?</Typography>
                </div>
                {props.subjects && props.subjects.map((data, index) => {
                    let seconds  = index+2
                    if (index > 7){
                        seconds = index-6
                    }
                    return(
                        <div key={index} className={`${styles.cardContainer} ${styles.effectHover} bounce-in-fwd-${seconds}s`}>
                            <div className={`${styles.cardFront} ${data.style_name}`}>
                                <div className={styles.subjectImageContainer}>
                                    <Typography variant="h6" className={styles.subjectName}>{data.name}</Typography>
                                    <div className={styles.subjectIcon}>
                                        {data.image && (<img src={data.image} alt={data.name} />)}
                                    </div>
                                </div>
                            </div>
                            <div className={`${styles.cardBack} ${data.style_name}`}>
                                <Typography variant="h6" className={styles.subjectName}>{data.name}</Typography>
                                <ul>
                                    {data.data.map((_data, index) => (
                                        <li key={index}><Link href={`/notes/list_notes/${_data.form.id}/${_data.subject.id}`}>{_data.form.name}</Link></li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    )
                })}

                <div className={styles.moreArrowWrapper}>
                    <Button className={styles.arrowButton} onClick={props.scrollToSection} label="Click to scroll down to view more">
                        <KeyboardArrowDownIcon />
                    </Button>
                </div>
            </div>
        </Container>
        <CarouselImage />
    </div>

}

export default Carousel
