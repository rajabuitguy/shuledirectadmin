import { Typography,Button } from '@mui/material'
import Image from 'next/image'
import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { useRouter } from 'next/router'
import Link from 'next/link'
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp'
import { connect } from 'react-redux';

// Begin: Custom
import styles from './style/ProfilePage.module.scss'
import FeaturesCards from '../common/features/FeaturesCards'
import { RoundImage } from '../resource/Resources'
import userImage from '../../public/assets/images/sampleimage.png'
// End: Custom

import useAuth from '../common/useAuth';
import {useSession} from 'next-auth/client'
//commented for user

const TabPanel = (props) =>{
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
}

const a11yProps = (index) =>{
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

const tabsArray = [
   {
     id:0,
     name:"Quick Links",
     tab: 'quickLinks'
   },
   {
     id:2,
     name:"Discussion",
     tab: 'discussion'
   }
]



const ProfilePage = ({userData,dispatch})=> {
    const router = useRouter()
    const {user} = useAuth()
    const userId = userData.id
    const activeUrlTab = (typeof router.query.param[1] ==='string') ? router.query.param[1] : null
    const [ showWallet, setShowWallet ] = React.useState(false)
    const [ walletArray, setWalletArray ] = React.useState(false)
    const [ value, setValue ] = React.useState(0)
    const [session,loading] = useSession()
    const [renderStatus,setRenderStatus] = React.useState(false)
    const [profilepageRender,setProfilepageRender] = React.useState(false)
    const sectionScrollTarget = React.useRef(null)
    

    React.useEffect(()=> {
      checkActiveTab()
      handleWalletOpen()
      if (!walletArray)
      {
        getUserWallet()
      }
    },[activeUrlTab])
   

    const checkActiveTab = ()=> {
      let currentTabArray = tabsArray.filter(data=> data.tab === activeUrlTab)
      if (currentTabArray.length > 0)
      {
        const value_ = currentTabArray[0].id
        setValue(value_)
      }
      else{
        setValue(0)
      }
    }

    const handleChange = (event, newValue) => {
      setValue(newValue);
    }

    const handleWalletOpen = ()=> {
        if (activeUrlTab === 'wallet')
        {
            setShowWallet(!showWallet)
        }
    }

    const getUserWallet = async (userId)=> {
          const _walletResult = await Wallet.findUserWallet(userId)
          if (!_walletResult)
          {
             return false
          }
          setWalletArray(_walletResult)
    }

  return <>

      {showWallet && (
        <WalletPage user={ userData } handleWalletOpen = { handleWalletOpen } walletArray = { walletArray }/>
      )}

       <div className={styles.profilePageWrapper}>
          <div className={styles.profileInnerWrapper}>
              <div className={styles.bannerWrapper}>
                  <div className={styles.userAvatarWrapper}>
                      <RoundImage size="120" name="image" url={userImage}/>
                  </div>
                  <div className={`${styles.userDetailsWrapper} p-20`}>
                      <Typography className={styles.h4} variant="h4">{userData && userData.first_name} {userData && userData.last_name}</Typography>
                      <Typography className={styles.h6} variant="h6">Form 1 {userData.profile_type.name} {userData.schools ? `at ${userData.schools.name}` : ""}</Typography> 
                      <ul className={styles.pointsWalletBoardWrapper}>
                          {walletArray && walletArray.map((_data, index) => (
                              <React.Fragment key={index}>
                                 {_data.wallet_types.name==="Points" && (
                                     <li>{ _data.balance_after} Points</li>
                                 )}

                                 {_data.wallet_types.name==="Coins" && (
                                     <li>{ _data.balance_after} Coins</li>
                                 )}
                              </React.Fragment>
                          ))}

                            <li>
                                <Button onClick={ handleWalletOpen }>
                                    <Link href={`/users/student/${userData.id}/wallet`} passHref>
                                      <div className="sd-flex">Wallet <ArrowForwardIosSharpIcon /></div>
                                    </Link>
                                </Button>
                            </li>
                      </ul>
                  </div>
              </div>
              <div ref={sectionScrollTarget}>
                <div style={{padding:'0px 20px'}}>
                  <Box sx={{ borderTop: 1, borderColor: 'divider' }}>
                    <Tabs value={value}
                          onChange={handleChange}
                          aria-label="navigation menu"
                          scrollButtons={true}
                          allowScrollButt onsMobile
                          centered>
                       { tabsArray.map((data,index)=> (
                         <Tab label={data.name} {...a11yProps(`${index}`)} key = {index}/>
                       ))}
                    </Tabs>
                  </Box>
                </div>
                <TabPanel value={value} index={0}>
                    <FeaturesCards />
                </TabPanel>
                <TabPanel value={value} index={1}>
                </TabPanel>
                <TabPanel value={value} index={2}>
                   Discussion content
                </TabPanel>
                <TabPanel value={value} index={3}>
                   Bookmark
                </TabPanel>
              </div>

          </div>
       </div>

    </>

}

function mapStateToProps(state){
  return{
    state
  }
}
export default connect(mapStateToProps)(ProfilePage)
