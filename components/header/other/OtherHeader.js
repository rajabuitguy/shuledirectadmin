import { Container,Button } from '@mui/material'
import { useRouter } from 'next/router'

// Begin: Custom
import styles from './style/OtherHeader.module.scss'
import { SdLogo } from '../../resource/Resources'
// End: Custom
const OtherHeader = ({showCloseIcon})=> {

    const router = useRouter()

    const handleClick = ()=> {
       router.back()
    }

    return <div className={styles.headerWrapper}> 
                <Container>
                    <div className="sd-flex">
                       <div className="sd-flex-column">
                        <SdLogo />
                       </div>
                       <div>
                          {showCloseIcon && (
                             <Button onClick={ handleClick }>
                                <button className="cancel-button">x</button>
                             </Button>
                          )} 
                       </div>
                    </div>
                </Container>
            </div>
}
export default OtherHeader