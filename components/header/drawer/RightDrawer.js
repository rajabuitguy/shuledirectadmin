import { Divider,List,Box,ListItem,ListItemIcon,ListItemText,Typography,Button } from '@mui/material'
import AccountBoxSharpIcon from '@mui/icons-material/AccountBoxSharp'
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts'
import ExitToAppIcon from '@mui/icons-material/ExitToApp'
import BookmarkSharpIcon from '@mui/icons-material/BookmarkSharp'
import FavoriteSharpIcon from '@mui/icons-material/FavoriteSharp';
import Link from 'next/link'
import { useRouter } from 'next/router'
import { signOut } from 'next-auth/client'
import React from 'react'
import ModeEditSharpIcon from '@mui/icons-material/ModeEditSharp'

// Begin: Custom
import styles from './styles/Drawer.module.scss'
import User from '../../../process/User'
import userImage from '../../../public/assets/images/sampleimage.png'
import { RoundImage,SdLogo } from '../../resource/Resources'
// End: Custom

const RightDrawer = ({anchor, handleRightDrawerToggle, user}) => {

		const router = useRouter()
		const [loading,setLoading] = React.useState(false)

		/**
		 * [description]
		 * @return {[type]} [description]
		 */
		const handleLogOut = ()=> {
			setLoading(true)
	 		signOut({
	 			// callbackUrl: `${window.location.origin}/`
	 			callbackUrl: `http://${window.location.host}/redirect`
	 		})
	 	}
	 	//----------------------------------------------------------------------------------------

		return <Box
				 sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
				 role="presentation"
				 onClick={handleRightDrawerToggle(anchor, false)}
				 onKeyDown={handleRightDrawerToggle(anchor, false)}
				 className={styles.drawerConatiner}
			 	>
				 <div className={`${styles.userInfoContainer} sd-flex`}>
					 <div className="">
						<RoundImage size="70" url={userImage} name="image name"/>
					 </div>
					 <div className="sd-flex-column p-10">
						<Typography variant="h6">{user.data.first_name} {user.data.last_name}</Typography>
						<span>{user.data.profile_type.name}</span>
					 </div>
				 </div>
				 <div style={{padding:"0 20px 10px",cursor:"pointer"}}>
				 	<Link href="/auth/edit">
				 	    <div>
				 			<ModeEditSharpIcon /> Edit Profile 
				 		</div>
				 	</Link>
				 </div>
				 <Divider/>
				 <div className={styles.drawerLogoutButtonContainer}>
				   <Divider/>
				    {!loading ? (
						 <ListItem button onClick= { handleLogOut }>
							 <ListItemIcon className={styles.listIcon}><ExitToAppIcon /></ListItemIcon>
							 <ListItemText primary="Sign Out" />
						 </ListItem>
					):(
						<center style={{
							padding:'15px'
						}}>Wait! signing you out ... </center>
					)}
				 </div>

		 </Box>
}
export default RightDrawer
