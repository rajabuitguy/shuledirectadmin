import React from 'react'
import styles from './styles/Nav.module.scss'
import { Typography,Button } from '@mui/material'
import Link from 'next/link'
import MenuIcon from '@mui/icons-material/Menu'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'

// Begin:Custom
import { RoundImage } from '../../resource/Resources'
import userImage from '../../../public/assets/images/sampleimage.png'
// End:Custom

const Nav = ({handleRightDrawerToggle,user})=> {

  const [state, setState]  = React.useState(false)
  const onMouseOver = ()=> {
   		setState(true)
   }

   const onMouseOut = ()=> {
   		setState(false)
   }

   const showMenuOnMobile = ()=> {
   	   if (!state){
   		    setState(true)
   	   }else{
   		    setState(false)
   	   }
   } 

	return <div className={styles.navigationWrapper}>
		<div className="sd-flex-column">
			<ul className={styles.mainMenuWrapper}>
				<li className={styles.classroom} onClick={ ()=> showMenuOnMobile() }>
				    <a href="javascript:void(0)" aria-label="Click to select from Classroom menu">
				        Classroom <KeyboardArrowDownIcon />
				    </a>
			   		{ state && (
						<div className={ styles.classroomWrapper } onMouseLeave={ ()=> onMouseOut() } aria-label="Classroom menu">
							 <div className={styles.subMenuWrapper}>
							 	 
							 	<div className={styles.authOnMobileWrapper}>
							 	 	   {!user.isLoggedIn ? (
												<ul className={styles.authButtonWrapper}>
													<li><a href="/auth/login">Sign In</a></li>
													<li className={styles.activeButton}><a href="/auth/register" >Register</a></li>
												</ul>
											):(
							       	 	<Button
								            color="inherit"
								            aria-label="Open user right menu"
								            edge="end"
														onClick={ handleRightDrawerToggle('right', true) }
														style={{ textTransform:"capitalize"}}
													>
														<RoundImage size="80" name="image" url={userImage}/>
														<div className="p-10">
															 Hi, { user.data.first_name }
														</div>
								         </Button>
						         	)}
							 	 </div>

							 	 <ul className={ styles.subMenuListWrapper}>
							 	 	<li><a href="#">Classes</a></li>
							 	 	<li><a href="">Form 1</a></li>
							 	 	<li><a href="">Form 2</a></li>
							 	 	<li><a href="">Form 3</a></li>
							 	 	<li><a href="">Form 4</a></li>
							 	 </ul>
							 	 <ul className={ styles.subMenuListWrapper}>
							 	 	<li><a href="#">Test Yourself</a></li>
							 	 	<li><a href="">Makini Quizzes</a></li>
							 	 	<li><a href="">Past Papers</a></li>
							 	 </ul>
							 	 <ul className={ styles.subMenuListWrapper}>
							 	 	<li><a href="#">Extracurricular <br />Subjects</a></li>
							 	 	<li><a href="">Life Skills</a></li>
							 	 	<li><a href="">Girls Leadership</a></li>
							 	 </ul>
							 	 <ul className={ styles.subMenuListWrapper}>
							 	 	<li><a href="#">More</a></li>
							 	 	<li><a href="">Discussion</a></li>
							 	 	<li><a href="">Mkali wa Mapindi</a></li>
							 	 	<li><a href="">Practical Videos</a></li>
							 	 	<li><a href="">Periodic Tables</a></li>
							 	 	<li><a href="">Ticha Kidevu&apos;s Shop</a></li>
							 	 	<li><a href="">Syllabus</a></li>
							 	 	<li><a href="">Teachers</a></li>
							 	 </ul>

							 </div>

						</div>
					)}
				</li>
				<li><a href="#" onMouseEnter= { ()=> onMouseOut() }>Teachers</a></li>
        		<li><a href="#" onMouseEnter= { ()=> onMouseOut() }>Syllabus</a></li>
			</ul>
		</div>
		<div>
			<div className={styles.webViewWrapper}>
					{!user.isLoggedIn ? (
						<ul className={styles.authButtonContainer}>
							<li><a href="/auth/login" >Sign In</a></li>
							<li className={ styles.activeButton }><a href="/auth/register" >Register</a></li>
						</ul>
					):(
	       	 	<Button
		            color="inherit"
		            aria-label="Open user right menu"
		            edge="end"
								onClick={ handleRightDrawerToggle('right', true) }
								style={{ textTransform:"capitalize"}}
							>
								<div className="p-10">
									 Hi, { user.data.first_name }
								</div>
								<RoundImage size="40" name="image" url={userImage}/>
		         </Button>
	         	)}
			</div>
			<div className={styles.mobileViewWrapper}>
				<Button sx={{color:"#ccc"}} onClick={ ()=> showMenuOnMobile() } className={ styles.menuButtom }>
				  	{ !state ? (
							<MenuIcon className={ styles.menuIcon }/>
						):(
				  		<button className={styles.cancelMenuOnMobile}>X</button>
						)}
				</Button>
			</div>
		</div>
	</div>
}

export default Nav
