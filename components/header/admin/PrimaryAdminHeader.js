import * as React from 'react';
import PropTypes from 'prop-types';
import InboxIcon from '@mui/icons-material/MoveToInbox'
import MailIcon from '@mui/icons-material/Mail'
import MenuIcon from '@mui/icons-material/Menu'
import Link from 'next/link'
import HomeSharpIcon from '@mui/icons-material/HomeSharp'
import PermMediaSharpIcon from '@mui/icons-material/PermMediaSharp'
import { useDispatch, useSelector } from 'react-redux'
import {
  AppBar, Box, CssBaseline,
  Divider, Drawer, IconButton,
  List, ListItem, ListItemIcon,
  ListItemText,
  Typography, Toolbar, Button
} from '@mui/material'

// Begin: Custom
import styles from './styles/AdminHeader.module.scss'
import userImage from '../../../public/assets/images/sampleimage.png'
import { RoundImage, SdLogo, SdKidsLogo } from '../../resource/Resources'
import User from '../../../process/User'
import RightDrawer from '../drawer/RightDrawer'
import useAuth from '../../common/useAuth'
import Gallery from '../../gallery'
import LanguageSwitch from '../../language/LanguageSwitch'
import {
  clearActiveSegmentToUploadMedia
} from '../../../redux/reducers/MediaGallerySlice'
// End: Custom

const drawerWidth = 240;

const AdminSideMenu = [
  {
    name: 'Home',
    url: "/primary",
    icon: <HomeSharpIcon />
  },
  {
    name: 'Subjects',
    url: "/primary/subjects",
    icon: <HomeSharpIcon />
  },
]
const SideMenuArray = AdminSideMenu

function ResponsiveDrawer(props) {
  const dispatch = useDispatch()
  const { user } = useAuth()
  const [showGallery, setShowGallery] = React.useState(null)

  const { window } = props
  const [mobileOpen, setMobileOpen] = React.useState(false)
  const [open, setOpen] = React.useState(false)
  const [state, setState] = React.useState({
    right: false
  })

  const handleRightDrawerToggle = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setState({ ...state, [anchor]: open })
  }

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  }

  const handleShowGallery = () => {
    dispatch(clearActiveSegmentToUploadMedia())
    setShowGallery(!showGallery)
  }

  const drawer = (
    <div className={styles.adminLeftMenu}>
      <div className="p-5">
        <Link href="/">
          <div style={{ cursor: "pointer" }}>
            <SdKidsLogo />
          </div>
        </Link>
      </div>
      <List>
        {SideMenuArray.map((data, index) => (
          <Link href={data.url} key={data.name}>
            <ListItem button>
              <ListItemIcon className={styles.listIcon}>{data.icon}</ListItemIcon>
              <ListItemText primary={data.name} />
            </ListItem>
          </Link>
        ))}
      </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return <div>

    {showGallery && (
      <Gallery handleShowGallery={handleShowGallery} />
    )}

    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
        }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography className="sd-flex-column" variant="h5" noWrap component="div">
            Primary Admin Dashboard
          </Typography>
          <div className="sd-flex">
            <div className="sd-flex-column">
              <LanguageSwitch />
            </div>
            <Button onClick={handleShowGallery} style={{ width: "50px" }}>
              <PermMediaSharpIcon />
            </Button>
          </div>
          <div>
            {user.isLoggedIn && (
              <Button
                color="inherit"
                aria-label="open drawer"
                edge="end"
                onClick={handleRightDrawerToggle('right', true)}
                style={{ textTransform: "capitalize" }}
              >
                <div className="p-10">
                  Hi, {user.data.first_name}
                </div>
                <RoundImage size="40" name="image" url={userImage} />
              </Button>
            )}
          </div>
        </Toolbar>
      </AppBar>
      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>

      {/* Begin:right drawer Menu */}
      {user.isLoggedIn && (
        <div >
          {['right'].map((anchor) => (
            <React.Fragment key={anchor}>
              <Drawer
                anchor={anchor}
                open={state[anchor]}
                onClose={handleRightDrawerToggle(anchor, false)}
                className="right-drawer-wrapper"
              >
                <RightDrawer achor={anchor} handleRightDrawerToggle={handleRightDrawerToggle} user={user} />
              </Drawer>
            </React.Fragment>
          ))}
        </div>
      )}
      {/* End:right drawer Menu */}

    </Box>
  </div>
}

export default ResponsiveDrawer;
