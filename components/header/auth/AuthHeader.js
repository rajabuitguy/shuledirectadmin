import Link from 'next/link'

// Begin:Custom
import styles from './styles/AuthHeader.module.scss'
import { SdLogo } from '../../resource/Resources'
// End:Custom

const AuthHeader = ()=> {
    return <div className={styles.mainConatiner}>
            <a href="/">
                <div className={styles.logoContainer}>
                   <SdLogo />
                </div>
            </a>
    </div>
}
export default AuthHeader
