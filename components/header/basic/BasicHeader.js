import * as React from 'react'
import { styled, useTheme } from '@mui/material/styles'
import MuiAppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import CssBaseline from '@mui/material/CssBaseline'
import MenuIcon from '@mui/icons-material/Menu'
import { useSession } from 'next-auth/client'

import Link from 'next/link'
import {  Container,
					Typography,
					Button,ListItem,
					ListItemText,
					List,Divider,
					ListItemIcon,
					Box,Drawer,
					IconButton
				} from '@mui/material'

// Begin: Custom
import styles from './styles/BasicHeader.module.scss'
import { SdLogo } from '../../resource/Resources'
// import User from '../../../process/User'
import RightDrawer from '../drawer/RightDrawer'
import useAuth from '../../common/useAuth'
import Nav from '../navigation/Nav'
// End: Custom

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({}))

const BasicHeader = ()=> {

	const { user } = useAuth()
  // const [session, loading ] = useSession()

  const theme = useTheme()
  const [open, setOpen] = React.useState(false)
	const [state, setState] = React.useState({
    right: false
  })

	const handleRightDrawerToggle = (anchor, open) => (event) => {
		 if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			 return;
		 }
		 setState({ ...state, [anchor]: open })
 	}

  return <div>
	    <Box sx={{ display: 'flex' }}>
	      <CssBaseline />
		      <AppBar position="fixed" open={open} className={styles.headerContainer}>
						<Container disableGutters={true}>
						  <Toolbar>
								<Typography variant="h6" noWrap sx={{width:"260px"}} component="div">
									<a href="/">
										<div  style={{ cursor: "pointer"}}>
											<SdLogo />
										</div>
									</a>
								</Typography>
								<div className="sd-flex-column">
									<Nav handleRightDrawerToggle={ handleRightDrawerToggle } user={ user }/>
								</div>
			        	</Toolbar>
					</Container>
		      </AppBar>
		  </Box>

	  	{/* Begin:right drawer Menu */}
		    {user.isLoggedIn && (
					<div className="right-drawer-wrapper">
			      {['right'].map((anchor) => (
			        <React.Fragment key={anchor}>
			          <Drawer
			            anchor={anchor}
			            open={state[anchor]}
			            onClose={handleRightDrawerToggle(anchor, false)}
			            className="right-drawer-wrapper"
			          >
			           <RightDrawer anchor = { anchor } handleRightDrawerToggle = { handleRightDrawerToggle } user = { user }/>
			          </Drawer>
			        </React.Fragment>
			      ))}
		    	</div>
	    	)}
	  	{/* End:right drawer Menu */}


		</div>
}

export default BasicHeader
