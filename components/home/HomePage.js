import React from 'react'
import { Typography,Container,Grid } from '@mui/material'
import MenuBookSharpIcon from '@mui/icons-material/MenuBookSharp'
import QuizSharpIcon from '@mui/icons-material/QuizSharp'
import PeopleAltSharpIcon from '@mui/icons-material/PeopleAltSharp'
import CheckSharpIcon from '@mui/icons-material/CheckSharp'

import Link from 'next/link'
import Image from 'next/image'

// Begin:Custom
import styles from './styles/Home.module.scss'
import Carousel from '../carousel/Carousel'
import { CarouselImage } from '../resource/Resources'
// End: Custom

const featuresList = [
	{
		"icon":<MenuBookSharpIcon className={styles.icon} />,
		"name":"Study notes",
		"description":"Access notes for secondary school subjects",
		"url":"/",
		"button_name":"Study now"
	},
	{
		"icon":<QuizSharpIcon className={styles.icon} />,
		"name":"Test yourself",
		"description":"Try Ticha Kidevu's quizzes. Answer thousands of questions",
		"url":"/",
		"button_name":"Accesss quizzes"
	},
	{
		"icon":<PeopleAltSharpIcon className={styles.icon} />,
		"name":"Network",
		"description":"Share knowledge with over students on Shule Direct",
		"url":"/",
		"button_name":"Meet"
	},
	{
		"icon":<CheckSharpIcon className={styles.icon} />,
		"name":"Revise",
		"description":"Download all NECTA papers and access the marking scheme",
		"url":"/",
		"button_name":"View past papers"
	}
]
const HomePage = (props)=> {

	const seectionScroll = React.useRef(null)
	const scrollToSection   = ()=> {
		window.scrollTo({
			top: seectionScroll.current.offsetTop,
			behavior: 'smooth'
		})
    }

	return <div>

			{/* Begin:Import carousel */}
         	<Carousel subjects={props.subjects} scrollToSection={ scrollToSection }/>
			{/* End:Import carousel */}

			<div className={styles.homePageWrapper} ref={seectionScroll}>
				<div className={styles.segmentWrapper}></div>
				<div className={styles.featuresWrapper}>
					<Container>
					<ul>
						{featuresList.map((data,index)=> (
							<li key={index}>
								<div className={styles.featureIcon}>{data.icon}</div>
								<div>
									<Typography variant="h5">{data.name}</Typography>
								</div>
								<div className={styles.featureDescriptions}>
									{data.description}
								</div>
								<div>
									<Link href={data.url}>{data.button_name}</Link>
								</div>
							</li>
						))}
					</ul>
					</Container>
				</div>

				<div className={styles.testimonialsWrapper}>
					<div className={styles.testimonialsInnerWrapper}></div>
					<CarouselImage />
				</div>
			</div>
		</div>

}

export default HomePage
