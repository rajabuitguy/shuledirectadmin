import { useSession } from 'next-auth/client'
import React from 'react'
import { useUpdate } from 'react-use'

let user = {
	isStudent : false,
	isTeacher: false,
	isParent: false,
	isAdmin: false,
	isLoggedIn: false,
	data: null
}
const useAuth = ()=> {

	const [session, loading ] = useSession()

    if (loading && !session ) return { user }

	if (session)
	{
		user.data = session?.data
		const user_ = session?.data
		user.isLoggedIn = true
		if (user_.profile_type.name==='Admin')
		{
			user.isAdmin = true
		}
		else if (user_.profile_type.name==='Teacher')
		{
			user.isTeacher = true
		}
		else if (user_.profile_type.name==='Parent')
		{
			user.isParent = true
		}
		else if (user_.profile_type.name==='Student')
		{
			user.isStudent = true
		}
	}

	return { user }

}

export default useAuth
