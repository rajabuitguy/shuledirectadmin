
import React from 'react';
import ArrowDropDown from '@mui/icons-material/ArrowDropDown';
import { connect } from 'react-redux';
import LocalData from '../../helper/LocalData';
import contentProcess from '../../process/Contents';
import Subjects from '../../process/Subjects';
import { creators } from '../../redux/action/ActionCreators';

class DropDownComponent extends React.PureComponent{

    constructor(props){
        super(props);
        this.state = {
            open:false,
            darasa:parseInt(props.classId)-1 ?? null
        }
    }

    static defaultProps={
        open:false,
        contents:[],
        width:100,
        height:40,
        border:"1px solid lightgrey",
        borderRadius:5,
        display:"flex",
        alignItems:"center",
        marginLeft:10,
        fontFamily:"chalkboard",
        fontSize:20,
        contents:[]
    }

    async changeMoreSubjects(classs){
        let thoseSubjects
        this.setState({open:!this.state.open})
        this.props.dispatch(creators.setClassId(parseInt(classs)+1))
        this.setState({darasa:parseInt(classs)})
        if (!this.props.subjects){
            thoseSubjects= await Subjects.findSubjects()
        }else{
            thoseSubjects = this.props.subjects
        }
        contentProcess.processStudyMoreSubjects(thoseSubjects,classs,this.props.dispatch,null,"notes")
    }

    render(){
        const styles = {

            outerContainer:{
                width:this.props.width,
                minHeight:this.props.height,
                border:this.props.border,
                borderRadius:this.state.open ? 0:this.props.borderRadius,
                display:this.props.display,
                alignItems:this.props.alignItems,
                marginLeft:this.props.marginLeft,
                position:"relative",
                borderBottom:this.state.open ? "none":"1px solid lightgrey"
            },

            innerContainer:{
                flex:"auto",
                display:"flex",
                flexDirection:"column",
                justifyContent:"center",
                alignItems:"center",
                fontFamily:this.props.fontFamily,
                fontSize:this.props.fontSize,
                color:'#666'
            }
     

        }

        return(

            <div 
                style = {{
                    display:"flex",
                    flexDirection:"column",
                    zIndex:9999999,
                    padding:"1%",
                    marginTop:this.state.open ? "11%":0
                }}
            >
                <div 
                    style = {
                            !this.props.className ?  
                            this.props.styles ?
                            this.props.styles:
                            styles.outerContainer:
                            styles.outerContainer
                    }
                    className = {this.props.className ?? " "}
                >
                    <div
                        style = {
                            !this.props.className ?  
                            this.props.styles ?
                            this.props.styles:
                            styles.innerContainer:
                            styles.innerContainer
                        }
                        className = {this.props.className ?? " "}
                    >
                        {this.state.darasa}
                    </div>
                    <div 
                        style = {{display:"flex",alignItems:"center",justifyContent:"center"}}
                        onClick = {()=>this.setState({open:!this.state.open})}
                    >
                        <ArrowDropDown />
                    </div>
                    
                </div>
                
                {this.state.open ?
                <div 
                    style = {{
                        border:"1px solid lightgrey",
                        height:100,width:"91%",
                        marginLeft:"9%",
                        backgroundColor:"white",
                        marginTop:"-2.1%",
                        display:"flex",
                        flexDirection:"column",
                        alignItems:"center",
                        borderTop:"none",
                    }}
                >
                    {   
                        classes.map((item,k)=>{
                            return(
                                <div 
                                    key = {k} 
                                    onClick = {()=>this.changeMoreSubjects(item.numeral)}
                                    style = {{
                                        backgroundColor:this.state.darasa === item.numeral ?"whitesmoke": "transparent",
                                        width:"100%",
                                        display:"flex",
                                        alignItems:"center",
                                        justifyContent:"center",
                                        fontSize:16,
                                        fontFamily:"chalkboard",
                                        cursor:"pointer",
                                        color:"#666"
                                    }}

                                >
                                    {item.name}
                                </div>
                                )
                            })
                    }
                </div>:null}
            </div>
        )
    }
}

const classes = [
    {
        name:"form 1",
        id:2,
        numeral:1,
        roman:"I",
        
    },
    {
        name:"form 2",
        id:3,
        numeral:2,
        roman:"II"
    },
    {
        name:"form 3",
        id:4,
        numeral:3,
        roman:"III"
    },
    {
        name:"form 4",
        id:5,
        numeral:4,
        roman:"IV"
    }
]

function mapStateToProps(state){
    return{
        state,
        subjects:state.subjects,
        classId:state.classId
    }
}

const DropDown = connect(mapStateToProps)(DropDownComponent)
export {DropDown}