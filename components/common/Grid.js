
import React from 'react'
import styles from './styles/__CommonStyle.module.scss'

export function Grid (props){

    const renderer = (childrens)=>{
        if (childrens){
            if(childrens.length > 0){
                    return(
                        <React.Fragment>
                            {
                                childrens.map((child,k)=> {
                                    if (child){
                                        return(
                                            <div 
                                            style = {!child.props.className ?{
                                                width:`${child.props.xs ? child.props.xs*10 : props.xs*10}%`,
                                                alignItems:`${child.props.flex ? child.props.flex:props.flex}`,
                                                paddingRight:child.props.paddingRight ? 
                                                            child.props.paddingRight :props.paddingRight,
                                                paddingLeft:child.props.paddingLeft ? 
                                                            child.props.paddingLeft :props.paddingLeft,
                                                paddingTop:child.props.paddingTop ? 
                                                            child.props.paddingTop :props.paddingTop,
                                                paddingBottom:child.props.paddingBottom ? 
                                                            child.props.paddingBottom :props.paddingBottom,
                                            }:{}} 
                                            className = {child.props.className ? child.props.className:" " } 
                                            key = {k}
                                        >
                                            {child}
                                        </div>)   
                                    }else{
                                        null
                                    }
                                })
                            }
                        </React.Fragment>
                    )
              
            }else{
                return <div>{child}</div>
            }
        }
    
    }

    return(
        <div className = {styles.gridmenuContainer}>
            {renderer(props.children)}
        </div>
    )
}

Grid.defaultProps = {
    xs:10,
    flex:"center",
    paddingRight:0,
    paddingLeft:0,
    paddingTop:0,
    paddingBottom:0,
}