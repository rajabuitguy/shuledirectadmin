import React from "react";

const PhoneInput = ({
  number,
  placeholder,
  format,
  separator,
  onChange,
  onFocus,
  onValidity,
  style,
}) => {
  const [numberValue, setNumberValue] = React.useState("");

  const makeFormatedNumber = (numberString, formatString, separatorValue) => {
    const numberParts = numberString.trim().split(separatorValue);
    const formatParts = formatString.split(separatorValue);

    const formatedNumberParts = [];

    formatParts.forEach((formatSeg, index) => {
      const reg = /^\d+$/;
      if (numberParts[index] && reg.test(numberParts[index])) {
        const num = numberParts[index];
        formatedNumberParts.push(num.substring(0, formatSeg.length));
        if (num.length > formatSeg.length) {
          const extraString = num.substring(formatSeg.length, num.length);
          numberParts.splice(index + 1, 0, extraString);
        }
      }
    });

    return formatedNumberParts.join(separatorValue);
  };

  const initOnValidity = (isValid) => {
    if (onValidity) {
      onValidity(isValid);
    }
  };

  React.useEffect(() => {
    if (format && format.trim().length > 0) {
      if (separator && separator.length > 0) {
        let phoneNumberFormat = makeFormatedNumber(number, format, separator);
        setNumberValue(phoneNumberFormat);
        initOnValidity(true);
      } else {
        initOnValidity(false);
      }
    } else {
      const reg = /^\d+$/;
      if (reg.test(number)) {
        setNumberValue(number);
        initOnValidity(true);
      } else if (number.trim().length === 0) {
        setNumberValue("");
        initOnValidity(false);
      } else if (reg.test(numberValue)) {
        setNumberValue(numberValue);
        initOnValidity(true);
      }
    }
  }, [number]);

  return (
    <input
      type={"text"}
      value={numberValue}
      placeholder={placeholder}
      style={{ ...styles.input, ...style }}
      onChange={onChange}
      onFocus={onFocus}
    />
  );
};

export default PhoneInput;

const styles = {
  input: {
    width: "100%",
  },
};
