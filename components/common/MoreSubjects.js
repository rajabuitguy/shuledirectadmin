import {
  RowColsContainer,
  ContainerButton,
  ContentsComponent,
} from "./RowColsContainer";
import styles from "./styles/__CommonStyle.module.scss";
import { moreContens } from "../../data/";
import {connect} from 'react-redux'
import { useRouter } from "next/router";
import LocalData from "../../helper/LocalData";
import React from "react";
import Link from 'next/link'
import useAuth from './useAuth'
import { creators } from "../../redux/action/ActionCreators";
import subjectContents from '../../process/Contents'
import { notesHelper } from "../../helper/Notes";
import  {inclusiveHelper} from '../../helper/InclusiveHelper'

export function MoreSubjectsComponent(props) {

  const router = useRouter()

  const [staticContents,setStaticContents] = React.useState([])
  const [rendererStatus,setRendererStatus] = React.useState(false)
  const {user} = useAuth()

  const {moreText,studyText} = props;

  let studyContents = props.moreContents;
  let moreContents = moreContens;

  let path = router.pathname.split("/")

  const fetchingMoreContents = async ()=>{
    let thatStaticContents = LocalData.getMoreContents("curriculum")
    if(!thatStaticContents){
      thatStaticContents = await subjectContents.processExtraculliculumContents()
    }
    setStaticContents(thatStaticContents)
  }

  React.useEffect(()=>{
    fetchingMoreContents()
  },[])

  const fetchingAnotherSubject = async (classs,subjectId)=>{
    notesHelper.clearRedux(props.dispatch)
    notesHelper.scrollToTheTop(document)
    // let profileId = user ? user.id ? user.id:null:null
    let classsId = classs ? classs + 1:2
    let classes = LocalData.getClasses()
    // props.dispatch(creators.setRenderControl(parseInt(null)))
    props.dispatch(creators.setClasses(classes))
    // props.dispatch(creators.setProfileId(profileId ? parseInt(profileId):null))
    props.dispatch(creators.setSubjectId(subjectId))
    props.dispatch(creators.setClassId(classsId))
    await subjectContents.processTopics(subjectId,props.dispatch,classsId,props.profileId,null,null)

  }

  const themeStatus = inclusiveHelper.getSpecificInclusiveFeature(props.inclusiveContents,"theme")


  const style = {
    contentsStyle:{
      color:themeStatus ? "white":"#666",
      fontWeight:"normal",
      fontFamily:inclusiveHelper.getInclusiveFeature(props.inclusiveContents)??'Chalkboard',
      margin:"5% 0% 0% 2.5%",
      lineHeight: 1.4,
      fontSize: 18,
    }
  }


  return (
    <>
      <ContentsComponent
        content={"Study More"}
        styles = {style.contentsStyle}
        // className={styles.moreContents}
        // color = {themeStatus ? "white":"grey"}
      />
      <RowColsContainer marginTop={4}>
        {staticContents && staticContents.length > 0
          ? staticContents.map((content, k) => {
              return (
                  <ContainerButton key={k} className={styles.moreButton}>
                      <Link href = {content && content.link ? content.link:""}>
                        {content ? (content.content ? content.content : " ") : " "}
                      </Link>
                  </ContainerButton>
              
              );
            })
          : null}
      </RowColsContainer>

      {path && path.length > 0 && path.includes("notes") ? 
        <ContentsComponent
          content={"More from Shule Direct"}
          styles = {style.contentsStyle}
          // className={styles.moreContents}
          // color = {themeStatus ? "white":"grey"}
        />:null
      }

      {path && path.length > 0 && path.includes("notes") ? 
        <RowColsContainer>
          {studyContents && studyContents.length > 0
            ? studyContents.map((content, k) => {
              if (content && content.classs && content.subjectId){
                return (
                  <ContainerButton key={k} className={styles.moreButton}>
                    {/* <Link href = {`/notes/list_notes/${parseInt(content.classs)+1}/${content.subjectId}`}> */}
                      <div 
                        style={{ display: "flex", alignItems: "center" }} 
                        onClick = {()=>fetchingAnotherSubject(parseInt(content.classs),parseInt(content.subjectId))}
                      >
                        <div>
                          <img
                            src={
                              content ? (content.image ? content.image : " ") : " "
                            }
                            style={{ width: 20, height: 20, marginRight: 5 }}
                          />
                        </div>
                        <div>
                          {content
                            ? content.subject
                              ? content.subject
                              : " "
                            : " "}
                        </div>
                      </div>
                    {/* </Link> */}
                  </ContainerButton>
                );
              }
            
            })
          : null
      }
      </RowColsContainer>:null}
    </>
  );
}

MoreSubjectsComponent.defaultProps = {
  moreText: "More from Shule Direct",
  studyText: "Study more",
  moreContents: [],
  studyContents: [],
};

function mapStateToProps(state) {
  return {
    moreContents: state.moreContents,
    classId:state.classId,
    profileId:state.profileId,
    inclusiveContents:state.inclusiveContents,
  };
}

const MoreSubjects = connect(mapStateToProps)(MoreSubjectsComponent);
export { MoreSubjects };
