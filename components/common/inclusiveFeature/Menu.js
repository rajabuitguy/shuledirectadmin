
import React from 'react';
import {connect} from 'react-redux'
import AccessibleIcon from '@mui/icons-material/Accessible';
import SettingsIcon from '@mui/icons-material/Settings';
import {creators} from '../../../redux/action/ActionCreators'
import CloseIcon from '@mui/icons-material/Close';
import { Typography } from '@mui/material';
import { inclusiveHelper } from '../../../helper/InclusiveHelper';
import style from './style/InclusiveFeature.module.scss'
import ArrowDropDown from '@mui/icons-material/ArrowDropDown';
import { Journey } from '../../resource/Util';
import ZoomInIcon from '@mui/icons-material/ZoomIn';

const DropDown = (props)=>{

    return(
        <>
            <div
                style = {!props.className ?{
                    width:props.width,
                    minHeight:props.height,
                    borderRadius:props.borderRadius,
                    border:props.border,
                    backgroundColor:props.backgroundColor,
                    color:props.color,
                    display:props.display,
                    alignItems:props.alignItems,
                    marginTop:props.open ? 100:0,
                    padding:"1%"
                }:{}}
                className = {props.className ?? ""}
            >
                <div 
                    style = {{
                        flex:"auto",
                        display:"flex",
                        alignItems:"center",
                        justifyContent:"center",
                    }}
                >
                    {props.content}
                </div>
                <div onClick = {props.onclick}>
                    <ArrowDropDown />
                </div>
            </div>
    </>
    )
}

DropDown.defaultProps = {
    width:"80%",
    height:35,
    borderRadius:10,
    border:"1px solid lightgrey",
    backgroundColor:"white",
    color:"black",
    display:"flex",
    alignItems:"center",
    justifyContent:"center",
    content:" ",
    onclick:()=>{}
}

const MenuContainer = (props)=>{

    const [showBorder,setShowBorder] = React.useState(false)
    const [fontContent,setFontContent] = React.useState(
                                            inclusiveHelper.getInclusiveFeature(props.inclusiveContents)
                                             ?? "default"
                                        )
    const [open,setOpen] = React.useState(false)
    
    const styles = {
        outerContainer:{
            backgroundColor:"whitesmoke",
            width:"38%",
            height:"85%",
            marginLeft:"5%",
            marginTop:"5%",
            borderRadius:10,
            overFlowY:"auto",
        },
        headerContainer:{
            width:"100%",
            height:50,
            // backgroundColor:"#750c0e",
            backgroundColor:"grey",
            borderTopLeftRadius:10,
            borderTopRightRadius:10,
            display:"flex",
            alignItems:"center"
        },
        closeContainer:{
            width:30,
            height:30,
            backgroundColor:"white",
            borderRadius:30,
            display:"flex",
            alignItems:"center",
            justifyContent:"center",
            marginLeft:"5%"
        },
        innerContainer:{
            // backgroundColor:"#871617",
            backgroundColor:"#e8e4e3",
            height:"10%",
            width:"100%",
        },
        contentsContainer:{
            width:"90%",
            backgroundColor:"white",
            borderRadius:10,
            minHeight:"30%",
            marginTop:"-10%",
            marginLeft:"auto",
            marginRight:"auto",
            boxShadow:"1px 1px 1px -2px black",
            marginTop:"2%",
            paddingBottom:"2%",
        },
    
    }

    const inclusiveController = (contents)=>{
        if (contents && contents.feature && contents.content){
            inclusiveHelper.setInclusiveFeature(
                props.inclusiveContents,
                contents.feature,
                contents.content,
                props.dispatch
            )
        }
    }

    const controlrDropDown = (fonts = null,contents=null,fontFamily)=>{
        if(fonts && contents && fontFamily){
            setFontContent(fonts)
            inclusiveHelper.setInclusiveFeature(
                props.inclusiveContents,
                contents.feature,
                fontFamily,
                props.dispatch
            )
        }
        setOpen(!open)
    }

    

    return(
        <div className = {style.outerContainerMenu}>
            <div style = {styles.headerContainer}>
                <div style = {styles.closeContainer} id = "close">
                    <CloseIcon id = "innerClose"/>
                </div>
            </div>
            
            {
                inclusiveFeatures.map((inclusive,k)=>
                    <div style = {styles.contentsContainer} key = {`${k}`+"inclusive"}>
                        <Typography 
                            variant = "h5" 
                            sx = {{
                                marginLeft:"5%",
                                paddingTop:"5%",
                                fontFamily:inclusiveHelper.getInclusiveFeature(props.inclusiveContents)??""
                            }}
                        >
                            {inclusive.header}
                        </Typography>
                        <div 
                            className= {style.contentsContainer}
                            style = {{
                                display:"flex",
                                flexWrap:"wrap",
                                flexDirection:"row",
                                marginTop:"5%",
                                marginLeft:inclusive.header !== "Contents Adjustment" ? "2%":"0%",
                            }}
                        >
                            {
                                inclusive.contents.map((content,j)=>{
                                    return  <React.Fragment  key = {`${j}` + "contents"} >
                                                <div 
                                                    className = {
                                                        content.feature !== "font" && 
                                                        inclusiveHelper.getSpecificInclusiveFeature(
                                                        props.inclusiveContents,content.feature
                                                        ) ? style.containerActive:content.feature !== "font" ?
                                                        style.container:style.containerFont
                                                    }
                                                    onClick = {()=>{
                                                                    content.feature !== "font" ?
                                                                    inclusiveController(content):{}}
                                                                }
                                                >
                                                    {content.content}
                                                    {
                                                        content.feature === "zoom" ?
                                                        <ZoomInIcon style = {{width:50,height:50}} />:null 
                                                    }
                                                    {
                                                        content.feature === "font" ?
                                                        <DropDown 
                                                            content = {fontContent}
                                                            onclick = {()=>controlrDropDown(null,null,null)}
                                                        />
                                                        :null
                                                    }
                                                </div>
                                                {
                                                    open && content.feature === "font"  ?
                                                    <div className={style.dropDownMenu}>
                                                        {
                                                            content.childrens && content.childrens.length > 0 ?
                                                            content.childrens.map((font,m)=>
                                                                <div 
                                                                    key = {`${m}`+"dropdown"}
                                                                    style = {{
                                                                        color:"white",
                                                                        fontSize:16,
                                                                        fontWeight:"bold",
                                                                        display:"flex",
                                                                        alignItems:"center",
                                                                        justifyContent:"center",
                                                                        padding:"5%",
                                                                        cursor:"pointer",
                                                                        fontFamily:font ? 
                                                                            font.fontFamily?
                                                                            font.fontFamily:
                                                                            "TimesNewRoman":
                                                                            "TimesNewRoman"
                                                                    }}
                                                                    onClick = {()=>controlrDropDown(
                                                                                font ? 
                                                                                font.content ?
                                                                                font.content:
                                                                                "":"",
                                                                                content,
                                                                                font ?
                                                                                font.fontFamily?
                                                                                font.fontFamily:
                                                                                "TimesNewRoman":
                                                                                "TimesNewRoman"
                                                                            )}
                                                                >
                                                                    {font ? font.content ? font.content:" ":" "}
                                                                </div>
                                                            ):null
                                                        }
                                                    </div>
                                                :null}
                                            </React.Fragment>
                                        })
                               }
                  
                        </div>
                    </div>
                )
            }
            
        </div>
    )
}

export function InclusiveFeatureMenuComponent(props){

    const styles = {

        outerContainer:{
            position:"fixed",
            zIndex:99999999999,
            top:0,
            right:0,
            bottom:0,
            right:0,
            backgroundColor:"rgba(0,0,0,0.2)",
            width:"100%",
            display:"flex",
            alignItems:"center",
            justifyContent:"flex-start"
        },
        innerContainer:{
            backgroundColor:"white",
            width:"60%",
            height:"90%",
            marginLeft:"5%",
            borderRadius:10,
            
        }
    }

    const removeMenuContainer = (e)=>{
        if(e && e.target){
            if(
                e.target.id === "outerContainer" || 
                e.target.id === "close" || e.target.id === "innerClose" 
            ){
                props.dispatch(creators.setShowInclusiveMenuContainer(false))
            }
        }
       
    }

    if(props.showInclusiveMenu){
        return(
            <div style = {styles.outerContainer} onClick = {(e)=>removeMenuContainer(e)} id = "outerContainer">
            <Journey 
                id = {10}
                keyName = "inclusiceFeature01"
                feature = "Inclusive"
                action= "click-font-timefont"
            />
             <MenuContainer 
                dispatch = {props.dispatch} 
                inclusiveContents = {props.inclusiveContents}
            />
            </div>
        )
    }else{
        return null
    }
}

export function InclusiveButtonComponent(props){

    const styles = {

        iconContainer:{
            backgroundColor:"#871617",
            height:80,
            width:80,
            display:"flex",
            justifyContent:"center",
            alignItems:"center",
            borderRadius:40,
            position:"fixed",
            top:"85%",
            left:"2%",
            right:"89%",
            bottom:"5%"
        },
        iconStyle:{
            width:50,
            height:50,
            color:"white"
        },
        textContainer:{
            borderRadius:40,
            position:"fixed",
            top:"95%",
            left:"2%",
            right:"89%",
            bottom:"5%",
            fontSize:18,
            fontWeight:"bold"
        }
    }

    const inclusiveButtonController = ()=>{
        props.dispatch(creators.setShowInclusiveMenuContainer(!props.showInclusiveMenu))
    }

    if (!props.showInclusiveMenu){
        return(
            <>
                <div 
                    style = {styles.iconContainer}
                    onClick = {inclusiveButtonController}
                >
                    <SettingsIcon style = {styles.iconStyle}/>
                    
                </div>
                {/* <div style = {styles.textContainer}>
                    Accessibility
                </div> */}
            </>
        )
    }else{
        return null
    }
  
}

function mapStateToProps(state){

    return{
        state,
        showInclusiveMenu:state.showInclusiveMenu,
        inclusiveContents:state.inclusiveContents
    }
}

const InclusiveButton = connect(mapStateToProps)(InclusiveButtonComponent)
const InclusiveFeatureMenu = connect(mapStateToProps)(InclusiveFeatureMenuComponent)
export {InclusiveButton,InclusiveFeatureMenu}

const inclusiveFeatures = [

    {
        header:"Contents Adjustment",
        contents:[
            {
                content:"Readable fonts",
                feature:"font",
                childrens:[
                    {
                        content:"default",
                        fontFamily:"none"
                    },
                    {
                        content:"Chalkboard",
                        fontFamily:"Chalkboard"
                    },
                    {
                        content:"TimesNewRoman",
                        fontFamily:"TimesNewRoman"
                    },
                ]
            },
            {
                content:"Text magnifier",
                feature:"zoom",
                childrens:null
            },

            
        ]
    },
    {
        header:"Color Adjustment",
        contents:[
            {
                content:"Theme",
                feature:"theme",
                childrens:null
            }
           
        ]
    }
]