
import React from 'react';


class PositionChanger{

    getMouseEvent(evnt,callback,callbackZoomContents,zoomContents){
        if(evnt && evnt.target && evnt.target.innerText){
            zoomContents.contents = ""
            if (evnt.target.innerText && evnt.target.innerText !== ""){
                if (evnt.target.id  && evnt.target.id === "notesPage"){
                    callback(false)
                }else{
                    callback(true)
                    zoomContents.top = evnt.pageY + 20
                    zoomContents.left =  evnt.pageX
                    zoomContents.contents = evnt.target.innerText
                    callbackZoomContents(zoomContents)
                }
               
            }else{
                callback(false)
            }
        }else{
            callback(false)
        }
    }
}

export function ZoomComponent(props){

    const styles = {

        zoomContainer:{
            width:props.width,
            minHeight:props.height,
            position:props.position,
            top:props.top ?? props.zoomContents.top,
            left:props.left ?? props.zoomContents.left,
            paddingLeft:props.paddingLeft,
            paddingRight:props.paddingRight,
            paddingTop:props.paddingTop,
            paddingBottom:props.paddingBottom,
            backgroundColor:props.backgroundColor,
            color:props.color,
            fontSize:props.fontSize,
            fontWeight:props.fontWeight,
            zIndex:10000000
        }
    }

    return(
        <div style = {styles.zoomContainer}>
            {props.contents !== " "?props.contents:props.zoomContents.contents}
        </div>
    )
}

ZoomComponent.defaultProps = {

    position:"absolute",
    backgroundColor:"rgba(0,0,0,0.8)",
    width:400,
    height:50,
    paddingRight:10,
    paddingLeft:10,
    paddingTop:10,
    paddingBottom:10,
    color:"white",
    fontSize:36,
    contents:" ",
    fontWeight:"bold"
}

export const positionChanger = new PositionChanger()