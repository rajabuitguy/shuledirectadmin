import { inclusiveHelper } from "../../helper/InclusiveHelper"


export function ContainerButton(props){

    const containerButtonStyle = {
        containerButton:{
            width:props.width,
            height:props.height,
            display:props.display,
            width:props.width,
            backgroundColor:props.backgroundColor,
            marginLeft:props.marginLeft,
            borderWidth:props.borderWidth,
            borderColor:props.borderColor,
            paddingBottom:props.paddingBottom,
            paddingLeft:props.paddingLeft,
            paddingRight:props.paddingRight,
            paddingTop:props.paddingTop,
            borderStyle:props.borderStyle,
            alignItems:props.alignItem,
            justifyContent:props.justifyContents,
            display:props.display,
            marginTop:props.marginTop,
            borderRadius:props.borderRadius,
            fontFamily:inclusiveHelper.getInclusiveFeature(props.inclusiveContents)??""
        }
    }

    return(

        <div
            className = {props.className ? props.className:" "}
            style = {
                !props.className ?
                props.styles ? 
                props.styles ? 
                containerButtonStyle.containerButton:
                containerButtonStyle.containerButton:
                containerButtonStyle.containerButton:
                {
                    fontFamily:inclusiveHelper.getInclusiveFeature(props.inclusiveContents)??""
                }
            }
            onClick = {props.onclick}
        >
            {props.children}
        </div>
    )
}

export function ContentsComponent(props){

    const contentsStyle = {

        contents:{
            fontSize:props.fontSize,
            color:props.color,
            fontWeight:props.fontWeight,
            fontFamily:inclusiveHelper.getInclusiveFeature(props.inclusiveContents)
        }
    }

    return(

        <>
            <div
                className = {props.className ?? " "}
                style = {
                    !props.className ?
                    props.styles ?
                    props.styles :
                    contentsStyle.contents:
                    {fontFamily:inclusiveHelper.getInclusiveFeature(props.inclusiveContents)}
                }
                onClick = {props.onpress}
            >
                {props.content}
            </div>
        </>
    )
}

export function RowColsContainer(props){

    const rowColsContainerStyle = {

        containerStyle:{
            backgroundColor:props.backgroundColor,
            minHeight:props.height,
            display:props.display,
            width:props.width,
            flexWrap:props.flexWrap,
            flexDirection:props.flexDirection,
            marginLeft:props.marginLeft,
            marginTop:props.marginTop,
            alignItems:props.alignItem,
            justifyContent:props.justifyContent
        }
    }


    return(

        <>
            <div
                className = {props.classname ? props.classname:" "}
                style = {
                    !props.classname ?
                    props.styles ? 
                    props.styles :
                    rowColsContainerStyle.containerStyle:
                    {

                    }
                }
            >
                {props.children}
            </div>
        </>

    
    )
}

ContainerButton.defaultProps = {
    backgroundColor:"whitesmoke",
    height:40,
    display:"flex",
    minWidth:"5%",
    marginLeft:"3%",
    borderWidth:1,
    borderStyle:"solid",
    borderColor:"#CCCCCC",
    display:"flex",
    alignItem:"center",
    justifyContents:"center",
    paddingLeft:"2%",
    paddingRight:"2%",
    marginTop:"5%",
    borderRadius:10,
    paddingTop:"5%",
    paddingBottom:"5%",
    onclick :()=>{},
    inclusiveContents:null
    
}

ContentsComponent.defaultProps = {
    fontSize:18,
    color:"#666",
    fontWeight:"200",
    onpress:()=>{},
    inclusiveContents:null,
    themeStatus:false
}

RowColsContainer.defaultProps = {
    backgroundColor:"transparent",
    height:80,
    display:"flex",
    flexDirection:"row",
    width:"100%",
    flexWrap:"wrap",
    marginLeft:15,
    marginTop:0,
    alignItem:"center",
    justifyContent:"flex-start"
}