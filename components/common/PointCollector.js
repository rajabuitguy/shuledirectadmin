
import {useEffect} from 'react'
import Gateway from '../../service/Gateway'

export const calculatingPoints = async(props)=>{

    let request = {
        data: [{
            userId:props.userId,
            featureId :props.id,
            key :props.features,
            // direction:{
            //     id:props.directionId,
            //     name:props.directionName
            // },
            createdTime : new Date().getTime()
        }],
        service:{
            micro:"reward",
            task:"api/create",
            token:12234
        }
    }
    await Gateway.post(request)
    props.share ? props.setShare(false):null

}

export function PointCollector(props){

    // const calculatingReadingPoint = async()=>{
    //    let request = {
    //         data: [{
    //             userId:props.userId,
    //             featureId :props.id,
    //             key :props.features,
    //             createdTime : new Date().getTime()
    //         }],
    //         service:{
    //             micro:"reward",
    //             task:"api/create",
    //             token:12234
    //         }
    //     }
    //     await Gateway.post(request)
    //     props.share ? props.setShare(false):null
    // }

    useEffect(()=>{
        calculatingPoints(props)   
    },[])
    return null
}

PointCollector.defaultProps = {
    share:null,
    directionName:null,
    directionId:null
}