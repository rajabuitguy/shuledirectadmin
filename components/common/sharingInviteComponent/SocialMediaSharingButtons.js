
import {shareHelper} from './ShareHelpers'
import {createShareButton} from './CreateShareButton'

function whatsapLink(title,url){
    shareHelper.assert(url,"whatsap")

    return(
        'https://' + (shareHelper.deviceChecker() ? 'api':'web' + '.whatsapp.com/send' +
        shareHelper.getParams({
            text:title ? title + url:url
        })
        ) 
    )
}

function telegramLink(title,url){
    shareHelper.assert(url,"telegram")

    return(
        'https://telegram.me/share/url' +
        shareHelper.getParams({
            url,
            text:title ?? " "
        })
    )
}

function twitterLink(title,url){
    shareHelper.assert(url)

    return(
        'https://twitter.com/share' + 
        shareHelper.getParams({
            url,
            text:title ?? " "
        })
    )
}

function faceBookLink(title,url){
    return(
        'https://www.facebook.com/sharer/sharer.php' + 
        shareHelper.getParams({
            u:url,
            quote:title,
            hashtag:"#facebook"
        })
    )
}


export const WhatsappShareButton = (props)=>{
    return(
        <>
            {
                createShareButton(
                    "whatsapp",
                    whatsapLink(props.title,props.url),
                    props.styles,
                    props.className,
                    props.children
                )
            }
        </>
    )
}



export const TelegramShareButton = (props)=>{

    return(
        <>
            {
                createShareButton(
                    "telegram",
                    telegramLink(props.title,props.url),
                    props.styles,
                    props.className,
                    props.children
                )
            }
        </>
    )
}


export const TwitterShareButton = (props)=>{

    return(
        <>
            {
                createShareButton(
                    "twitter",
                    twitterLink(props.title,props.url),
                    props.styles,
                    props.className,
                    props.children
                )
            }
        </>
    )
}



export const FacebookShareButton = (props)=>{

    return(
        <>
            {
                createShareButton(
                    "facebook",
                    faceBookLink(props.title,props.url),
                    props.styles,
                    props.className,
                    props.children
                )
            }
        </>
    )
}


