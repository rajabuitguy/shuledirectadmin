
import React from "react"
import {PointCollector} from '../PointCollector'
export function createShareButton (socialMediaType,url,styles,className,children){
    
    return<ShareComponent 
                styles = {styles}
                className = {className}
                url = {url}
                // children = {children}
            >
                {children}
            </ShareComponent>
    
}

function ShareComponent(props){

    const [share,setShare] = React.useState(false)

    const handleClick = ()=>{
        setShare(true)
        window.open(props.url)
    }

    return(
        <div
            style = {!props.className && props.styles ? props.styles:{}}
            className = {props.className ? props.className:" "}
            onClick = {handleClick}
        >   
            {share ? 
                <PointCollector id = {8} userId = {12} features = "feature8" share = {share} setShare = {setShare}/>
                :null
            }
            {props.children}
        </div>
    )

}