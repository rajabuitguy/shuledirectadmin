
class AssetMessage extends Error{

    constructor(message){
        super(message);
    }
}

class ShareHelpers{

    deviceChecker(){
        return /(android|mobile|ipad|iphone)/i.test(navigator.userAgent); 
    }

    assert(value,type){
        // if(!value){
        //     throw ("The value is null")
        // }
    }

    getParams(object){

        if (object){
            let objectArray = Object.entries(object)
            if(objectArray && objectArray.length > 0){
                try{
                    let params = objectArray.filter(([,value])=>value !== undefined && value !== null)
                    .map(([key,value])=>`${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
                    return params.length > 0 ? `?${params.join('&')}`:' ' 
                }
                catch(err){
                    return ''
                }
             
            }
        }
    }

}

export const shareHelper = new ShareHelpers()