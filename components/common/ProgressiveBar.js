
import React from 'react';
import {connect} from 'react-redux'
import styles from './styles/__CommonStyle.module.scss'


export class ProgressiveBar extends React.PureComponent{

    constructor(props){
        super(props)
        this.state = {width:0}
    }


    percentage = this.props.progressBarContents ? 
                this.props.progressBarContents.percentage ? 
                this.props.progressBarContents.percentage :0:0

    componentDidMount(){
        this.setState({width:this.percentage})
    }

    componentDidUpdate(){
        this.setState({width:this.percentage})

    }
    
    render(){
    
    return(
        <div className = {styles.progressiveBarContainer}>
            <div 
                style = {{
                    width:`${Math.ceil(parseInt(this.state.width))}%`,
                    height:15,
                    borderRadius:10,
                    backgroundColor:"#f7c309"
                }}
            >
            </div>
        </div>
    )
}
}

