
import React from 'react';


export function BorderCircle(props){

    const styles = {
        container:{
            display:"flex",
            flexDirection:"column",
            alignItems:"center"
        },
        border:{
            width:1,
            height:25,
            backgroundColor:"#871617",
        },
        circle:{
            width:15,
            height:15,
            borderRadius:10,
            border:"1px solid #871617",
            backgroundColor:"#871617"
        }
    }

    return(
        <div style = {styles.container}>
            <div style = {styles.border}></div>
            <div style = {styles.circle}></div>
            <div style = {styles.border}></div>
        </div>
    )
}