import React from "react";

const DefaultButton = ({ label, children, hoverStyles, buttonStyles, onButtonClick }) => {
  const [isHover, setIsHover] = React.useState(false);

  const defaultStyles = buttonStyles
    ? { ...styles.button, ...buttonStyles }
    : styles.button;
  const onHoverStyle = {
    ...defaultStyles,
    backgroundColor: "rgba(135, 22, 25, 0.9)",
    boxShadow: "5px 5px 10px #aaa",
  };

  return (
    <div
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      onClick={onButtonClick}
      style={isHover ? {...onHoverStyle, ...hoverStyles} : defaultStyles}
    >
      {label ?? children}
    </div>
  );
};

export default DefaultButton;

const styles = {
  button: {
    backgroundColor: "#871620",
    paddingTop: "10px",
    paddingBottom: "10px",
    paddingLeft: "20px",
    paddingRight: "20px",
    color: "#fff",
    borderRadius: "3px",
    border: "none",
    cursor: "pointer"
  },
};
