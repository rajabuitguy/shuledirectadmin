import Link from 'next/link'
import {Typography} from '@mui/material'
import Image from 'next/image'

// Begin:Custom
import styles from './style/FeaturesCards.module.scss'
import icons from './icons'
// End:Custom

const featuresArray = [
   {
      name:"TIE",
      url:"/",
      icon:icons.tie,
      bgColor:"whiteColor"
   },
   {
      name:"Discussion",
      url:"/",
      icon:icons.discussion,
      bgColor:"reddishColor"
   },
   {
      name:"Past papers",
      url:"/",
      icon:icons.pastpaper,
      bgColor:"History"
   },
   {
      name:"Study Notes",
      url:"/",
      icon:icons.notes,
      bgColor:"blueColor"
   },
   {
      name:"Syllabus",
      url:"/",
      icon:icons.syllabus,
      bgColor:"greenColor"
   },
   {
      name:"Practical Videos",
      url:"/",
      icon:icons.video,
      bgColor:"semiPurpleColor"
   },
   {
      name:"Quizzes",
      url:"/",
      icon:icons.quizzes,
      bgColor:"Physics"
   },
   {
      name:"Mkali wa Mapindi",
      url:"/",
      icon:icons.mapindi,
      bgColor:"oceanColor"
   }
]

const FeaturesCards = ()=> {
   return <>
        <Typography variant="h5">Quick Links</Typography>
        <div className={styles.featuresCardsWrapper}>
          { featuresArray.map((data,index)=> (
              <div key={index} className={`${styles.featuresCard} ${data.bgColor}`}>
                <Link href="/">
                    <div>
                      <div>{data.name}</div>
                      <Image src={data.icon} alt={data.name} width={80} height={80}/>
                    </div>
                </Link>
              </div>
          ))}
        </div>
   </>

}
export default FeaturesCards
