import practicalVideoIcon from '../../../public/assets/icons/practical-video.png'
import notesIcon from '../../../public/assets/icons/Notes.png'
import pastpaperIcon from '../../../public/assets/icons/Pastpapers.png'
import quizzesIcon from '../../../public/assets/icons/Quizzes.png'
import tieIcon from '../../../public/assets/icons/TIE_img_.png'
import discussionIcon from '../../../public/assets/icons/Discussion.svg'
import mkaliWaPindiIcon from '../../../public/assets/icons/Mkali-wa-mapindi.svg'
import syllabusIcon from '../../../public/assets/icons/Syllabus.svg'

export default {
   pastpaper: pastpaperIcon,
   notes:notesIcon,
   quizzes:quizzesIcon,
   tie:tieIcon,
   video:practicalVideoIcon,
   discussion:discussionIcon,
   mapindi:mkaliWaPindiIcon,
   syllabus:syllabusIcon
}
