import { Route,Redirect } from 'react-router-dom'
import User from '../process/User'

const ProtectedRoute = ({component: Component, ...rest})=> {

   return <Route {...rest} render={
   	 	(props)=> {
   	 		if (User.isLoggedIn())
   	 		{
   	 			  return <Component {...props}/>
   	 		}
   	 		else
   	 		{
   	 			return <Redirect to={{
	   	 				pathname:'/redirect',
	   	 				state:{
	   	 					from: props.location
	   	 				}
   	 			}} />
   	 		}
   	 	}
   } />

}
export default ProtectedRoute
