
import { creators } from "../redux/action/ActionCreators";
import LocalData from './LocalData';

class InclusiveHelper{

    setInclusiveContents(inclusiveContents){LocalData.setInclusiveFeature(inclusiveContents)} 

    getInclusiveContents(dispatch){
        let inclusiveContents = LocalData.getInclusiveContents()
        dispatch(creators.setInclusiveContents(inclusiveContents))
    }

    setInclusiveFeature(inclusiveContents,inclusiveFeature,content,dispatch){
        let array = []
        if(inclusiveContents && inclusiveContents.length > 0){
            let index = inclusiveContents.findIndex((item)=>item.feature === inclusiveFeature)
            if(index !== -1){
                inclusiveContents[index].content = content;
                inclusiveContents[index].status = !inclusiveContents[index].status
                dispatch(creators.setInclusiveContents(inclusiveContents))
                this.setInclusiveContents(inclusiveContents)
            }else{
                let object = this.createObject(content,inclusiveFeature,true)
                inclusiveContents.push(object)
                dispatch(creators.setInclusiveContents(inclusiveContents))
                this.setInclusiveContents(inclusiveContents)
            }

        }else{
            let object = this.createObject(content,inclusiveFeature,true)
            array.push(object)
            dispatch(creators.setInclusiveContents(array))
            this.setInclusiveContents(array)
        }
       
    }

    createObject(content,feature,bool){
        let object = new Object()
        object.content = content;
        object.feature = feature;
        object.status = bool
        return object
    }

    getSpecificInclusiveFeature(contents,feature){
        let bool = false
        if (contents && contents.length > 0){
            let item = contents.find((content)=>{
                if(content && content.feature){
                    if(content.feature.trim().toLowerCase() === feature.trim().toLowerCase()){
                        return content
                    }
                }
            })
            if(item && item.status){
                bool = true
            }else{
                bool = false
            }

        }
        return bool
    }

    getInclusiveFeature = (inclusiveContents)=>{
        let fontFamily = null
        if (inclusiveContents && inclusiveContents.length > 0){
            let array = inclusiveContents.filter((value)=>{
                if(value && value.feature && value.feature.trim().toLowerCase() === "font"){
                    return value
                }
            })
            let font = array[0] &&  array.length > 0 ?  array[0].content ? array[0].content:null:null
            fontFamily = font !== "none" ? font:null
            
        }
       
        return fontFamily
    }

    getInclusiveFeature = (inclusiveContents,type="font")=>{
        let fontFamily = null
        if (inclusiveContents && inclusiveContents.length > 0){
            let array = inclusiveContents.filter((value)=>{
                if(value && value.feature && value.feature.trim().toLowerCase() === type){
                    return value
                }
            })
            let font = array[0] &&  array.length > 0 ?  array[0].content ? array[0].content:null:null
            fontFamily = font !== "none" ? font:null
            
        }
        return fontFamily
    }
}

const inclusiveHelper = new InclusiveHelper()
export {inclusiveHelper}