import CryptoJS from 'crypto-js';
class Utils{
  constructor(){
     this.salt = '6d090796-ecdf-11ea-adc1-0242ac120003'
  }
  encryptData(data){
    return CryptoJS.AES.encrypt(JSON.stringify(data), this.salt).toString()
  }
  decryptData(ciphertext, salt){
       const bytes = CryptoJS.AES.decrypt(ciphertext, this.salt)
       try {
          return JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
       }
       catch(err){
         return null
       }
  }

}

export default new Utils()
