import Utils from "./Utils";
import API from '../service/Gateway'

class LocalData {
  constructor() {
    this.domain = typeof window !== "undefined" ? window.location.origin : "";
    this.AUTHUSER = `AUTHUSER::${this.domain}`
    this.ACCOUNTS = `ACCOUNTS::${this.domain}`
    this.QUIZZES_BATCH = `QUIZZES_BATCH::${this.domain}`
    this.ACTIVE_QUIZZES_BATCH = `ACTIVE_QUIZZES_BATCH::${this.domain}`
    this.USER_JOURNEY = "user_journey";
    this.TOPICID = "TOPICID::TOPIC"
    this.SUBTOPICID = "SUBTOPICID::SUBTOPIC"
    this.SUBJECTID = "SUBJECTID::SUBJECT"
    this.COMPLETEDSUBTOPICS = "COMPLETEDSUBTOPICS::SUBTOPICS"
    this.CLASSES = "CLASSES::NOTES"
    this.INCLUSIVECONTENTS = "INCLUSIVECONTENTS::NOTES"
    this.MORESUBJECTS = "MORESUBJECTS::MORECONTENTS"
    this.EXTRACURRICULUM = "EXTRACURRICULUM::MORECONTENTS"
    this.USERACTIVITY = "USERACTIVITY"
    this.ACTIVELANGUAGE = `ACTIVELANGUAGE::${this.domain}`
  }

  /**
   * [description]
   * @param  {[type]} idContent [description]
   * @param  {String} type      [description]
   * @return {[type]}           [description]
   */
  setTopicIdAndSubtopicId = (idContent,type="topic")=>{
    let keyName = type === "topic" ? this.TOPICID : type === "subtopic" ? this.SUBTOPICID:this.SUBJECTID
    localStorage.setItem(keyName,JSON.stringify(idContent))
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @param  {String} type [description]
   * @return {[type]}      [description]
   */
  getTopicIdAndSubtopicId = (type = "topic")=>{

    let topicId
    let keyName = type === "topic" ?  this.TOPICID : type === "subtopic" ? this.SUBTOPICID:this.SUBJECTID

    try{
        topicId = localStorage.getItem(keyName)
    }
    catch(err){
      topicId = null
    }
    return topicId !== "undefined" && topicId ? JSON.parse(topicId):null
  }
  //-----------------------------------------------------------------------------------------------------------


  /**
   * [description]
   * @param  {String} type [description]
   * @return {[type]}      [description]
   */
  removeTopicIdAndSubtopicId = (type = "topic")=>{
    let keyName = type === "topic" ? this.TOPICID : type === "subtopic" ? this.SUBTOPICID:this.SUBJECTID
    try{
        localStorage.removeItem(keyName)
    }
    catch(err){
      throw "there is an error"
    }
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @param  {[type]} completedSubtopics [description]
   * @return {[type]}                    [description]
   */
  setCompletedSubtopics = (completedSubtopics)=>{
    localStorage.setItem(this.COMPLETEDSUBTOPICS,JSON.stringify(completedSubtopics))
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @return {[type]} [description]
   */
  getCompletedSubtopics = ()=>{
    let completedArray
    try{
      completedArray = localStorage.getItem(this.COMPLETEDSUBTOPICS)
    }
    catch(err){
      completedArray = null
    }
    return completedArray !== "undefined" && completedArray ? JSON.parse(completedArray):null
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @return {[type]} [description]
   */
  removeCompletedSubtopics = ()=>{

    try{
      localStorage.removeItem(this.COMPLETEDSUBTOPICS)
    }
    catch(err){
      throw "error"
    }
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @param  {[type]} inclusiveContents [description]
   * @return {[type]}                   [description]
   */
  setInclusiveFeature = (inclusiveContents)=>{
    localStorage.setItem(this.INCLUSIVECONTENTS,JSON.stringify(inclusiveContents))
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @return {[type]} [description]
   */
  getInclusiveContents = ()=>{
    let inclusiveContents
    try{
      inclusiveContents = localStorage.getItem(this.INCLUSIVECONTENTS)
    }
    catch(err){
      throw "error"
    }
    return inclusiveContents !== "undefined" && inclusiveContents ? JSON.parse(inclusiveContents):null
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @param  {[type]} classes [description]
   * @return {[type]}         [description]
   */
  setClasses = (classes)=>{
    localStorage.setItem(this.CLASSES,JSON.stringify(classes))
  }
  //-----------------------------------------------------------------------------------------------------------

 /**
  * [description]
  * @return {[type]} [description]
  */
  getClasses = ()=>{

    let classes

    try{
      classes = localStorage.getItem(this.CLASSES)
    }
    catch(err){
      classes = null
    }

    return classes !== "undefined" && classes ? JSON.parse(classes):null
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @return {[type]} [description]
   */
  removeClass = ()=>{

    try{
      localStorage.removeItem(this.CLASSES)
    }
    catch(err){
      throw "error"
    }
  }
  //-----------------------------------------------------------------------------------------------------------


  setMoreContents = (item,key)=>{
    let keyType = key === "subject" ? this.MORESUBJECTS :this.EXTRACURRICULUM
    localStorage.setItem(keyType,JSON.stringify(item))
  }

  getMoreContents = (key)=>{
    let keyType = key === "subject" ? this.MORESUBJECTS:this.EXTRACURRICULUM
    let contents = null
    try{
      contents = localStorage.getItem(keyType)
    }
    catch(err){
      throw "error"
    }
    return contents !== "undefined" && contents ? JSON.parse(contents):null
  }

  removeMoreContents = ()=>{
    let keyType = key === "subject" ? this.MORESUBJECTS:this.EXTRACURRICULUM
    try{
      localStorage.removeItem(keyType)
    }catch(err){
      throw "error"
    }
    
  }

  /**
   * [description]
   * @param  {[type]} journey [description]
   * @return {[type]}         [description]
   */
  addJourney = (journey) => {
    if (typeof window === "undefined") {
      return null;
    }

    if (localStorage.getItem(this.USER_JOURNEY)) {
      const journeyStore = localStorage.getItem(this.USER_JOURNEY);
      if (journeyStore.features) {
        journeyStore.data.features.push(journey);
      }
      localStorage.setItem(this.USER_JOURNEY, journeyStore);
    }
  };
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [description]
   * @param  {[type]} journey [description]
   * @return {[type]}         [description]
   */
  createJourney = async (journey) => {
    if (typeof window === "undefined") {
      return null;
    }
    if (JSON.parse(localStorage.getItem(this.USER_JOURNEY))) {
      const userJourney = localStorage.getItem(this.USER_JOURNEY);
      let data = JSON.parse(userJourney)
      let gateWay = {
        "service":{
        "micro":"logger",
        "task":"collectDeviceLogs",
        "token":12234
        }
     }

    let request = {...gateWay,...data}
    // let response = await API.post(request)
    }
    localStorage.setItem(this.USER_JOURNEY, JSON.stringify(journey));
  };
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [findAuthUser description]
   * @return {[type]} [description]
   */
  findAuthUser() {
    if (typeof window === "undefined") {
      return null;
    }
   
    let dataArray = localStorage.getItem(this.AUTHUSER);
    return dataArray ? Utils.decryptData(dataArray) : null;
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [removeAuthUser description]
   * @return {[type]} [description]
   */
  removeAuthUser() {
    if (typeof window === "undefined") {
      return false;
    }

    localStorage.removeItem(this.AUTHUSER);
    return true;
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [saveAuthUser description]
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  saveAuthUser(data) {
    if (typeof window === "undefined") {
      return false;
    }

    const encryptedData = Utils.encryptData(data);
    localStorage.setItem(this.AUTHUSER, JSON.stringify(encryptedData));
    if (data && localStorage.getItem(this.AUTHUSER)){
      return true
    }else{
      return false
    }
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [saveLoggedInAccount description]
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  saveLoggedInAccount(data) {
    if (typeof window === "undefined") {
      return false;
    }
    const encryptedData = Utils.encryptData(data);
    localStorage.setItem(this.ACCOUNTS, encryptedData);
    if (data && localStorage.getItem(this.ACCOUNTS)){
      return true
    }else{
      return false
    }
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [findAllLoggedAccounts description]
   * @return {[type]} [description]
   */
  findAllLoggedAccounts(){
    if (typeof window === "undefined") {
      return false;
    }

    let dataArray = localStorage.getItem(this.ACCOUNTS);
    return dataArray ? Utils.decryptData(dataArray) : null;
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [findLoggedAcctountById description]
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  findLoggedAcctountById(id) {
    if (typeof window === "undefined") {
      return false;
    }

    let accountArray = this.findAllLoggedAccounts();
    let result = accountArray.filter((data) => data.id === id);
    return result ? result : false;
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [removeUserAccount description]
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  removeUserAccount(id) {
    if (typeof window === "undefined") {
      return false;
    }

    let accountArray = this.findAllLoggedAccounts();
    let result = accountArray.filter((data) => data.id !== id);
    if (result.length === 0) {
      //remove the accounts storage
      localStorage.removeItem(this.ACCOUNTS);
      return false;
    }
    this.saveLoggedInAccount(result);
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [processLoggedAccounts description]
   * @param  {[type]} data_ [description]
   * @return {[type]}       [description]
   */
  processLoggedAccounts(data_) {  

    let dataArray = [];
    let userId = data_[0].id;
    data_.forEach((d) => {
      dataArray.push(d);
    });

    //get all saved accounts
    let accountArray = this.findAllLoggedAccounts();
    if (!accountArray) {
      //no accounts found, then save new account
      this.saveLoggedInAccount(dataArray);
      return false;
    }

    //check if the new accounts exist in a saved accounts
    let result = accountArray.some((data) => data.id === userId);
    if (result) {
      //account exist, exit (no need to proceed further process)
      return true;
    }

    //new account not found in a saved accounts,process the saved accounts
    //and the new account into the existing accounts
    accountArray.forEach((d) => {
      dataArray.push(d);
    });

    //save the new account array
    return this.saveLoggedInAccount(dataArray);
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [setQuizzesBatch description]
   * @param {[type]} data [description]
   */
  setQuizzesBatch(data){

    if (typeof window === "undefined") {
      return false;
    }

    const encryptedData = Utils.encryptData(data)
    localStorage.setItem(this.QUIZZES_BATCH, encryptedData)
    if (data && localStorage.getItem(this.QUIZZES_BATCH)){
      return true
    }else{
      return false
    }

  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [getQuzzesBatch description]
   * @return {[type]} [description]
   */
  getQuzzesBatch(){
      if (typeof window === "undefined") {
        return null;
      }
   
      let dataArray = localStorage.getItem(this.QUIZZES_BATCH)
      return dataArray ? Utils.decryptData(dataArray) : null
  }
  //-----------------------------------------------------------------------------------------------------------
  
  /**
   * [removeQuizzesBacth description]
   * @return {[type]} [description]
   */
  removeQuizzesBacth(){
      if (typeof window === "undefined") {
        return false;
      }

      localStorage.removeItem(this.QUIZZES_BATCH);
      return true;
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [setActiveQuizzesBatch description]
   * @param {[type]} data [description]
   */
  setActiveQuizzesBatch(data){
      if (typeof window === "undefined") {
        return false;
      }

      const encryptedData = Utils.encryptData(data);
      localStorage.setItem(this.ACTIVE_QUIZZES_BATCH,encryptedData);
      if (data && localStorage.getItem(this.ACTIVE_QUIZZES_BATCH)){
        return true
      }else{
        return false
      }
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [getActiveQuizzesBatch description]
   * @return {[type]} [description]
   */
  getActiveQuizzesBatch(){
      if (typeof window === "undefined") {
        return null;
      }
   
      let dataArray = localStorage.getItem(this.ACTIVE_QUIZZES_BATCH)
      return dataArray ? Utils.decryptData(dataArray) : null
  }
  //-----------------------------------------------------------------------------------------------------------
  
  /**
   * [remveActiveQuizzesBatch description]
   * @return {[type]} [description]
   */
  removeActiveQuizzesBatch(){
      if (typeof window === "undefined") {
        return false;
      }

      localStorage.removeItem(this.ACTIVE_QUIZZES_BATCH);
      return true;
  }
  //-----------------------------------------------------------------------------------------------------------

  /**
   * [setUserActivity description]
   */
  setUserActivity(data){
     if (typeof window === "undefined") {
        return false;
      }
      const encryptedData = Utils.encryptData(data);
      localStorage.setItem(this.USERACTIVITY,encryptedData);
      if (data && localStorage.getItem(this.USERACTIVITY)){
        return true
      }else{
        return false
      }
  }

  getUserActivity(){
      if (typeof window === "undefined") {
        return null;
      }
   
      let dataArray = localStorage.getItem(this.USERACTIVITY)
      return dataArray ? Utils.decryptData(dataArray) : null
  }

  processGetUserActivity(activityName){
     let activityArray = this.getUserActivity()
      if (!activityArray || activityArray.length == 0 )
      {
         return false
      }

      return activityArray.filter(data => data.name===activityName && data.status === 'active') ? true : false
  }

  processSetUserActivity(activityName){
      
      let activityArray = this.getUserActivity()

      activityArray = (activityArray && activityArray.length > 0) ? activityArray : null
      if (!activityArray)
      {
        //add activity
        let activityData = [
          {
             name: activityName,
             status: 'active'
          }
        ]
        this.setUserActivity(activityData)
        return false
      }
      
      //check if the activity is active
      activityArray = activityArray.filter(data => data.name===activityName && data.status != 'active')
      if (activityArray && activityArray.length > 0)
      {
         return false
      }

      this.setUserActivity(activityArray)
  }

  addActiveLanguage(data){
    if (typeof window === "undefined") {
      return false;
    }

    const encryptedData = Utils.encryptData(data);
    localStorage.setItem(this.ACTIVELANGUAGE,encryptedData)
    return data && localStorage.getItem(this.ACTIVELANGUAGE) ? true : false
  }

  getActiveLanguage(){
      if (typeof window === "undefined") {
        return null;
      }
   
      let dataArray = localStorage.getItem(this.ACTIVELANGUAGE)
      return dataArray ? Utils.decryptData(dataArray) : null
  }


}

export default new LocalData();
