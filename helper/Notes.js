
import DOMPurify from 'dompurify';
import contentProcess from '../process/Contents'
import {creators} from '../redux/action/ActionCreators'
import LocalData from './LocalData';
import subjectProcess from '../process/Subjects'
import notesProcess from '../process/Contents'

class NotesHelperClass{

    trackingObject = null

    checkHtmlTag (text){
        if (text){
            if (typeof text === "string"){
                return <div  dangerouslySetInnerHTML = {{__html:DOMPurify.sanitize(text)}}></div>
            }else{
                return text
            }
        }else{
            return " "
        }
    }

    checkBookedStatus(isBooked,bookedArr,subtopicId){
        let status
        if(isBooked){
            status = true
        }else{
            if (bookedArr && bookedArr.length > 0){
                let index = bookedArr.findIndex((item)=>{
                    if (item && item.subtopic && item.subtopic.id){
                        if(parseInt(item.subtopic.id) === parseInt(subtopicId)){
                            return item
                        }else{
                            status = false
                        }
                    }else{
                        status = false
                    }
                })
                if(index !== -1){
                    status = true
                }else{
                    status = false
                }
            }else{
                status = false
            }
        }
        return status
    }

    deletFavoriteFromRedux(contents,id,dispatch){
        if (contents && contents.length > 0){
            let index = contents.findIndex((favorite)=>favorite.id === id)
            if(index !== -1){
                contents.splice(index,1)
                dispatch(creators.getFavouriteSubject(contents))

            }
        }
    }

    async deleteBookedContent(
        subtopicId,
        bookedContents,
        userId,
        dispatch,
        type="bookmark",
        callback=null,
        subjectsContents = null,
        ){
        let item 
        let id
        if (bookedContents && bookedContents.length > 0){
            item = bookedContents.find((item)=>{
                if (item){
                    if (item.subtopic){
                        if (item.subtopic.id){
                            if (parseInt(item.subtopic.id) === parseInt(subtopicId)){
                                return item
                            }
                        }
                    }
                }
            })
        }
        if (type === "bookmark"){
            id = item ? item.id ? item.id:0:0
        }else if(type === "favorite"){
            id = subtopicId ?? 0
            this.deletFavoriteFromRedux(bookedContents,id,dispatch)
        }else{
            id = 0
        }
        let response = await contentProcess.deleteBookmarkContents(id,type)
        let data = response ? response.data ? response.data.data ? response.data.data:null:null:null
        if (data && data.length > 0){
            console.log("called",type)
            if(subjectsContents){
                this.changeClassAndSubjectsStatus(data,subjectsContents,dispatch,callback,"delete")
            }else{
                await contentProcess.requestingBookmarksContens(userId,dispatch,type,"progress",callback)
            }            
        }
    }

    attachImageToSubject(subjects,favoritesArray,dispatch){

        let newArray = []
        let thatItem

        if (favoritesArray && favoritesArray.length > 0){
            for(let i = 0;i < favoritesArray.length;i++){
                let favorite = favoritesArray[i]
                if(favorite && favorite.subject && favorite.subject.name){
                    let item = subjects[favorite.subject.name]
                    if (item && item.length > 0){
                        let newItem = item.find((subject)=>{
                            if(subject && subject.subject && subject.subject.id && favorite.subject.id){
                                if(subject.subject.id === favorite.subject.id){
                                    thatItem = favorite
                                    return subject
                                }
                            }
                        })
                        let image = newItem ? newItem.subject ? newItem.subject.subject_default?
                                    newItem.subject.subject_default.image ? newItem.subject.subject_default.image:
                                    null:null:null:null
                        let subjectName = newItem ? newItem .subject ? newItem.subject.name? 
                                        newItem.subject.name:null:null:null
                        let classs = newItem ? newItem.form ? newItem.form.numeral ? newItem.form.numeral
                                        :null:null:null
                        let subjectId = newItem ? newItem.subject ? newItem.subject.id ?  
                                            newItem.subject.id :null:null:null
                        let id = favoritesArray[i].id??null
                        let object = this.createFavouriteSubjctObject(image,subjectName,classs,subjectId,id)
                        newArray = this.getFavoritesContents(object,dispatch)
                    }
                }
            }
        }
        return newArray
    }

    createFavouriteSubjctObject(image,subjectName,classs,subjectId,id){
        let object = new Object()
        object.icon = image;
        object.name = subjectName;
        object.classs = classs;
        object.subjectId = subjectId;
        object.id = id
        return object
    }

    favoritesContents = []

    favouredArray = []

    setFavouriteArray(){
        this.favoritesContents = []
        this.favouredArray = []
    }

    getFavoritesContents(favoriteSubject,dispatch,where="server"){
        let index = -1;
        let innerIndex = -1;
        let extraIndex = -1;

        if (this.favoritesContents && this.favoritesContents.length > 0 && favoriteSubject && favoriteSubject.classs){
            index = this.favoritesContents.findIndex((item)=>parseInt(item.classs) === parseInt(favoriteSubject.classs))
            if(index !== -1){
                let object = this.createFavoriteObject(favoriteSubject,dispatch,where)
                return this.checkingObjectExist(object,index)     
            }else{
                extraIndex = this.favoritesContents.findIndex((item)=>item.classs === favoriteSubject.classs)
                if(extraIndex !== -1){
                    let object = this.createFavoriteObject(favoriteSubject,dispatch,where)
                    return this.checkingObjectExist(object,extraIndex)
                }else{
                    let favoritesObject = this.createFullObjectOfFavorite(favoriteSubject,dispatch,where)
                    this.favoritesContents.push(favoritesObject)
                    return this.favoritesContents
                }
              
            }
        }else{
            let favoritesObject = this.createFullObjectOfFavorite(favoriteSubject,dispatch,where)
            this.favoritesContents.push(favoritesObject)
            return this.favoritesContents
        }
    
    }

    checkingObjectExist(object,index){
        let innerIndex = -1;
        if(this.favoritesContents[index].subjects && this.favoritesContents[index].subjects.length > 0){
            innerIndex = this.favoritesContents[index].subjects.findIndex((item)=>{
                if(item){
                    if(parseInt(item.subjectId) === parseInt(object.subjectId)){
                        return item
                    }
                }
            })
            if(innerIndex === -1){
                this.favoritesContents[index].subjects.push(object)
            }
            return this.favoritesContents
        }else{
            this.favoritesContents[index].subjects.push(object)
            return this.favoritesContents
        }
    }

    createFavoriteObject(favoriteSubject,dispatch,where){
        let object = new Object()
        object.icon = favoriteSubject.icon ?? null
        object.name = favoriteSubject.name ?? null
        object.id = favoriteSubject.id ?? null
        object.subjectId = favoriteSubject.subjectId ??null
        this.dispatchingSubjectId(favoriteSubject,dispatch,"create",where)
        return object
    }

    createFullObjectOfFavorite(favoriteSubject,dispatch,where){
        let favoriteObject = new Object();
        let subjectObject = new Object();
        subjectObject.icon = favoriteSubject.icon ?? null;
        subjectObject.name = favoriteSubject.name ?? null;
        subjectObject.subjectId = favoriteSubject.subjectId
        let subjectArray = [];
        subjectArray.push(subjectObject)
        favoriteObject.classs = favoriteSubject.classs ?? null;
        subjectObject.id = favoriteSubject.id ?? null
        favoriteObject.subjects = subjectArray
        this.dispatchingSubjectId(favoriteSubject,dispatch,"create",where)
        return favoriteObject
    }

    dispatchingSubjectId(subjectObject,dispatch,where){
        if (subjectObject && subjectObject.id){
           this.createAndDeleteFavouredSubjects(
               null,
               null,
               this.favouredArray,
               null,
               subjectObject.subjectId,
               null,
               dispatch,
               "create",
               where
               )
        }
    }

    favoriteSubjectsController(id,subjectId,profileId,classAndSubjects,subjectName,classs,dispatch,callback){
        let status = null
        status = id ? "delete":"create"
        this.createAndDeleteFavouredSubjects(
            id,
            profileId,
            classAndSubjects,
            subjectName,
            subjectId,
            classs,
            dispatch,
            status,
            "client",
            callback
        )
    }

    deleteSubjectsSelectionFromState(id,subjectsContents,callback,renderer){
        let index = -1;
        if(id && subjectsContents && subjectsContents.length > 0){
            index = subjectsContents.findIndex((item)=>{
                if(item && item.id && parseInt(item.id) === parseInt(id)){
                    return item
                }
            })
            if(index !== -1){
                subjectsContents[index].id = null
                callback(subjectsContents)
                renderer()
            }
        }
    }

    deleteFavoritesFromState(id,favoritesContents,callback,renderer){
        let index = -1;
        if(favoritesContents && favoritesContents.length >  0){
            for(let i = 0;i < favoritesContents.length;i++){
                if(favoritesContents[i] && favoritesContents[i].subjects){
                    index = favoritesContents[i].subjects.findIndex((item)=>{
                        if(item && item.id && parseInt(item.id) === parseInt(id)){
                            return index
                        }
                    })
                    if(index !== -1){
                        favoritesContents[i].subjects.splice(index,1)
                        callback(favoritesContents)
                        renderer()
                    }
                   
                }
            }
        
        }
        return favoritesContents
    }

    async createAndDeleteFavouredSubjects(
        id,
        profileId,
        classAndSubjects,
        subjectName,
        subjectId,
        classs,
        dispatch,
        status="delete",
        where,
        callback = null
    ){
        let index = -1
            if(status === "create"){
                if(where === "client"){
                    await this.postFavouriteToApi(
                        id,
                        profileId,
                        subjectName,
                        subjectId,
                        classs,
                        dispatch,
                        status,
                        classAndSubjects
                    )
                }
            }else if (status === "delete"){
                if(where === "client"){
                    await this.postFavouriteToApi(
                        id,
                        profileId,
                        subjectName,
                        subjectId,
                        classs,
                        dispatch,
                        status,
                        classAndSubjects,
                        callback
                    )
                }
            }
    }

    async postFavouriteToApi(id,profileId,subjctName,subjectId,classs,dispatch,status,classAndSubjects,callback){
        if(status === "create"){
            let self = dispatch
            let object = new Object()
            let subjectName = subjctName 
            let classsId = parseInt(classs) + 1
            object.subtopic = null;
            object.topic = null;
            object.subject = {name:subjectName,id:subjectId}
            object.classs = {id:classsId,name:classs}
            object["profiles_id"] = profileId ?? null
            object["shortcut_types_id"] = 2
            let response = notesProcess.processingBookmarkContents(self,object,"favorite","progress",true)
            if(response.then()){
                response.then((data)=>{
                    this.changeClassAndSubjectsStatus(data,classAndSubjects,dispatch,callback,"create")
                })
            }
        
        }else{
            let {data} = await contentProcess.deleteBookmarkContents(id,"favorite")
            let response = data ? data.data ? data.data:null:null
            this.changeClassAndSubjectsStatus(response,classAndSubjects,dispatch,callback,"delete")
        }
    }

    changeClassAndSubjectsStatus(response,classAndSubjects,dispatch,callback,status="create"){
        let thatObject = response && response.length > 0 ? response[0]:null
        let index = -1
        let parsedSubject = null
        if(classAndSubjects && classAndSubjects.length > 0 && thatObject){
            for(let i = 0;i<classAndSubjects.length;i++){
                if (classAndSubjects[i].subjects && classAndSubjects[i].subjects.length){
                    for (let j = 0;j < classAndSubjects[i].subjects.length;j++){
                        index = classAndSubjects[i].subjects.findIndex((subject)=>{
                                parsedSubject = JSON.parse(thatObject.subject)
                                if(parseInt(subject.subjectId) === parseInt(parsedSubject.id)){
                                    return subject
                                }
                        })
                        if(index !== -1){
                            if(status === "create"){
                                classAndSubjects[i].subjects[index].id = thatObject ? parseInt(thatObject.id)??null:null
                            }else{
                                classAndSubjects[i].subjects[index].id = null 
                            }
                        }
                    }
                }
             
            }
        }
        dispatch(creators.setClassesAndSubjects(classAndSubjects))
        let profileId = thatObject ? thatObject.profiles ? 
        thatObject.profiles.id ? parseInt(thatObject.profiles.id):
        null:null:null
        notesProcess.requestingBookmarksContens(profileId,dispatch,"favorite","progress",callback)
    }

    async viewTopicsContents(subjectId,dispatch,classs,profileId,topicId=null,subtopicId=null){
        dispatch(creators.getTopics([]))
        dispatch(creators.getSubtopics([]))
        dispatch(creators.getActiveSubtopic({}))
        dispatch(creators.getActiveTopic(0))
        dispatch(creators.setTabs("Study Notes"))
        await contentProcess.processTopics(subjectId,dispatch,classs,profileId,topicId,subtopicId)

    }

    async setActiveSubjectId(subjectId,dispatch,classs,profileId){
        dispatch(creators.setSubjectId(subjectId))
        dispatch(creators.setSyllabusContents([]))
        contentProcess.processingSyllabus(subjectId,dispatch,classs,profileId)
        // contentProcess.processTopics(subjectId,dispatch,classs,profileId,null,null)

    }

    completedSubtopicsSetter(subtopics,dispatch){
        let completedSubtopics = []
        if(subtopics && subtopics.length > 0){
            subtopics.forEach((item)=>{
                if(item && item.type && item.type.toLowerCase() === "subtopic" && item["is_completed"]){
                   item.id ? completedSubtopics.push(parseInt(item.id)):null
                }
            })
        }
        dispatch(creators.completedSubtopics(completedSubtopics))
        LocalData.setCompletedSubtopics(completedSubtopics)
    }

    progressBarController(progressObject,dispatch,subtopicId,completedSubtopicArr){
        let id = parseInt(subtopicId)
        if(progressObject){
            let status = completedSubtopicArr.includes(id)
            if(!status){
                completedSubtopicArr.push(id)
                dispatch(creators.completedSubtopics(completedSubtopicArr))
                let newCompleted = parseInt(progressObject.completed) + 1
                if(newCompleted <= parseInt(progressObject.total)){
                    let newPercentage = ((newCompleted)/parseInt(progressObject.total))*100
                    progressObject.completed = newCompleted;
                    progressObject.percentage = newPercentage
                    dispatch(creators.progressBarContents(progressObject))
                }
            }
        }
    }

    clearRedux(dispatch){
        dispatch(creators.setSubjectId(null))
        dispatch(creators.getTopics(null))
        dispatch(creators.getSubtopics(null))
        dispatch(creators.getActiveTopic(null))
        dispatch(creators.getActiveSubtopic(null))
        dispatch(creators.progressBarContents(null))
    }

    shareController(dispatch,currentStatus){
        dispatch(creators.activateShareContainer(!currentStatus))
    }

    async getSyllabus(classs,subjectId,dispatch,profileId){
        const subjects = await subjectProcess.findSubjects()
        contentProcess.processStudyMoreSubjects(subjects,classs,dispatch,profileId,"notes")
        dispatch(creators.setSubjectId(subjectId))
        // this.setActiveSubjectId(subjectId,dispatch,classs,profileId)
        await contentProcess.processingSyllabus(subjectId,dispatch,classs,profileId)
    }

    filterSpecificSubjectClasses(topics,classes,subjectName,dispatch){
        let subjectNameFromTopics = topics ? topics.subjectName ? topics.subjectName:null:null
        let thatSubjectName = subjectName ?? subjectNameFromTopics
        if(thatSubjectName  && classes && classes.length > 0){
           classes.map((classs_)=>{
                if (classs_&&classs_.name){
                    if(classs_.name.trim().toLowerCase() === thatSubjectName.toLowerCase()){
                        dispatch(creators.setSpecificClasese(classs_.data ?? null))
                    }
                }
            })
        }
    }

    nextSubtopic(subtopicsArray,activeSubtopics,subtopicLength,currentIndex,dispatch){
        let nextIndex = 0;
        let nextSubtopicId = 0;
        let subtopicChildrens = []
        if(parseInt(currentIndex) + 1 < subtopicLength){
            nextIndex = currentIndex + 1
            if (subtopicsArray && subtopicsArray.length > 0){
                let subtopics = subtopicsArray.filter((item)=>{
                    if(item && item.type && item.type.trim().toLowerCase() === "subtopic"){
                        return item
                    }
                })
                let newLength = Math.abs(parseInt(subtopicLength)-parseInt(subtopics.length))
                nextIndex -= newLength
                let item = subtopics[Math.abs(nextIndex)]
                nextSubtopicId = item ? item.id ? item.id :0:0
                subtopicChildrens = item ? item.children? item.children:null:null
                contentProcess.processingAccordionsContents(
                    nextSubtopicId,
                    subtopicChildrens,
                    activeSubtopics,
                    dispatch,
                    null
                )
            }
        }
    }

    scrollToTheTop(self){
        self.documentElement.scrollTop = 0
        self.body.scrollTop = 0
    }

    getSubjectsAccordingToClass(array,dispatch){
        let subjectsArray = [];
        let index = -1;
        if (array){
            for(let subjectKey in array){
                if(subjectKey){
                    let newArray = array[subjectKey] && array[subjectKey].length > 0 ? array[subjectKey]:null
                    if (newArray && newArray.length > 0){
                        for(let i = 0;i < newArray.length;i++){
                            if (subjectsArray && subjectsArray.length > 0){
                                index = subjectsArray.findIndex((item)=>{
                                    if(
                                        item && item.classs && item.classs.id &&  
                                        newArray[i] && newArray[i].form && newArray[i].form.id
                                        ){
                                        if(parseInt(item.classs.id) === parseInt(newArray[i].form.id)){
                                            return item
                                        }
                                    }else{
                                        return -1
                                    }
                                })
                                if(index !== -1){
                                    subjectsArray = this.addingToExistingObject(newArray[i],subjectsArray,index,dispatch)
                                }else{
                                    let thatObject = this.creatingNewObject(newArray[i],dispatch)
                                    subjectsArray.push(thatObject)
                                }
                            }else{
                                let thatObject = this.creatingNewObject(newArray[i],dispatch)
                                subjectsArray.push(thatObject)
                            }
                        }
                    }
                }
            }
        }
        dispatch(creators.setClassesAndSubjects(subjectsArray))
    }

    addingToExistingObject(newObject,subjectsArray,index,dispatch){
        let subjectObject = new Object()
        let subjects = newObject && newObject.subject ? newObject.subject:null
        subjectObject.name = subjects.name
        subjectObject.subjectId = subjects.id 
        subjectObject.image = subjects["subject_default"]?subjects["subject_default"].image?
                              subjects["subject_default"].image:null:null
        subjectObject.id = null
        subjectsArray[index].subjects.push(subjectObject)
        return subjectsArray
    }

    creatingNewObject(newObject,dispatch){
        let object = new Object()
        let subjectObject = new Object()
        let subjects = newObject && newObject.subject ? newObject.subject:null
        subjectObject.name = subjects.name
        subjectObject.subjectId = subjects.id 
        subjectObject.id = null
        subjectObject.image = subjects["subject_default"]?subjects["subject_default"].image?
                              subjects["subject_default"].image:null:null
        object.classs = newObject && newObject.form ? newObject.form:null
        object.subjects =  []
        object.subjects.push(subjectObject)
        return object
    }

    checkFavouredSubjects(favouriteContents,classAndSubjectContents,dispatch){
        let index = -1;

            if(
                favouriteContents && favouriteContents.length > 0 && 
                classAndSubjectContents && classAndSubjectContents.length > 0
            ){
                for(let i = 0;i < classAndSubjectContents.length;i++){
                    if(
                        classAndSubjectContents && classAndSubjectContents[i] && 
                        classAndSubjectContents[i].classs && classAndSubjectContents[i].classs.numeral
                    ){
                        let filteredArray = favouriteContents.filter((item)=>{
                            if(item && item.classs){
                                if(item.classs && classAndSubjectContents[i].classs.numeral){
                                    if (parseInt(item.classs) === parseInt(classAndSubjectContents[i].classs.numeral)){
                                        return item
                                    }
                                }
                            }
                        })

                        for(let j = 0;j < classAndSubjectContents[i].subjects.length;j++){

                            if (
                                    filteredArray && filteredArray.length > 0 && 
                                    filteredArray[0] && filteredArray[0].subjects &&
                                    filteredArray[0].subjects.length > 0
                                ){
                                index = filteredArray[0].subjects.findIndex((subject)=>{
                                    if(
                                        subject && subject.subjectId && 
                                        classAndSubjectContents[i].subjects[j] &&
                                        classAndSubjectContents[i].subjects[j].subjectId
                                        )
                                    {
                                        if (parseInt(subject.subjectId) === parseInt(classAndSubjectContents[i].subjects[j].subjectId)){
                                            return subject
                                        }
                                    }
                                })
                                if(index !== -1){
                                    classAndSubjectContents[i].subjects[j].id = filteredArray[0].subjects[index].id
                                }
                            }else{
                                continue
                            }
                        }

                    }
                }
        } 
        // console.log(classAndSubjectContents)   
        dispatch(creators.setFavouredSubjects(classAndSubjectContents))    
    }

    checkSubjectInFavouriteArray(favouredArray,subjectId){
        let status = false
        if (subjectId && favouredArray && favouredArray.length > 0){
            status = favouredArray.includes(parseInt(subjectId))
        }
        return status
    }

    calculatingReminedNumber(array){
        let slicedArray = null
        if (array && array.length > 0){
            slicedArray = array.slice(0,3)
            return array.length - slicedArray.length > 0 ? array.length - slicedArray.length:0
        }else{
            return 0
        }
 
    }

    sliceContents(array,selectedId,classId){
        let returnedArray = null
        if (selectedId && classId && array && array.length > 0 ){
            if (parseInt(selectedId) === parseInt(classId)){
                returnedArray = array
            }else{
                returnedArray = this.slicingArray(array)
            }
        }else{
            returnedArray  = this.slicingArray(array)
        }
        return returnedArray
    }

    slicingArray(thatArray){
        if (thatArray && thatArray.length > 0){
            return  thatArray.slice(0,3)
        }else{
            return null
        }
    }

}

export const notesHelper = new NotesHelperClass()

