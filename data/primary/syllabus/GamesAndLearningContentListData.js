export const GamesAndLearningContentListData = { 
		learning_content: [
			{
				id:"4",
				subject_id:'3',
				topic_id:'3',
				subtopic_id:'1',
				classs_id:"45",
				name:"Kutambua herufi m,n,o",
				unique_key_code:'uniqueKeyCode',
				content_type:"direct_video_upload",
				cover_image:[
				    {
				   	  	"id": "36",
		               "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
		               "file_type": "image/jpeg",
		               "file_name": "istockphoto-1297349747-612x612.jpg",
		               "media_types": "image",
		               "file_extension": "jpg",
		               "file_size": "44KB",
		               "mapping_id": "",
		               "parent_id": "",
		               "dest": "file_contents"
				    }
				]
			},
			{
				id:"2",
				subject_id:'3',
				topic_id:'3',
				subtopic_id:'1',
				classs_id:"45",
				name:"Kutambua herufi m,n,o",
				unique_key_code:'uniqueKeyCode',
				content_type:"image_sound_upload",
				cover_image:[
				    {
				   	   "id": "36",
		               "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
		               "file_type": "image/jpeg",
		               "file_name": "istockphoto-1297349747-612x612.jpg",
		               "media_types": "image",
		               "file_extension": "jpg",
		               "file_size": "44KB",
		               "mapping_id": "",
		               "parent_id": "",
		               "dest": "file_contents"
				    }
				]
			}
		],
		games_resources: [
		    {
				id:"3",
				subject_id:'3',
				topic_id:'3',
				subtopic_id:'1',
				classs_id:"45",
				name:"Kutambua herufi m,n,o",
				unique_key_code:'uniqueKeyCode',
				content_type:"image_game_resource",
				cover_image:[
				    {
				   	   "id": "36",
		               "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
		               "file_type": "image/jpeg",
		               "file_name": "istockphoto-1297349747-612x612.jpg",
		               "media_types": "image",
		               "file_extension": "jpg",
		               "file_size": "44KB",
		               "mapping_id": "",
		               "parent_id": "",
		               "dest": "file_contents"
				    }
				]
			},
			{
				id:"4",
				subject_id:'3',
				topic_id:'3',
				subtopic_id:'1',
				classs_id:"45",
				name:"Kutambua herufi m,n,o",
				unique_key_code:'uniqueKeyCode',
				content_type:"number_range",
				cover_image:[
				    {
				   	   "id": "36",
		               "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
		               "file_type": "image/jpeg",
		               "file_name": "istockphoto-1297349747-612x612.jpg",
		               "media_types": "image",
		               "file_extension": "jpg",
		               "file_size": "44KB",
		               "mapping_id": "",
		               "parent_id": "",
		               "dest": "file_contents"
				    }
				]
			}
		]
	}
