export const LearningContentDirectVideoById = {
    "learning_content": {
        "id": "125",
        "subject_id": "59628",
        "classs_id": "59601",
        "topic_id": "59706",
        "subtopic_id": "59707",
        "name": "Learning Video Content",
        "content_type": "direct_video_upload",
        "indicator": "update",
        "unique_key_code": "LearningVideoContent",
        "cover_image": [
            {
                "id": "31",
                "url": "https://shulemedia.s3.amazonaws.com/image/1644403683_cbf975fb19a31ffe95a5.png",
                "file_type": "image/png",
                "file_name": "KidsApp.png",
                "media_types": "image",
                "file_extension": "png",
                "file_size": "6KB",
                "mapping_id": "161",
                "parent_id": "31",
                "dest": "file_contents"
            }
        ],
        "data": {
            "video_content": [
                {
                    "block_info": {
                        "id": "40"
                    },
                    "media": {
                        "id": "34",
                        "url": "https://shulemedia.s3.amazonaws.com/video/1644409594_e1273eeb0e0b98ebc8ac.mp4",
                        "file_type": "video/mp4",
                        "file_name": "WhatsApp Video 2022-01-17 at 10.14.43 PM.mp4",
                        "media_types": "video",
                        "file_extension": "mp4",
                        "file_size": "1MB",
                        "mapping_id": "202",
                        "parent_id": "22",
                        "dest": "file_contents"
                    },
                    "settings_data": [
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        },
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        },
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        }
                    ]
                }
            ],
            "image_audio_resources": null
        }
    },
    "game_resources": null
}