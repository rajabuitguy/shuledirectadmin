export const LearningContentById = {
    "id": "2",
    "subject_id": "3",
    "topic_id": "3",
    "subtopic_id": "1",
    "name": "Jifunze kusoma a,e,i,o,u",
    "content_type": "direct_video_upload",
	"resource_content_type":'learning_content',
    "cover_image": [
        {
            "file_name": "KidsApp1",
            "url": "https://res.cloudinary.com/demo/image/upload/v1642630213/docs_uploading_example/KidsApp1_ubsxpf.png",
            "file_type": "png",
            "resource_type": "image"
        }
    ],
    "data": {
        "video_content": [
            {
                "file_name": "Teacher dashboard",
                "url": "https://res.cloudinary.com/demo/image/upload/v1642631782/docs_uploading_example/Teacher_dashboard_eqozdo.png",
                "file_type": "png",
                "resource_type": "image"
            }
        ],
        "audio_content": {
		    "content": [
		        {
		            "audio_cover": {
		                "file_name": "Screenshot 2021-12-02 150500",
		                "url": "https://res.cloudinary.com/demo/image/upload/v1642661890/docs_uploading_example/Screenshot_2021-12-02_150500_bkvpig.png",
		                "file_type": "png",
		                "resource_type": "image"
		            },
		            "audio_content": {
		                "file_name": "KidsApp",
		                "url": "https://res.cloudinary.com/demo/image/upload/v1642688653/docs_uploading_example/KidsApp_eimiae.png",
		                "file_type": "png",
		                "resource_type": "image"
		            }
		        },
		        {
		            "audio_cover": {
		                "file_name": "Teacher dashboard",
		                "url": "https://res.cloudinary.com/demo/image/upload/v1642631782/docs_uploading_example/Teacher_dashboard_eqozdo.png",
		                "file_type": "png",
		                "resource_type": "image"
		            },
		            "audio_content": {
		                "file_name": "Teacher dashboard",
		                "url": "https://res.cloudinary.com/demo/image/upload/v1642629892/docs_uploading_example/Teacher_dashboard_i7jk8g.png",
		                "file_type": "png",
		                "resource_type": "image"
		            }
		        }
		    ],
		    "settings_data": [
		        {
		            "id": "",
		            "title": "Instruction (Audio/sound)",
		            "type": "audio",
		            "slug": "instruction",
		            "media": {
		                "id": "",
		                "file_name": "",
		                "file_type": "",
		                "url": "",
		                "resource_type": ""
		            }
		        },
		        {
		            "id": "",
		            "title": "Tips (Video)",
		            "type": "video",
		            "slug": "tips_slug",
		            "media": {
		                "id": "",
		                "file_name": "",
		                "file_type": "",
		                "url": "",
		                "resource_type": ""
		            }
		        },
		        {
		            "id": "",
		            "title": "Media on sucess (Audio/video)",
		            "type": "video",
		            "slug": "on_success",
		            "media": {
		                "id": "",
		                "file_name": "",
		                "file_type": "",
		                "url": "",
		                "resource_type": ""
		            }
		        },
		        {
		            "id": "",
		            "title": "Media on fail (audio/video)",
		            "type": "video",
		            "slug": "on_fail",
		            "media": {
		                "id": "",
		                "file_name": "",
		                "file_type": "",
		                "url": "",
		                "resource_type": ""
		            }
		        }
		    ]
		}
    }
}