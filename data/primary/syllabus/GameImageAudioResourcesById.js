export const GameImageAudioResourcesById = {
    "learning_content": null,
    "game_resources": {
        "id": "112",
        "subject_id": "59814",
        "classs_id": "59602",
        "topic_id": "59842",
        "subtopic_id": "59843",
        "name": "Games for people",
        "content_type": "image_game_resource",
        "indicator": "update",
        "unique_key_code": "GamesForPeople",
        "cover_image": [
            {
                "id": "35",
                "url": "https://shulemedia.s3.amazonaws.com/image/1644473885_1f17ee52d35c1dd34e3b.png",
                "file_type": "image/png",
                "file_name": "changepassword.png",
                "media_types": "image",
                "file_extension": "png",
                "file_size": "52KB",
                "mapping_id": "105",
                "parent_id": "35",
                "dest": "file_contents"
            }
        ],
        "data": {
            "game_image_audio_resources": [
                {
                    "block_info": {
                        "id": "13"
                    },
                    "options_array": [
                        {
                            "answer": {
                                "id": "",
                                "url": "",
                                "file_type": "",
                                "file_name": "",
                                "media_types": "",
                                "file_extension": "",
                                "file_size": "",
                                "mapping_id": "109",
                                "parent_id": "3",
                                "dest": "file_contents"
                            },
                            "audio": {
                                "id": "",
                                "url": "",
                                "file_type": "",
                                "file_name": "",
                                "media_types": "",
                                "file_extension": "",
                                "file_size": "",
                                "mapping_id": "110",
                                "parent_id": "3",
                                "dest": "file_contents"
                            },
                            "question": {
                                "id": "36",
                                "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
                                "file_type": "image/jpeg",
                                "file_name": "istockphoto-1297349747-612x612.jpg",
                                "media_types": "image",
                                "file_extension": "jpg",
                                "file_size": "44KB",
                                "mapping_id": "108",
                                "parent_id": "3",
                                "dest": "file_contents"
                            }
                        },
                        {
                            "answer": {
                                "id": "",
                                "url": "",
                                "file_type": "",
                                "file_name": "",
                                "media_types": "",
                                "file_extension": "",
                                "file_size": "",
                                "mapping_id": "109",
                                "parent_id": "3",
                                "dest": "file_contents"
                            },
                            "audio": {
                                "id": "",
                                "url": "",
                                "file_type": "",
                                "file_name": "",
                                "media_types": "",
                                "file_extension": "",
                                "file_size": "",
                                "mapping_id": "110",
                                "parent_id": "3",
                                "dest": "file_contents"
                            },
                            "question": {
                                "id": "36",
                                "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
                                "file_type": "image/jpeg",
                                "file_name": "istockphoto-1297349747-612x612.jpg",
                                "media_types": "image",
                                "file_extension": "jpg",
                                "file_size": "44KB",
                                "mapping_id": "108",
                                "parent_id": "3",
                                "dest": "file_contents"
                            }
                        },
                        {
                            "answer": {
                                "id": "",
                                "url": "",
                                "file_type": "",
                                "file_name": "",
                                "media_types": "",
                                "file_extension": "",
                                "file_size": "",
                                "mapping_id": "109",
                                "parent_id": "3",
                                "dest": "file_contents"
                            },
                            "audio": {
                                "id": "",
                                "url": "",
                                "file_type": "",
                                "file_name": "",
                                "media_types": "",
                                "file_extension": "",
                                "file_size": "",
                                "mapping_id": "110",
                                "parent_id": "3",
                                "dest": "file_contents"
                            },
                            "question": {
                                "id": "36",
                                "url": "https://shulemedia.s3.amazonaws.com/image/1644473949_ea108525408e8c596806.jpg",
                                "file_type": "image/jpeg",
                                "file_name": "istockphoto-1297349747-612x612.jpg",
                                "media_types": "image",
                                "file_extension": "jpg",
                                "file_size": "44KB",
                                "mapping_id": "108",
                                "parent_id": "3",
                                "dest": "file_contents"
                            }
                        }
                    ],
                    "settings_data": [
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        },
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        },
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        }
                    ]
                }
            ],
            "number_range_data": null
        }
    }
}