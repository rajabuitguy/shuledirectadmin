import { GamesAndLearningContentListData } from './GamesAndLearningContentListData'
import { LearningContentById } from './LearningContentById'
import { GameImageAudioResourcesById } from './GameImageAudioResourcesById'
import { GameNumberRangeById } from './GameNumberRangeById'
import { LearningContentDirectVideoById } from './LearningContentDirectVideoById'
import { LearningContentImageAudioById } from './LearningContentImageAudioById'

export const Data = {
    learning_content_by_id:LearningContentById,
    game_image_audio_resource_by_id:GameImageAudioResourcesById,
    game_number_range_by_id:GameNumberRangeById, 
    games_resource_list:GamesAndLearningContentListData,
    learing_content_direct_video: LearningContentDirectVideoById,
    learning_content_image_audio_by_id: LearningContentImageAudioById
}

