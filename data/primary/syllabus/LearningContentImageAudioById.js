export const LearningContentImageAudioById = {
    "learning_content": {
        "id": "125",
        "subject_id": "59628",
        "classs_id": "59601",
        "topic_id": "59706",
        "subtopic_id": "59707",
        "name": "Learning Content",
        "content_type": "image_sound_upload",
        "indicator": "update",
        "unique_key_code": "LearningContent",
        "cover_image": [
            {
                "id": "31",
                "url": "https://shulemedia.s3.amazonaws.com/image/1644403683_cbf975fb19a31ffe95a5.png",
                "file_type": "image/png",
                "file_name": "KidsApp.png",
                "media_types": "image",
                "file_extension": "png",
                "file_size": "6KB",
                "mapping_id": "161",
                "parent_id": "31",
                "dest": "file_contents"
            }
        ],
        "data": {
            "video_content": null,
            "image_audio_resources": [
                {
                    "block_info": {
                        "id": "41"
                    },
                    "content": [
                        {
                            "audio_cover": {
                                "id": "31",
                                "url": "https://shulemedia.s3.amazonaws.com/image/1644403683_cbf975fb19a31ffe95a5.png",
                                "file_type": "image/png",
                                "file_name": "KidsApp.png",
                                "active_label": "active_label_0",
                                "media_types": "image",
                                "file_extension": "png",
                                "file_size": "6KB",
                                "mapping_id": "",
                                "parent_id": "",
                                "dest": "file_contents"
                            },
                            "audio_content": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "",
                                "parent_id": "",
                                "dest": "file_contents"
                            }
                        },
                        {
                            "audio_cover": {
                                "id": "31",
                                "url": "https://shulemedia.s3.amazonaws.com/image/1644403683_cbf975fb19a31ffe95a5.png",
                                "file_type": "image/png",
                                "file_name": "KidsApp.png",
                                "active_label": "active_label_0",
                                "media_types": "image",
                                "file_extension": "png",
                                "file_size": "6KB",
                                "mapping_id": "",
                                "parent_id": "",
                                "dest": "file_contents"
                            },
                            "audio_content": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "",
                                "parent_id": "",
                                "dest": "file_contents"
                            }
                        }
                    ],
                    "settings_data": [
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        },
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        },
                        {
                            "id": "28",
                            "title": "Media on fail (audio/video)",
                            "type": "video",
                            "slug": "on_fail",
                            "media": {
                                "id": "33",
                                "url": "https://shulemedia.s3.amazonaws.com/audio/1644406380_e3265bbf3d960e77fd05.mp3",
                                "file_type": "audio/mpeg",
                                "file_name": "file_example_MP3_1MG.mp3",
                                "media_types": "audio",
                                "file_extension": "mp3",
                                "file_size": "1MB",
                                "mapping_id": "111",
                                "parent_id": "33",
                                "dest": "file_contents",
                            }
                        }
                    ]
                }
            ]
        }
    },
    "game_resources": null
}