import { Container } from '@mui/material'
import { getSession } from "next-auth/client"

import React from 'react'

// Begin:Custom
import User from '../../../process/User'
import ProfilePage from '../../../components/users/ProfilePage'

// End:Custom

const Dashboard = ({user})=> {
	return <Container><ProfilePage userData={user} /></Container>
}

export default Dashboard

export async function getServerSideProps(context) {

	const session = await getSession(context)
	if (!session)
	{
	  return { redirect: { destination: "/redirect", permanent: false } }
	}

	const { param }  = context.query
	const userParam = param[0]
	const { data,networkError } = await User._findById(userParam)
	const user = (networkError) ? null : data.data[0] 

	if (!user.schools)
	{
		return { redirect: { destination: "/auth/complete", permanent: false } }
	}

	
	return {
    	props: { user }
  	}

}



