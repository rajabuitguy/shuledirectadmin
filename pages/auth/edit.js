import { getSession } from "next-auth/client"

// Begin: Custom
import EditPage from '../../components/auth/EditPage'
import User from '../../process/User'
// End: Custom

const Edit = ({user})=> {
	 return <EditPage user={user} />
}

export default Edit

export async function getServerSideProps(context) {
    const session = await getSession(context)
    if (!session)
    {
      return { redirect: { destination: "/redirect", permanent: false } }
    }

    //get user details
    const { data: userArray }  = await User._findById(session?.data.id)
    const user = userArray.data[0]
    return {
      props: {  user }
    }
 }