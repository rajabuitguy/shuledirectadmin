import { getSession } from "next-auth/client"

// Begin: Custom
import RegisterForm from '../../components/auth/RegisterForm'
// End: Begin: Custom

const RegisterPage = ()=> {

	return <RegisterForm />

}

export default RegisterPage

export async function getServerSideProps(context) {
	const session = await getSession(context)
	if (session)
	{
	  return { redirect: { destination: "/redirect", permanent: false } }
	}
    const data = []
	return {
	  props: {  data }
	}
  }