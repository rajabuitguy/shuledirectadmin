import React from 'react'
import { signOut } from 'next-auth/client'
const Logout = () => {
    React.useEffect(() => {
        signOut({
            callbackUrl: `${window.location.origin}/auth/login`
        })
    }, [])
    return <>Logout</>
}
export default Logout