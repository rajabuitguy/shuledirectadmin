
import React from 'react'
import { useSession,signIn,getCsrfToken } from 'next-auth/client'
import { getSession } from "next-auth/client"

// Begin: Custom
import LoginForm from '../components/auth/LoginForm'
// End: Custom

const login = (props) => {

   const [csrfToken_, setCsrfToken_] = React.useState(null)

   React.useEffect(()=> {
      getCsrfToken()
   }, [])


   const getCsrfToken = async ()=> {
    
   }

   return <div>
        <LoginForm csrfToken = {props.csrfToken } />
     </div>
}

export default login;

export async function getServerSideProps(context) {
  const session = await getSession(context)
  if (session)
  {
    return { redirect: { destination: "/redirect", permanent: false } }
  }

  const csrfToken = await getCsrfToken(context)

  return {
    props: { csrfToken },
  }
}




// import Head from 'next/head'
// import Image from 'next/image'
// import { Typography,Container } from '@mui/material'
// import HomePage from '../components/home/HomePage'
// import { Journey } from '../components/resource/Util'
// import LocalData from '../helper/LocalData'
// 
// import Subjects from '../process/Subjects'
// import Contents from '../process/Contents'
// import React from 'react'
// 
// export default function Home(props) { 
// 
//   React.useEffect(()=>{
//     Contents.processStudyMoreSubjects(null,null,null,null,"home")
//   },[])
// 
//   return <div>
//             <Journey page = {"homePage"} id = {1} keyName = {"page1"} end = {null} />
//             <HomePage subjects = { props.subjectsData}/>
//         </div>
// }
// 
// export async function getServerSideProps(context) {
//         const subjectsArray = await Subjects.findSubjects()
//         const subjectsData = []
//         for (let subject in subjectsArray) {
//             let __default = subjectsArray[subject][0].subject.subject_default
//             let image = (__default.length !== 0) ? __default.image : null
// 
//             let subjectDiv = subject.replace(/\s+/g, '')
//             let result = {
//                 image: image,
//                 style_name: subjectDiv,
//                 name: (subject === 'Information and Computer Studies') ? "ICS" : subject,
//                 data: subjectsArray[subject],
//             }
//             subjectsData.push(result)
//         }
//   return {
//     props: { subjectsData }
//   }
// 
// }

