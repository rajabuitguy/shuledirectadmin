import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import CredentialsProvider from "next-auth/providers/credentials";
import User from "../../../process/User";

// export default NextAuth({
//   // Configure one or more authentication providers
//   providers: [
//     GoogleProvider({
//       clientId: '113362832624-ap1f9oipfd5a0kv549a34v0c5tqbsmqv.apps.googleusercontent.com',
//       clientSecret: 'Th_gkuVoCeoXkzqiTWTq4KDr'
//     }),
//     CredentialsProvider({
//     // The name to display on the sign in form (e.g. 'Sign in with...')
//     name: 'Credentials',
//     // The credentials is used to generate a suitable form on the sign in page.
//     // You can specify whatever fields you are expecting to be submitted.
//     // e.g. domain, username, password, 2FA token, etc.
//     // You can pass any HTML attribute to the <input> tag through the object.
//     credentials: {
//       username: { label: "Username", type: "text", placeholder: "jsmith" },
//       password: {  label: "Password", type: "password" }
//     },
//     async authorize(credentials, req) {
//       // You need to provide your own logic here that takes the credentials
//       // submitted and returns either a object representing a user or value
//       // that is false/null if the credentials are invalid.
//       // e.g. return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
//       // You can also use the `req` object to obtain additional parameters
//       // (i.e., the request IP address)
//       const res = await fetch("/your/endpoint", {
//         method: 'POST',
//         body: JSON.stringify(credentials),
//         headers: { "Content-Type": "application/json" }
//       })
//       const user = await res.json()
//
//       // If no error and we have user data, return it
//       if (res.ok && user) {
//         return user
//       }
//       // Return null if user data could not be retrieved
//       return null
//     }
//   })
//     // ...add more providers here
//   ],
//
//
//
// })

const providers = [
  GoogleProvider({
    clientId: '113362832624-ap1f9oipfd5a0kv549a34v0c5tqbsmqv.apps.googleusercontent.com',
    clientSecret: 'Th_gkuVoCeoXkzqiTWTq4KDr'
  }),
  CredentialsProvider({
    name: "Credentials",
    async authorize(credentials, req) {
      let form = {
        email: credentials.email,
        password: credentials.password,
      };
      const user = await User._login(form);
      if (user) {
        return user;
      } else {
        throw new Error("&email=" + credentials.email);
      }
    },
  }),
];

const callbacks = {
  // Getting the JWT token from API response
  async jwt(token, user) {
    if (user) {
      token.data = user;
    }
    return token;
  },

  async session(session, token) {
    session.data = token.data;
    return session;
  },
};

const options = {
  providers,
  callbacks,
  pages: {
    error: "/auth/form", // Changing the error redirect page to our custom login page
  },
};

export default (req, res) => NextAuth(req, res, options);
