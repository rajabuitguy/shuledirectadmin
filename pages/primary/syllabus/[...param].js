import React from 'react'
import { getSession } from "next-auth/client"
import { useDispatch, useSelector } from 'react-redux'

// Custom
import SyllabusProcess from '../../../process/primary/SyllabusProcess'
import SyllabusPage from '../../../components/primary/syllabus/SyllabusPage'
import { PrimaryTopicsLoader } from '../../../components/skeleton/SkeletonShapes'

// End Custom


const Syllabus = ({subjectId,classId})=> {

	const [syllabus,setSyllabus] = React.useState(null)
	const { active_language:activeLanguage,default_language: defaultLanguage } = useSelector(state => state.language )

	React.useEffect(()=> {
		const languageId =  (activeLanguage && activeLanguage.length > 0) 
							? activeLanguage[0].id 
							: defaultLanguage[0].id
		getStllabusData(subjectId,classId,languageId)
	},[activeLanguage])

	const getStllabusData = async (subjectId,classId,languageId)=> {
		 let _resultArray = await SyllabusProcess.findSubjectSyllabus(subjectId,classId,languageId)
		 setSyllabus(_resultArray)
	}

	return <> 
	    {!syllabus ?(
	    	<PrimaryTopicsLoader width="100%" height="500px" padding="80px"/>
	    ):(
			<SyllabusPage syllabus = { syllabus } subjectId={subjectId} classId={ classId }/>
		)}
	</>
	// return <div></div>
}

export default Syllabus

export async function getServerSideProps(context) {

	const session = await getSession(context)
	if (!session)
	{
	  return { redirect: { destination: "/redirect", permanent: false } }
	}

	const { param }  = context.query
	const subjectId = param[0]
	const classId = param[1]
	if (typeof subjectId !='string' || typeof classId !="string"){
		return { redirect: { destination: "/primary/subjects", permanent: false } }
	}
	// const languageId = 2
	// const syllabus = await SyllabusProcess.findSubjectSyllabus(subjectId,classId,languageId)
	// if (!syllabus)
	// {
		// return { redirect: { destination: "/primary/subjects", permanent: false } }
	// }

	return {
    	props: { subjectId,classId }
  	}

}