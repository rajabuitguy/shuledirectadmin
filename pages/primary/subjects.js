import React from 'react'
import SyllabusProcess from '../../process/primary/SyllabusProcess'
import { getSession } from "next-auth/client"
import { useDispatch, useSelector } from 'react-redux'

// Custom
import SubjectsPage from '../../components/primary/syllabus/SubjectsPage'
// End Custom

const Subjects = ()=> {
  const [subjects, setSubjects] = React.useState(null)
  const { active_language:activeLanguage,default_language: defaultLanguage } = useSelector(state => state.language )

  React.useEffect(()=> {
    const languageName =  (activeLanguage && activeLanguage.length > 0) 
              ? activeLanguage[0].name 
              : defaultLanguage[0].name
    getSubjects(languageName)
  },[])

  const getSubjects = async (languageName)=> {
     const subjectsArray = await SyllabusProcess._findSubjects(languageName)
     const arrayData = []
        for (let subject in subjectsArray) {
            let subjectDiv = subject.replace(/\s+/g, '')
            let result = {
                style_name: subjectDiv,
                name: subject,
                data: subjectsArray[subject],
            }
            arrayData.push(result)
        }
        // setSubjects(arrayData)
     setSubjects(arrayData)
  }

	return <SubjectsPage subjects = { subjects } />

}

export default Subjects

export async function getServerSideProps(context) {
  const session = await getSession(context)
  let data = null
  if (!session)
  {
    return { redirect: { destination: "/", permanent: false } }
  }

  return {
    props: { data }
  }
}