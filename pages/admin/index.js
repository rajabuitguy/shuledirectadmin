import React from 'react'
import { useSession,signIn,getCsrfToken } from 'next-auth/client'
import { getSession } from "next-auth/client"

const Dashboard = ()=> {
    
    return <div>Admin dashboard</div>

}

export default Dashboard

export async function getServerSideProps(context) {
  const session = await getSession(context)
  if (!session)
  {
    return { redirect: { destination: "/redirect", permanent: false } }
  }

  return {
    props: {  },
  }
}