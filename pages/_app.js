import '../styles/globals.scss'
import { Layout } from '../components/layout'
import App from 'next/app'
import {createWrapper} from 'next-redux-wrapper'
import {Provider} from 'react-redux'
import store from '../redux/store/'
import * as SessionProvider from 'next-auth/client'
import 'react-circular-progressbar/dist/styles.css';
// import 'animate.css'


class MyApp extends App{

  static async getInitialProps({Component,ctx}){
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) :{}
    return {pageProps:pageProps}
  }

  render(){
    const {Component,pageProps} = this.props;
    return <SessionProvider.Provider session = {pageProps.session}>
            <Provider store = {store}>
              <Layout>
                  <Component {...pageProps} />
              </Layout>
            </Provider>
          </SessionProvider.Provider>
    }
}

const makeStore = ()=>store

const wrapper = createWrapper(makeStore)
export default wrapper.withRedux(MyApp)
// export default withRedux(makeStore)(MyApp)

// function MyApp({ Component, pageProps }) {
//   return <Layout><Component {...pageProps} /></Layout>
// }

// export default MyApp
