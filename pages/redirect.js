import React from 'react'
import { Typography } from '@mui/material'
import { getSession } from 'next-auth/client'

const Redirect = ({session})=> {
	return <div style={{ 
		width:"100%",
		minHeight:"100%",
		position:"absolute",
		bottom:"0",
		top:"0",
		left:"0",
		right:"0",
		background:"#fff",
		zIndex:'9999',
		display:"flex",
		alignItems:"center",
		justifyContent:"center"
	}}>
		<Typography variant="h2">Wait its redirecting .....</Typography>
	</div>

}

export default Redirect

export async function getServerSideProps(context) {
  const session = await getSession(context)
  if (!session) return { redirect: { destination: "/", permanent: false } } 
  const user = session?.data

	// if (user.profile_type.name==='Admin')
	// {
		return { redirect: { destination: "/primary", permanent: false } }
	// }

  return {
    props: { session }
  }

}