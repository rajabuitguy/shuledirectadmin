import Auth from './Auth'
import Gateway from '../service/Gateway'

class User {

  constructor() {
    this.data = null
    this.serverError = null
    this.validation = { error: null, params: null }
    this.avatar = ''
  }

  /**
   * [register description]
   * @param  {[type]} form            [description]
   * @param  {[type]} activeGroupName [description]
   * @return {[type]}                 [description]
   */
  async processRegisterData(form, activeGroupName = null) {
    // let generatedCodes = (this.isLoggedIn && this.group() === 'Parent') ? this._data().generated_codes : 0
    let username = (activeGroupName === "Student") ? form.username : form.email
    let params = {}

    //register kids
    // if (form.create_form && this.isLoggedIn && this.group() === 'Parent') {
    //   params = {
    //     image: (form.image) ? form.image : this.avatar,
    //     first_name: form.fname,
    //     last_name: form.lname,
    //     email: form.email,
    //     sex: form.gender,
    //     phone: form.phone,
    //     profile_type_id: form.group,
    //     username: username,
    //     password: form.password,
    //     source: form.source,
    //     profiles_id: '',
    //     generated_codes: '',
    //     forms_mid: form.forms_mid,
    //     schools_mid: form.school_id
    //   }
    //   return await this._register(params)
    // }

    //create new account
    if (form.create_form) {
      params = {
        image: (form.image) ? form.image : this.avatar,
        first_name: form.fname,
        last_name: form.lname,
        email: form.email,
        sex: form.gender,
        phone: form.phone,
        profile_type_id: form.group,
        username: username,
        password: form.password,
        source: form.source
      }
      if (activeGroupName === 'Student') {
        params.forms_mid = form ? form["class_name"] ? form["class_name"]:" ":" "
        params.schools_mid = ''
      }
      return await this._register(params)
    }

    //edit existing user
    params = {
      image: (form.image && typeof form.image==='string') ? form.image : this.avatar,
      first_name: (form.fname && typeof form.fname==='string') ? form.fname: '',
      last_name: (form.lname && typeof form.lname==='string') ? form.lname : '',
      email: (form.email && typeof form.email==='string') ? form.email : '',
      sex: (form.gender && typeof form.gender==='string') ? form.gender : '',
      phone: form.phone,
      profile_type_id: form.group,
      forms_mid: form.forms_mid,
      schools_mid: form.school_id
      // username: (form.username && typeof form.username==='string') ? username : ''
    }
    return await this._edit(form.id,params)

  }
  //------------------------------------------------------------------------------------


  /**
   * [findAllGroups description]
   * @return {[type]} [description]
   */
  async findAllGroups() {
    let params = {
      service: {micro: "user", task: "profileTypes/api/find", token: "12234" },
      // where: {  schoolLevelsId: 2 },
    }
    const { data: resultArray } = await Gateway.post(params)

    if (!resultArray) {
      return false
    }

    let data = resultArray
    // let data = {
    //   data:[
    //     {
    //       id:'1',
    //       name:"Student"
    //     }
    //
    //   ]
    // }

    return { data }
  }
  //--------------------------------------------------------------------------------------

  
  userResposeFormatter(result){
    let data = result.data.user_info[0]
    let group = result.data.user_group[0]
    let formattedData = {
                      age_group: null,
                      // class_name: data.class_name,
                      class_info: {
                        id:data.class_info.class_id,
                        name:data.class_info.class_name
                      },
                      cover_photo: data.cover_photo,
                      date_of_birth: data.date_of_birth,
                      email: data.email,
                      first_name: data.first_name,
                      id: data.user_id,
                      user_id: data.user_id,
                      image: data.image,
                      last_name: data.last_name,
                      more_info: data.more_info,
                      phone: data.phone,
                      sex: data.sex,
                      is_cloned: data.is_cloned,
                      profile_type: {
                        id: group.id,
                        name: group.name,
                      },
                      user_token: (typeof result.data.user_tokens==='object' &&  result.data.user_tokens) ? result.data.user_tokens[0] : null
            }
        return formattedData
 }

  /**
   * [_login description]
   * @param  {[type]} form [description]
   * @return {[type]}      [description]
   */
  async _login(form) {
    // let params = {
    //   service: { micro: "user", task: "profileLogin", token: "12234" },
    //   data: {
    //     username: form.email,
    //     password: form.password,
    //     grent_type: "password",
    //   }
    // }
    let request = {
      service: {
        micro: "user_management",
        task: "signin",
        token: 12234
      },
      POST: {
        username: form.email,
        password: form.password,
        social_auth: (typeof form.social_auth==='boolean') ? form.social_auth : false 
      }
    }
    let { data: result, networkError } = await Gateway.post(request)
    if (networkError){
        return false
    }

    if (result.code !==200){
         return false
    }

    if (!result.data){
        return false
    }

    let user = this.userResposeFormatter(result)
    return user

  }
  //--------------------------------------------------------------------------------------


  /**
   * [edit description]
   * @param  {[type]} id   [description]
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  async _edit(id, data) {
     let params = {
      service: {
        micro: "user",
        task: "profileRegister/api/update",
        token: "12234",
      },
      data: data,
      where: { id: id },
    }
    return await Gateway.post(params)
  }
  //-----------------------------------------------------------------------------------

  /**
   * [_register description]
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  async _register(data) {
    let params = {
      service: {
        micro: "user",
        task: "profileRegister/api/create",
        token: "12234",
      },
      data: data,
    }
    return await Gateway.post(params)
  }
  //---------------------------------------------------------------------------------

  /**
   * [findById description]
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  async _findById(id,param=null) {
     let data = {}
    if (param && param==='username')
    {
      data.username = id 
    }
    else
    {
      data.id = id
    }

     let params = {
      service: {
        micro: "user",
        task: "profile/api/find",
        token: "12234"
      },
      where: data,
    }
    return await Gateway.post(params)
  }
  //-------------------------------------------------------------------------------------


  /**
   * [findChildById description]
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  async findChildrendById(id) {
    let code = this._data().generated_codes
    let params = {
      parent_id: id,
      parent_code: code
    }
    // return await Api.findUserChildrenData(params)
  }
  //--------------------------------------------------------------------------------------


  /**
   * [data description]
   * @return {[type]} [description]
   */
  _data() {
    return Auth.userAuthData()
  }
  //------------------------------------------------------------------------------------


  /**
   * [logout description]
   * @return {[type]} [description]
   */
  logout() {
    return Auth.userLogout()
  }
  //------------------------------------------------------------------------------------


  /**
   * [isLoggedIn description]
   * @return {Boolean} [description]
   */
  isLoggedIn() {
    return Auth.isLoggedInCheck()
  }
  //-------------------------------------------------------------------------------------


  /**
   * [isAdmin description]
   * @return {Boolean} [description]
   */
  isAdmin() {
    return Auth.isAdminCheck()
  }
  //-------------------------------------------------------------------------------------


  /**
   * [isParent description]
   * @return {Boolean} [description]
   */
  isParent() {
    return Auth.isParentCheck()
  }
  //------------------------------------------------------------------------------------

  /**
   * [isStudent description]
   * @return {Boolean} [description]
   */
  isStudent() {
    return Auth.isStudentCheck()
  }
  //--------------------------------------------------------------------------------------

  /**
   * [isTeacher description]
   * @return {Boolean} [description]
   */
  isTeacher() {
    return Auth.isTeacherCheck()
  }
  //--------------------------------------------------------------------------------------

  /**
   * [group description]
   * @return {[type]} [description]
   */
  group() {
    return Auth.userActiveGroup()
  }
  //------------------------------------------------------------------------------------

  /**
   * [_findAllUsers description]
   * @return {[type]} [description]
   */
  async _findAllUsers() {
    let params = {
      where: { date: '' },
      groupBy: '',
      page: 0
    }
    // return await Api.findAllUsersData(params)
  }
  //------------------------------------------------------------------------------------

  /**
   * [deleteUser description]
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  async _deleteUser(id) {
    // return await Api.deleteUserData(id)
  }
  //-------------------------------------------------------------------------------------


}

export default new User()
