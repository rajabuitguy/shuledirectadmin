import LocalData from '../helper/LocalData'

class Auth{

	constructor(){
		let data = LocalData.findAuthUser()
		this.data = data
		this.authenticated = false
	}

	/**
	 * [userLogout description]
	 * @return {[type]} [description]
	 */
	userLogout(){
		let result = LocalData.removeAuthUser()
		if (!result)
		{
				return false
		}
		// window.location.reload()
		return true
	}
	//----------------------------------------------------------------

	/**
	 * [isLoggedInCheck description]
	 * @return {Boolean} [description]
	 */
	isLoggedInCheck(){
		return (this.data) ? true : false
	}
	//-----------------------------------------------------------------

	/**
	 * [userAuthData description]
	 * @return {[type]} [description]
	 */
	userAuthData(){
		return (this.data) ? this.data[0] : null
	}
	//------------------------------------------------------------------

	/**
	 * [isAdminCheck description]
	 * @return {Boolean} [description]
	 */
	isAdminCheck(){
		let role = null
		if (this.data && this.data.length>0)
		{
			this.data.forEach( d => {
					role = d.profile_type.name
			})
		}
		return (role==="Admin") ? role : null
	}
	//------------------------------------------------------------------

	/**
	 * [isParentCheck description]
	 * @return {Boolean} [description]
	 */
	isParentCheck(){
		let role = null
		if (this.data && this.data.length>0)
		{
			this.data.forEach( d => {
					role = d.profile_type.name
			})
		}
		return (role==="Parent") ? role : null
	}
	//-----------------------------------------------------------------

	/**
	 * [isStudentCheck description]
	 * @return {Boolean} [description]
	 */
	isStudentCheck(){
		let role = null
		if (this.data && this.data.length>0)
		{
			this.data.forEach( d => {
					role = d.profile_type.name
			})
		}
		return (role==="Student") ? role : null
	}
	//-------------------------------------------------------------------

	/**
	 * [isTeacherCheck description]
	 * @return {Boolean} [description]
	 */
	isTeacherCheck(){
		let role = null
		if (this.data && this.data.length>0)
		{
			this.data.forEach( d => {
					role = d.profile_type.name
			})
		}
		return (role==="Teacher") ? role : null
	}
	//---------------------------------------------------------------------

	/**
	 * [userActiveGroup description]
	 * @return {[type]} [description]
	 */
	userActiveGroup(){
		let role = null
		if (this.data && this.data.length>0)
		{
			this.data.forEach( d => {
					role = d.profile_type.name
			})
		}
		return (role) ? role : null
	}
	//-------------------------------------------------------------------

}

export default new Auth()
