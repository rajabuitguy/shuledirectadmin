import Utils from "../helper/Utils"
import Gateway from "../service/Gateway";

class GalleryProcess{

	constructor() {
    this.domain = typeof window !== "undefined" ? window.location.origin : "";
    this.GALLERY = `GALLERY::${this.domain}`
  }

// 	saveUploadedFileData(data){
//        
//        if (typeof window === "undefined") {
// 	      return false;
// 	    }
// 
// 	    const encryptedData = Utils.encryptData(data);
// 	    localStorage.setItem(this.GALLERY, encryptedData);
// 	    if (data && localStorage.getItem(this.GALLERY)){
// 	      return true
// 	    }else{
// 	      return false
// 	    }
// 	}

// 	processFileToSave(__data){
// 		let data = __data.data[0]
// 		let data_ = {
//        	   id: data.id,
//        	   file_type: data.file_extension,
//        	   resource_type: data.media_types,
//        	   url: data.url,
//        	   created_at: data.created_at,
//        	   file_size: data.file_size,
//        	   file_name: data.file_name
//        	}
// 		dataArray.push(data_)
// 
// 		let resultArray = this.findSavedGallery();
// 	    if (!resultArray) {
// 	      //no accounts found, then save new account
//        	  this.saveUploadedFileData(dataArray)
// 	      return false;
// 	    }
// 
// 	    resultArray.forEach((d) => {
//       		dataArray.push(d)
//     	})
// 
//        	this.saveUploadedFileData(dataArray)
// 	}

	async findSavedGalleryByUserId(userId){
		 	let params = {
		  	 where: {
		  	 	 userId: userId
		  	 }
		  }
		  const { data: dataArray } = await Gateway.____post(params)
		  if (!dataArray && dataArray.data.length === 0){
		  	return null
		  }
		  return dataArray
	}
// 
// 	findSavedGallery(){
// 		 
//       if (typeof window === "undefined") {
//         return null;
//       }
//    
//       let dataArray = localStorage.getItem(this.GALLERY)
//       return dataArray ? Utils.decryptData(dataArray) : null
//   	}


}

export default new GalleryProcess()