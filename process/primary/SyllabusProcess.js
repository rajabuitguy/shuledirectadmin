import {Data} from '../../data/primary/syllabus'
import Gateway from "../../service/Gateway";
class SyllabusProcess{

	/**
	 * [_findSubjects description]
	 * @return {[type]} [description]
	 */
	async _findSubjects(languageName=null){
		let params = {
	      service: {
	        micro: "response",
	        task: "getSubjectClassObject",
	        token: 12234,
	      },
	      where: { 
	      	level: "primary", 
	      	"language" : languageName,
	        "is_admin": true,
	      },
	    }

	    const { data: dataArray } = await Gateway.post(params)
    	let formsBySubject = []

	    if (!dataArray) {
	       return false
	    }

	    const resultArray = dataArray.data.filter((data_) => {
            if (  data_.classs.roman === "I" || data_.classs.roman === "II") {
              	return data_;
            }
	    })

	    if (!resultArray){
	    	return null
	    }

	    resultArray.forEach((_data) => {

	      	_data.subject.forEach((subject) => {

		        if (!formsBySubject[subject.name]) {
		          formsBySubject[subject.name] = []
		        }

		        let entry = {}
		        entry.form = _data.classs
		        entry.subject = subject
		        formsBySubject[subject.name].push(entry)
	      	})

	    })

     	return formsBySubject

	}
	//------------------------------------------------------------------------------------------------------------------
	

	/**
	 * [findSubjectSyllabus description]
	 * @param  {[type]} subjectId  [description]
	 * @param  {[type]} classId    [description]
	 * @param  {[type]} languageId [description]
	 * @return {[type]}            [description]
	 */
	async findSubjectSyllabus(subjectId,classId,languageId){
		let params = {
	      service: {
	        micro: "response",
	        task: "getSylabusAndResourceBySubjectId",
	        token: 12234
	      },
	      "where":{
		        "subjectId":subjectId,
		        "classsId" :classId,
		        "languageId":languageId
		    }
	    }
	    const { data: dataArray } = await Gateway.post(params)


		return dataArray
	}
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * [saveResourceDetails description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	async saveResourceDetails(data){
		console.log("Saving Details Request", data)
		let params = {
	      service: {
	        micro: "response",
	        task: "contentCovers",
	        token: 12234,
	      },
	      data
	    }
	    const { data: dataArray } = await Gateway.post(params)
		console.log("Saving Details Response", dataArray)
	    return (dataArray && dataArray.data && dataArray.data.length > 0) ? dataArray.data : null
	}
	//-------------------------------------------------------------------------------------------------------------------

	/**
	 * [saveContentResources description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	async saveContentResources(data){
		console.log("Saving Resources Request", data)
		let params = {
	      service: {
	        micro: "response",
	        task: "contentsBaseData",
	        token: 12234,
	      },
	      data:data
	    }
	    const { data: dataArray } = await Gateway.post(params)
	    console.log("Saving Resources Response", dataArray)

	    return (dataArray && dataArray.data && dataArray.data.length > 0) ? dataArray.data : null
	}
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * [_saveResourceContent description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	async _saveResourceContent(data){
		let _return = {
			details: null,
			data: null
		}
		if (data.learning_content){
			let learningContent = data.learning_content.data
			if (learningContent.image_audio_resources || learningContent.video_content){
				//Saving the learning content resources
				_return.data = await this.saveContentResources(data)
			}else{
				//save content details
				// data.learning_content.content_type = "content_details"
				_return.details = await this.saveResourceDetails(data)
			}
		}

		if (data.game_resources){
			let gameResourcesContent = data.game_resources.data
			if (gameResourcesContent.game_image_audio_resources || gameResourcesContent.number_range_data){
				_return.data = await this.saveContentResources(data)
			}else{
				//save content details
				// data.game_resources.content_type = "content_details"
				_return.details  = await this.saveResourceDetails(data)
			}
		}

		return _return

	}
	//------------------------------------------------------------------------------------------------------------------
	
	/**
	 * [findResourcesDataById description]
	 * @param  {[type]} data [description]
	 * @return {[type]}    [description]
	 */
	async findResourcesDataById(data){
		let params = {
	      service: {
	        micro: "response",
	        task: "getContentBaseBlocksData",
	        token: 12234,
	      },
	      "where":{
		      "sourceId":data.sourceId,
		      "baseId":data.baseId,
		      "content_type" : (data.content_type) ? data.content_type: 'direct_video_upload'
		   }
	    }
	    // console.log("Resource by id params *****", params)
	    const { data: dataArray } = await Gateway.post(params)
	    console.log("****dataArray ****", dataArray)
	    return (dataArray && dataArray.data && dataArray.data.length > 0) ? dataArray.data[0] : null
	}
	//------------------------------------------------------------------------------------------------------------------

	async deletePublishResources(data){
		console.log("deleted resources", data)
		let params = {
	      service: {
	        micro: "response",
	        task: "contentBaseChangeActionData",
	        token: 12234,
	      },
	      "where":data
	    }
	    const { data: dataArray } = await Gateway.post(params)
	    return (dataArray) ? dataArray : null
	}


	/**
	 * [findResourcesData description]
	 * @return {[type]} [description]
	 */
	findGamesResourcesData(){
		return Data.games_resource_list
	}
	//------------------------------------------------------------------------------------------------------------------


}

export default new SyllabusProcess()