import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
	setResourceContentType,
	setResourceContentId,
	setResourceParentData,
	clearActiveContentToSave,
	setActiveFormInnerLayout,
	cancelActiveFormOverlay,
	setFormSubmitError
} from '../../redux/reducers/primary/ResourceFormIndexSlice'
import {
	setDetailsData,
	setImageAudioContent,
	clearImageAudioCoontent,
	addNewImageAudioForm,
	clearVideoContent,
	updateAudioContentData,
	updateAudioCoverData,
	updateVideoContentData,
	updateContentDetailsCoverImage,
	setVideoContent,
	updateImageAudiContent
} from '../../redux/reducers/primary/LearningContentSlice'

import {
  	setNumberRangeForm,
  	setResourceDetails,
  	setResourceStepsOptionBlock,
  	setNumberUpdateData 
} from '../../redux/reducers/primary/GameResourcesSlice'


import SyllabusProcess from './SyllabusProcess'

const RsourceHelper = ()=> {
	
	const dispatch = useDispatch()

	const {	
		resource_content_type:resourceContentType,
		active_content_to_save: activeContentToSave,
		resource_parent_data,resource_content_id } = useSelector( state => state.resource_form_index )

	const {
			details_data:_learningContent,
			video_content:videoContent,
			image_audio_content:imageAudioContent
	} = useSelector(state => state.learning_content)

	const { active_language:activeLanguage } = useSelector(state => state.language )

	const {
		details_data:_gameResourceDetails,
		number_range: gameNumberFormData,
		resource_steps_block: gameRsourcesBlocks
	} = useSelector(state=> state.game_resources)

    const languageId = activeLanguage[0].id
    

	const updateGameResourcesDetailsHelper = (data) => {
		dispatch(setResourceDetails({
			game_resource_type: (data.content_type !=='content_details') ? data.content_type : _gameResourceDetails.game_resource_type,
	 		data:{
				id:data.id,
				unique_key_code:data.unique_key_code,
				subject_id:data.subject_id,
				classs_id:data.classs_id,
				topic_id:data.topic_id,
				subtopic_id:data.subtopic_id,
				name:data.name,
				indicator:"update",
				baseId: data.baseId,
				sourceId:data.sourceId,
				cover_image: data.cover_image
			}
		}))
	}

	const updateLearningContentDetailsHelper = (data)=> {
		dispatch(setDetailsData({
			content_type: (data.content_type !=='content_details') ? data.content_type : _learningContent.content_type,
	 		data:{
				id:data.id,
				unique_key_code:data.unique_key_code,
				subject_id:data.subject_id,
				classs_id:data.classs_id,
				topic_id:data.topic_id,
				subtopic_id:data.subtopic_id,
				name:data.name,
				indicator:"update",
				baseId: data.baseId,
				sourceId:data.sourceId,
				cover_image: data.cover_image
			}
		}))
	}

	const getResourceByIdHeper = async (data)=> {
		// console.log("Resource by id request",data)
		const dataArray = await SyllabusProcess.findResourcesDataById(data)
		// console.log("Resource by id response",dataArray)
		if (dataArray.game_resources && dataArray.game_resources.length > 0){
			
			//process games resource details
			let resourcesData = dataArray.game_resources[0]
			updateGameResourcesDetailsHelper(resourcesData) //update the resource details

			//process image audio game resources
			if (resourcesData.data.game_image_audio_resources && resourcesData.data.game_image_audio_resources.length > 0){
				let imageAudioResourceData = resourcesData.data.game_image_audio_resources
				dispatch(setResourceStepsOptionBlock(imageAudioResourceData))
			}

			//process game number range resources
			if (resourcesData.data.number_range_data && resourcesData.data.number_range_data.length > 0){
				let numberRangeData = resourcesData.data.number_range_data
				dispatch(setNumberUpdateData(numberRangeData))
			}

			dispatch(setResourceContentType({  index_value: 1, name: "games_resources" })) //make the Game resource active

		}else if (dataArray.learning_content && dataArray.learning_content.length > 0){
			let _resourcesData = dataArray.learning_content[0]
			updateLearningContentDetailsHelper(_resourcesData)

			//Process direct video content upload
			if (_resourcesData.data.video_content && _resourcesData.data.video_content.length > 0){
				let directVideoContent = _resourcesData.data.video_content
				dispatch(setVideoContent(directVideoContent))
			}

			//Process audio image resources
			if (_resourcesData.data.image_audio_resources && _resourcesData.data.image_audio_resources.length > 0){
				let imageAudioContent = _resourcesData.data.image_audio_resources
				dispatch(updateImageAudiContent(imageAudioContent))
			}

			dispatch(setResourceContentType({  index_value: 0, name: "learning_content" })) //make the Game resource active

		}
		
		dispatch(cancelActiveFormOverlay())
		return false
	}

	const saveResourceHelper = async ()=> {
		let _resourceId = null
		let _contentData = {
  			 learning_content: null,
  			 game_resources: null
  		}


  		if (resourceContentType.name==="learning_content"){
		      _contentData.learning_content = {
		      	  id: _learningContent.data.id,
		      	  subject_id: _learningContent.data.subject_id,
		      	  classs_id: _learningContent.data.classs_id,
		      	  topic_id: _learningContent.data.topic_id,
		      	  subtopic_id: _learningContent.data.subtopic_id,
		      	  name: _learningContent.data.name,
		      	  // content_type:_learningContent.content_type,
		      	  content_type:activeContentToSave.content_type,
		      	  indicator:_learningContent.data.indicator,
		      	  unique_key_code: _learningContent.data.unique_key_code,
		      	  cover_image: _learningContent.data.cover_image,
		      	  languages_id: languageId,
		      	  baseId: _learningContent.data.baseId,
		      	  sourceId: _learningContent.data.sourceId,
		      	  data:{
			      	  video_content:  activeContentToSave.content_type === 'direct_video_upload' ? activeContentToSave.data : null,
			      	  image_audio_resources:  activeContentToSave.content_type === 'image_sound_upload' ? activeContentToSave.data : null
		      	  } 
      		}

  		}else if (resourceContentType.name==="games_resources"){
  			  _contentData.game_resources = {
		      	  id: _gameResourceDetails.data.id,
		      	  subject_id: _gameResourceDetails.data.subject_id,
		      	  classs_id: _gameResourceDetails.data.classs_id,
		      	  topic_id: _gameResourceDetails.data.topic_id,
		      	  subtopic_id: _gameResourceDetails.data.subtopic_id,
		      	  name: _gameResourceDetails.data.name,
		      	  // content_type:_gameResourceDetails.game_resource_type,
		      	  content_type:activeContentToSave.content_type,
		      	  indicator: _gameResourceDetails.data.indicator,
		      	  unique_key_code: _gameResourceDetails.data.unique_key_code,
		      	  cover_image: _gameResourceDetails.data.cover_image, 
		      	  languages_id: languageId,
		      	  baseId: _gameResourceDetails.data.baseId,
		      	  sourceId: _gameResourceDetails.data.sourceId,
		      	  data: {
			      	  game_image_audio_resources: activeContentToSave.content_type === 'image_game_resource' ? activeContentToSave.data : null,
			      	  number_range_data: activeContentToSave.content_type === 'number_range' ? activeContentToSave.data : null
		      	  }
      		}
  		}

  		console.log("_contentData",_contentData)
  		// return false

	  	let dataArray = await SyllabusProcess._saveResourceContent(_contentData)
	  	if (_contentData.game_resources && dataArray.details){
	  		let dataResult = dataArray.details[0].game_resources[0]
	  	 	_resourceId = dataResult
	  	  	updateGameResourcesDetailsHelper(dataResult)

	  	}else if (_contentData.game_resources && dataArray.data){
	  	 	let dataResult = dataArray.data[0].game_resources
	  	 	_resourceId = dataResult

	  	 	if (dataResult.length === 0){
	  	 		dispatch(setFormSubmitError())
	  	 		return false
	  	 	}

	  	    await getResourceByIdHeper(dataResult[0])

	  	}else if (_contentData.learning_content && dataArray.details){
	  		let dataResult = dataArray.details[0].learning_content[0]
	  		_resourceId = dataResult
	  	  	updateLearningContentDetailsHelper(dataResult)


	  	}else if (_contentData.learning_content && dataArray.data){
	  		let dataResult = dataArray.data[0].learning_content
	  	 	_resourceId = dataResult

	  	 	if (dataResult.length === 0){
	  	 		dispatch(setFormSubmitError())
	  	 		return false
	  	 	}

	  	    await getResourceByIdHeper(dataResult[0])
	  	}

	  	// console.log("_resultData",_resourceId)
  		// console.log("_contentData",dataArray)
	  	//get cretse/updated resource by id
	  	// await getResourceByIdHeper(_resourceId)

	}

	const deleteGameResources = async (data)=> {
		let dataArray = await SyllabusProcess.deletePublishResources(data)
		console.log("dataArray",dataArray)
	}

	const deleteLearningResource = async (data)=> {
		let dataArray = await SyllabusProcess.deletePublishResources(data)
		console.log("dataArray",dataArray)

	}

	return {
			updateGameResourcesDetailsHelper,
			saveResourceHelper,
			getResourceByIdHeper,
			deleteGameResources,
			deleteLearningResource
		}

}

export default RsourceHelper