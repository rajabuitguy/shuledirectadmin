import Gateway from "../service/Gateway";
class General {
  /**
   * [findClasses description]
   * @return {[type]} [description]
   */
  async findClasses() {
    let params = {
      service: {
        micro: "response",
        task: "getSubjectClassObject",
        token: 12234,
      },
      where: { level: "Secondary" },
    };
    const { data: resultArray } = await Gateway.post(params);
    if (!resultArray) {
      return false;
    }

    const _classArray = resultArray.data.map((data) => {
      return {
        id: data.classs.id,
        name: data.classs.name,
      };
    });

    const __classArray = _classArray.filter((data_) => {
      if (
        data_.name !== "Darasa la Nne" &&
        data_.name !== "Darasa la Saba " &&
        data_.name !== "Extracurricular Subj"
      ) {
        return data_;
      }
    });
    return __classArray ? __classArray : null;
  }
  //-----------------------------------------------------------------------

  /**
   * [_searchForSchool description]
   * @param  {[type]} searchName [description]
   * @return {[type]}      [description]
   */
  async _searchForSchool(searchName) {
    let _params = {
      word: searchName
    }
    let params = {
      service: {
        micro: "school",
        task: "schoolInformation/api/search",
        token: 12234
      },
      search: _params
    }
    const { data: resultArray } = await Gateway.post(params)
    if (!resultArray) {
      return false
    }
    return resultArray && resultArray.data.length > 0 ? resultArray.data : null;
  }
  //------------------------------------------------------------------------------

  searchAll = async (searchWord) => {
    const req = {
      service: {
        micro: "response",
        task: "syllabusSearch",
        token: 12234,
      },
      search: {
        word: searchWord,
        // formId: [2, 3, 4], // optional
      },
    };

	 const { data:resultArray, networkError} = await Gateway.post(req);
    if (networkError)
    {
      return []
    }
    
  	return resultArray && resultArray.data && resultArray.data.length > 0 ? resultArray.data : []

  }




}

export default new General();
