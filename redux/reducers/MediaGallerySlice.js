import { createSlice } from '@reduxjs/toolkit'

const initialActiveSegementToUpload =  {
	       	source_name:"",
	       	data: null,
	       	active_status: false,
	       	media_type:""
       }

export const MediaGallerySlice = createSlice({
	name:"Gallery",
	initialState: {
       gallery_data: [],
       single_media_data: [],
       show_media_gallery: false,
       active_segment_to_upload_media:initialActiveSegementToUpload
	},
	reducers:{
		setGalleryData: (state, action)=> {
			state.gallery_data = action.payload
		},
		setMediaSingleData: (state, action)=> {
			state.single_media_data = action.payload
		},
		showMediaGallery: (state)=> {
			state.show_media_gallery = !state.show_media_gallery
		},
		activeSegmentToUploadMedia: (state,action)=> {
			state.active_segment_to_upload_media = action.payload
		},
		clearActiveSegmentToUploadMedia: (state,action)=> {
			state.active_segment_to_upload_media = initialActiveSegementToUpload
		}
	}
})

export const { 
	setGalleryData,
	setMediaSingleData,
	showMediaGallery,
	activeSegmentToUploadMedia,
	clearActiveSegmentToUploadMedia 
} = MediaGallerySlice.actions
export default MediaGallerySlice.reducer