import { createSlice } from '@reduxjs/toolkit'
import LocalData from '../../helper/LocalData'

const initLanguage = [
	{
		id:"1",
		name:"en"
	},
	{
		id:"2",
		name:"sw"
	}
]

const defaultLanguage = initLanguage.filter(data=> data.name==="sw")
const resultArray = LocalData.getActiveLanguage()
const activeLanguage = (resultArray) ? resultArray : initLanguage.filter(data=> data.name==="sw") 

export const LanaguagesSlice = createSlice({
	name:"Language",
	initialState: {
       language_data:initLanguage,
       active_language: activeLanguage,
       default_language:defaultLanguage
	},
	reducers:{
		setLangaue: (state, action)=> {
			state.language_data = action.payload
		},
		setActiveLangauge: (state,action)=> {
            state.active_language = action.payload
		}
	}
})

export const { 
	setLangaue,
	setActiveLangauge
} = LanaguagesSlice.actions
export default LanaguagesSlice.reducer