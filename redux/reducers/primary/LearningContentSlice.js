import { createSlice } from '@reduxjs/toolkit'
// Custom
import { SettingdArray } from './SettingsData'
// eNDcUSTOM

const initialContentDetails = {
	content_type:"direct_video_upload",
	data:{ 
		id:"",
		unique_key_code:"",
		subject_id:'',
		classs_id:'',
		topic_id:'',
		subtopic_id:'',
		baseId: '',
		sourceId:'',
		name:"",
		indicator:"create",
		active_status: true,
		cover_image:[
		    {
		    	id:"",
		   	  	url:"", 
				file_type:"", 
				file_name:"", 
				media_types:"",
				file_extension:"",
				file_size:"",
				mapping_id:"",
				parent_id:"",
				dest:"file_contents"
		    }
		]
	}
}
const initialVideoContent = [
    {
      block_info:{ id: ""},
   	  media:{
   	  	 id:"",
	   	  url:"", 
	   	  file_type:"",
	   	  file_name:"Video file name",
	   	  media_types:"",
	   	  file_extension:"",
	   	  file_size:"",
	   	  mapping_id:"",
		  parent_id:"",
	   	  dest:"file_contents",
		  cover_resource:[]
	   	},
	   	settings_data: SettingdArray
    }
]
const initialImageAudioContent = {
	active_label:"",
	data:[
		{
			content: [
			    {
			   	  	audio_cover:{
						id:'',
						url:'',
						file_type:'',
						file_name:'',
						active_label: '',
						media_types:'',
						file_extension:"",
						file_size:"",
						mapping_id:"",
						parent_id:"",
						dest:"file_contents"
					},
					audio_content:{
						id:'',
						url:'',
						file_type:'',
						file_name:'',
						media_types:'',
						file_extension:"",
						file_size:"",
						mapping_id:"",
						parent_id:"",
					    dest:"file_contents"
					} 
			    }
			],
			block_info: {id:""},
		   	settings_data: SettingdArray
	    }
    ]
}

const imageAudioObject = initialImageAudioContent.data[0].content[0]
const videResourceObject = initialVideoContent[0]

export const LearningContentSlice = createSlice({
	name: "LearningContent",
	initialState: {
		details_data: initialContentDetails,
		video_content: initialVideoContent,
		image_audio_content: initialImageAudioContent,
		form_error: false
	},
	reducers: {
		setDetailsData: (state, action)=> {
			state.details_data = {...state.details_data, ...action.payload}
		},
		setImageAudioContent: (state, action)=> {
			state.image_audio_content = {...state.image_audio_content, ...action.payload }
		},
		updateImageAudiContent: (state,action)=> {
			 let dataArray = action.payload && action.payload.map(data => {
			 	   return {
			 	   	   	block_info: data.block_info,
						content: data.content,
						settings_data: (data.settings_data && data.settings_data.length > 0) ? data.settings_data : SettingdArray
			 	   }
			 })
			 state.image_audio_content = { ...state.image_audio_content}
			 state.image_audio_content.data = dataArray
		},
		setVideoContent: (state,action) => {
			state.video_content = action.payload
		},
		setFormError: (state,payload)=> {
			state.form_error  = action.payload
		},
		clearImageAudioCoontent: (state)=> {
			state.image_audio_content = initialImageAudioContent
		},
		clearVideoContent: (state)=> {
			state.video_content = initialVideoContent
		},
		clearContentDetailsCoverImage: (state)=> {
			state.details_data = initialContentDetails
		},
		addNewImageAudioForm: (state,action)=> {
			state.image_audio_content = {...state.image_audio_content }
			state.image_audio_content.data = [...state.image_audio_content.data,{
				block_info: { id: "" },
				content:[imageAudioObject],
				settings_data:SettingdArray
			}]
		},
		addNewVideoResource: (state, action)=> {
			state.video_content = [...state.video_content,videResourceObject]
		},
		removeVideoResource: (state,action)=> {
			state.video_content = [...state.video_content]
	    	state.video_content.splice(action.payload, 1)
		},
		removeImageAudioBlock: (state,action)=> {
			state.resource_steps_block = {...state.image_audio_content }
	    	state.image_audio_content.data.splice(action.payload, 1)
		},
		updateContentDetailsCoverImage: (state,action) => {
			state.details_data = {...state.details_data }
			state.details_data.data.cover_image[action.payload.index] = {
				...state.details_data.data.cover_image[action.payload.index],
				id:action.payload.media.id,
				file_name:action.payload.media.file_name,
				url: action.payload.media.url,
				file_type:action.payload.media.file_type,
				media_types:action.payload.media.media_types,
				file_extension:action.payload.media.file_extension,
				file_size:action.payload.media.file_size,
			}
		},
		updateVideoContentData: (state,action)=> {
			state.video_content = [...state.video_content]
			state.video_content[action.payload.index] = {
				...state.video_content[action.payload.index],
				media:{
					...state.video_content[action.payload.index].media,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size,
				}
			}
		},
		updateVideoContentCover: (state, action)=> {
			state.video_content = [...state.video_content]
			state.video_content[action.payload.index.parent_index].media.cover_resource[action.payload.index.child_index] = {
				...state.video_content[action.payload.index.parent_index].media.cover_resource[action.payload.index.child_index],
				id:action.payload.media.id,
				file_name:action.payload.media.file_name,
				url: action.payload.media.url,
				file_type:action.payload.media.file_type,
				media_types:action.payload.media.media_types,
				file_extension:action.payload.media.file_extension,
				file_size:action.payload.media.file_size,
			}
		},
		addNewVideoContentCover: (state, action)=> {
			state.video_content = [...state.video_content]
			state.video_content[action.payload] = {
				...state.video_content[action.payload],
				media:{
					...state.video_content[action.payload].media,
					cover_resource: [ ...state.video_content[action.payload].media.cover_resource, {
						id:"",
						file_name:"",
						url: "",
						file_type:"",
						media_types:"",
						file_extension:"",
						file_size:"",
						mapping_id:"",
						parent_id:"",
						unique_code: ""
					}]
				}
			}
		},
		updateAudioCoverData: (state, action)=> {
			state.image_audio_content = { ...state.image_audio_content }
			state.image_audio_content.active_label = `active_label_${action.payload.index.parent_index}`
			state.image_audio_content.data[action.payload.index.parent_index].content[action.payload.index.child_index] = {
				audio_cover: {
					...state.image_audio_content.data[action.payload.index.parent_index].content[action.payload.index.child_index].audio_cover,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size,
					active_label: `active_label_${action.payload.index.parent_index}` 
				},
				audio_content: { ...state.image_audio_content.data[action.payload.index.parent_index].content[action.payload.index.child_index].audio_content}
			}
		},
		updateAudioContentData: (state,action)=> {

			state.image_audio_content = { ...state.image_audio_content }
			state.image_audio_content.active_label = `active_label_${action.payload.index.parent_index}`
			state.image_audio_content.data[action.payload.index.parent_index].content[action.payload.index.child_index] = {
				audio_cover: {
					...state.image_audio_content.data[action.payload.index.parent_index].content[action.payload.index.child_index].audio_cover,
					active_label: `active_label_${action.payload.index.parent_index}` 
				},
				audio_content: {
					...state.image_audio_content.data[action.payload.index.parent_index].content[action.payload.index.child_index].audio_content,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size
				}
			}

		}
	}
})

export const { 
	setLearningContent,
	setDetailsData,
	setImageAudioContent,
	clearImageAudioCoontent,
	clearVideoContent,
	addNewImageAudioForm,
	updateAudioContentData,
	updateAudioCoverData,
	updateVideoContentData,
	updateContentDetailsCoverImage,
	clearContentDetailsCoverImage,
	setFormError,
	setVideoContent,
	removeImageAudioBlock,
	updateImageAudiContent,
	addNewVideoResource,
	removeVideoResource,
	updateVideoContentCover,
	addNewVideoContentCover
} = LearningContentSlice.actions
export default LearningContentSlice.reducer