export const SettingdArray =  [
	{
		id:"",title:"Instruction (Audio/sound)",type:"audio",slug:"instruction",
		media:{ id:"", 
				url:"", 
				file_type:"", 
				file_name:"", 
				media_types:"",
				file_extension:"",
				file_size:"",
				mapping_id:"",
				parent_id:"",
			    dest:"file_contents"
			} 
	},
	{
		id:"",title:"Tips (Video)",type:"video",slug:"tips_slug",
		media:{ id:"", 
				url:"", 
				file_type:"", 
				file_name:"", 
				media_types:"",
				file_extension:"",
				file_size:"",
				mapping_id:"",
				parent_id:"",
			    dest:"file_contents"
			}
	},
	{
		id:"",title:"Media on sucess (Audio/video)",type:"video",slug:"on_success",
		media:{ id:"", 
				url:"", 
				file_type:"", 
				file_name:"", 
				media_types:"",
				file_extension:"",
				file_size:"",
				mapping_id:"",
				parent_id:"",
			    dest:"file_contents"
			} 
	},
	{
		id:"",title:"Media on fail (audio/video)",type:"video",slug:"on_fail",
		media:{ id:"", 
				url:"", 
				file_type:"", 
				file_name:"", 
				media_types:"",
				file_extension:"",
				file_size:"",
				mapping_id:"",
				parent_id:"",
			    dest:"file_contents"
			}
	}
]