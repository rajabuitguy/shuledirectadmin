import { createSlice } from '@reduxjs/toolkit'

const initialData = []

export const DeletePublishActionBarSlice = createSlice({

	name: "DeletePublishActionBar",
	initialState:{
		delete_publish_data: initialData,
		show_action_bar: false,
		delete_mode: false
	},
	reducers: {
		 setDeletePublishData: (state, action)=> {
		 	 state.delete_publish_data = [ ...state.delete_publish_data, action.payload ]
		 	 // state.delete_publish_data = action.payload
		 },
		 removeDataById: (state,action)=> {
		 	let dataArray = state.delete_publish_data.filter( data=> data.id !== action.payload )
		 	state.delete_publish_data = dataArray
		 },
         setShowActionBar: (state,action)=> {
         	if (state.delete_publish_data.length > 0){
         		state.show_action_bar = true 
         	}else{
         		state.show_action_bar = false
         		state.delete_mode = false
         	} 
         },
         setDeleteMode: (state)=> {
         	state.delete_mode = true
         },
         clearDeleteMode: (state)=> {
         	state.delete_mode = false
         },
         clearShowActionBar: (state)=> {
		 	state.show_action_bar = false
         },
		 clearDeletePublishData: (state,action)=> {
		 	state.delete_publish_data = initialData
		 }
	}

})

export const {
	setDeletePublishData,
	setShowActionBar,
	clearDeletePublishData,
	removeDataById,
	clearShowActionBar,
	setDeleteMode,
	clearDeleteMode
} = DeletePublishActionBarSlice.actions
export default DeletePublishActionBarSlice.reducer