import { createSlice } from '@reduxjs/toolkit'
const initialActiveContentToSave = {
			resource_type: '',
			content_type: '',
			data: null,
			active_form_overlay: false,
			show_overlay_cancel: true,
			show_form_save_button: false
		}
const initalResourceContentType = {
			index_value: 0,
			name: "learning_content"
		}

const initialResourceParentData = {
			subject_id:'',
			topic_id:'',
			subtopic_id:'',
			classs_id: ''
		}

export const ResourceFormIndexSlice = createSlice({
	name:"ResourceFormIndex",
	initialState: {
		submit_form_error: false,
		active_content_to_save: initialActiveContentToSave,
		resource_content_id: null,
		resource_form_open_status: false,
		resource_content_type:initalResourceContentType,
		resource_parent_data:initialResourceParentData,
		form_saving_status: false
	},
	reducers: {
		setFormSubmitError: (state)=> {
			state.submit_form_error = true
		},
		clearFormSumitError: (state)=> {
			state.submit_form_error = false
		},
        setResourceContentType: (state,action)=> {
        	state.resource_content_type = {
        		index_value: action.payload.index_value,
        		name: action.payload.name
        	}
        },
        setResourceContentId: (state, action)=> {
        	state.resource_content_id = action.payload.resource_content_id
        },
        setResourceParentData: (state,action)=> {
        	state.resource_parent_data = {
        		subject_id: action.payload.subject_id,
        		topic_id: action.payload.topic_id,
        		subtopic_id: action.payload.subtopic_id,
        		classs_id: action.payload.classs_id
        	}
        },
        setResourceFormOpenStatus: (state,action)=> {
        	state.resource_form_open_status =  !state.resource_form_open_status
        },
        clearAllFromResourceIndex: (state) => {
        	state.resource_content_type = initalResourceContentType
        	state.resource_parent_data = initialResourceParentData
        	state.active_content_to_save = initialActiveContentToSave
        	state.resource_content_id = null
        },
        setActiveContentToSave: (state, action)=> {
        	state.active_content_to_save = {
        		resource_type: action.payload.resource_type,
        		content_type: action.payload.content_type,
        		active_form_overlay: action.payload.active_form_overlay,
        		show_form_save_button: action.payload.show_form_save_button,
        		data: action.payload.data,
        		show_overlay_cancel: false
        	}
        },
        setActiveFormInnerLayout: (state)=> {
        	state.active_content_to_save = {
        		...state.active_content_to_save ,
        		active_form_overlay: true,
        		show_overlay_cancel: false
        	}
        },
        clearActiveContentToSave: (state)=> {
        	state.active_content_to_save = initialActiveContentToSave
        },
        cancelActiveFormOverlay: (state)=> {
        	state.active_content_to_save = { 
        		...state.active_content_to_save,
        		active_form_overlay: false,
        		show_form_save_button: false
        	}
        },
        setFormSaveStatus: (state,action)=> {
        	state.form_saving_status = true
        },
        clearFormSaveStatus: (state)=> {
        	state.form_saving_status = false
        }
	}
})

export const { 
	setResourceContentType,
	setResourceFormOpenStatus,
	setResourceContentId,
	clearAllFromResourceIndex,
	setActiveContentToSave,
	clearActiveContentToSave,
	cancelActiveFormOverlay,
	setActiveFormInnerLayout,
	setFormSaveStatus,
	clearFormSaveStatus,
	setFormSubmitError,
	clearFormSumitError,
	setResourceParentData }  = ResourceFormIndexSlice.actions
export default ResourceFormIndexSlice.reducer