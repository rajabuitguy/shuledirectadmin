import { createSlice } from '@reduxjs/toolkit'

// Custom
import { SettingdArray } from './SettingsData'
// eNDcUSTOM


const initialResourcesDetails = {
	game_resource_type: "image_game_resource",
	data: {
			id:"",
			unique_key_code:"",
			subject_id:'',
			classs_id:'',
			topic_id:'',
			subtopic_id:'',
			name:"",
			baseId: '',
		  	sourceId:'',
			indicator:"create",
			cover_image:[
			    {
				   	id:"", 
				   	url:"", 
					file_type:"", 
					file_name:"", 
					media_types:"",
					file_extension:"",
					file_size:"",
					mapping_id:"",
					parent_id:"",
					dest:"file_contents"
			    }
			]
		}
}



const initalNumberRange = [
	{
		block_info:{ id:"" },
		data:{ 
			 id:"",
			 min:"",
			 max:"",
			 range_type:"",
			 unlock_points:0,
			 content:"" 
		},
		cover_image:[
			{ 
				id:"",
				file_name:"",
				file_type:"", 
				url:"", 
				media_types:"",
				file_extension:"",
				file_size:"",
				mapping_id:"", 
				parent_id:"",
			  dest:"file_contents"
			}
		],
		settings_data: SettingdArray
	}
]

const initOptionProperties = [
	{
		content:{ name:"" },
		answer:{
			id:"",
			url:"", 
			file_type:"", 
			file_name:"", 
			media_types:"",
			file_extension:"",
			file_size:"",
			mapping_id:"",
			parent_id:"",
			dest:"file_contents"
		},
		audio: {
			id:"",
			url:"", 
			file_type:"", 
			file_name:"", 
			media_types:"",
			file_extension:"",
			file_size:"",
			mapping_id:"",
			parent_id:"",
			dest:"file_contents"
		}
	}
]

const initialResourceQuestionsBlock  = [
	{ 
		block_info:{ id:"" },
		options_array:[],
		settings_data: SettingdArray
	}
]

 const numberRangeObject = initalNumberRange[0] 

export const GameResourcesSlice = createSlice({
	name:"GameResources",
	initialState:{
		details_data:initialResourcesDetails,
		number_range: initalNumberRange,
		resource_steps_block: initialResourceQuestionsBlock,
		option_properties_data: {
			parent_data:  {
				block_index: "",
				option_index: "",
				option_type: ""
			},
			data: initOptionProperties,
			open_properties_modal: false,
			save_properties: false
		}
	},
	reducers: {
		setResourceDetails: (state,action)=> {
			state.details_data = { ...state.details_data, ...action.payload }
		},
		setNumberRangeForm: (state,action)=> {
			state.number_range = action.payload
		},
		setResourceStepsBlock: (state, action)=> {
			 state.resource_steps_block = action.payload
		},
		setResourceStepsOptionBlock: (state, action)=> {

			 let dataArray = action.payload && action.payload.map(data => {

			     let _settingdArray = [];
				 		 if (data.setting_data && data.setting_data.length > 0){
						 		 
						 		 SettingdArray.forEach((_data)=> {
			               data.setting_data.forEach(__data=> {
			               	   if (__data.slug===_data.slug) _settingdArray.push(__data)
			               })

			               let _result = _settingdArray.some(el=> el.slug===_data.slug)
			               if (!_result) _settingdArray.push(_data)
						 		 })
				 		 }

			 	   return {
			 	   	   	block_info: data.block_info,
								options_array: (typeof data.options_array==="object") ? data.options_array : [],
								settings_data: (_settingdArray && _settingdArray.length > 0) ? _settingdArray : SettingdArray
			 	   }
			 })

			 state.resource_steps_block = dataArray
		},
		setNumberUpdateData: (state,action)=> {
			let dataArray = action.payload && action.payload.map(data => {

			     let _settingdArray = [];
				 		 if (data.setting_data && data.setting_data.length > 0){
						 		 
						 		 SettingdArray.forEach((_data)=> {
			               data.setting_data.forEach(__data=> {
			               	   if (__data.slug===_data.slug) _settingdArray.push(__data)
			               })

			               let _result = _settingdArray.some(el=> el.slug===_data.slug)
			               if (!_result) _settingdArray.push(_data)
						 		 })
				 		 }

				 		 return {
								block_info:data.block_info,
								data:data.data,
								cover_image: data.cover_image,
								settings_data: (_settingdArray && _settingdArray.length > 0) ? _settingdArray : SettingdArray
							}
			 })

			 state.number_range = dataArray
		},
		addNewStepBlock: (state) => {
			state.resource_steps_block = [...state.resource_steps_block,{
				block_info: { id: ''},
				options_array:[],
				settings_data:SettingdArray
			}]
		},
		addNewNumberRangeForm: (state,action)=> {
			 state.number_range = [...state.number_range,numberRangeObject]
		},
		addNewStepsOptionBlock: (state, action)=> {
			state.resource_steps_block = [...state.resource_steps_block]
			state.resource_steps_block[action.payload] = {
				block_info: {...state.resource_steps_block[action.payload].block_info },
				settings_data: [...state.resource_steps_block[action.payload].settings_data ],
				options_array: [...state.resource_steps_block[action.payload].options_array,{
					    content:{ name:"" },
				    	question:{
					    	id:"",
					    	url:"", 
					    	file_type:"", 
					    	file_name:"", 
					    	media_types:"",
					    	file_extension:"",
					    	file_size:"",
					    	mapping_id:"",
					    	parent_id:"",
					    	dest:"file_contents",
							properties: []
				    	},
				    	answer:{
				    		id:"",
				    		url:"", 
				    		file_type:"", 
				    		file_name:"", 
				    		media_types:"",
				    		file_extension:"",
				    		file_size:"",
				    		mapping_id:"",
					    	parent_id:"",
					    	dest:"file_contents",
							properties: []
				    	},
				    	audio: {
				    		id:"",
				    		url:"", 
				    		file_type:"", 
				    		file_name:"", 
				    		media_types:"",
				    		file_extension:"",
				    		file_size:"",
				    		mapping_id:"",
					    	parent_id:"",
					    	dest:"file_contents",
							properties: []
				    	}
				}]
			}
		},
		updateGamesDetailsCoverImage: (state,action) => {
			state.details_data = {...state.details_data }
			state.details_data.data.cover_image[action.payload.index] = {
				...state.details_data.data.cover_image[action.payload.index],
				id:action.payload.media.id,
				file_name:action.payload.media.file_name,
				url: action.payload.media.url,
				file_type:action.payload.media.file_type,
				media_types:action.payload.media.media_types,
				file_extension:action.payload.media.file_extension,
				file_size:action.payload.media.file_size,
			}
		},
		updateNumberRangeCover: (state,action) =>{
			state.number_range = [...state.number_range ]
			state.number_range[action.payload.index.parent_index].cover_image[action.payload.index.child_index] = {
				...state.number_range[action.payload.index.parent_index].cover_image[action.payload.index.child_index],
				id:action.payload.media.id,
				file_name:action.payload.media.file_name,
				url: action.payload.media.url,
				file_type:action.payload.media.file_type,
				media_types:action.payload.media.media_types,
				file_extension:action.payload.media.file_extension,
				file_size:action.payload.media.file_size
			}
		},
		updateNumberRangeData: (state,action)=> {
			state.number_range = [...state.number_range ]
			state.number_range[action.payload.index] = {
				data: {
					id: action.payload.id,
					min: action.payload.min,
					max: action.payload.max,
					range_type: action.payload.range_type,
					unlock_points: action.payload.unlock_points,
					content: action.payload.content,
				},
				...state.number_range[action.payload.parent_index].block_info,
				...state.number_range[action.payload.parent_index].cover_image,
				...state.number_range[action.parent_index].settings_data,
			}
		},
		updateGameSettingMedia: (state, action)=> {
			state.number_range = [...state.number_range ]
			state.number_range[action.payload.index.parent_index].settings_data[action.payload.index.child_index] = {
				...state.number_range[action.payload.index.parent_index].settings_data[action.payload.index.child_index],
				media:{
					...state.number_range[action.payload.index.parent_index].settings_data[action.payload.index.child_index].media,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size
				}
			}
		},
		updateResourceStepsAnswerBlock: (state,action)=> {
			state.resource_steps_block = [...state.resource_steps_block ]
			state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index] = {
				content:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].content },
				question:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].question },
				audio:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].audio },
				answer:{
					...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].answer,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size
				}
			}
		},
		updateResourceStepsQuestionsBlock: (state,action)=> {
			state.resource_steps_block = [...state.resource_steps_block ]
			state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index] = {
				content:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].content },
				answer:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].answer },
				audio:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].audio },
				question:{
					...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].question,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size
				}
			}
		},
		updateResourceStepsAudioBlock: (state,action)=> {
			state.resource_steps_block = [...state.resource_steps_block ]
			state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index] = {
				content:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].content },
				answer:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].answer },
				question:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].question },
				audio:{
					...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].audio,
					id:action.payload.media.id,
					file_name:action.payload.media.file_name,
					url: action.payload.media.url,
					file_type:action.payload.media.file_type,
					media_types:action.payload.media.media_types,
					file_extension:action.payload.media.file_extension,
					file_size:action.payload.media.file_size
				}
			}
		},
		updateResourceStepsTextBlock: (state,action)=> {
			state.resource_steps_block = [...state.resource_steps_block ]
			state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index] = {
				audio:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].audio },
				answer:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].answer },
				question:{ ...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].question },
				content:{
					...state.resource_steps_block[action.payload.index.parent_index].options_array[action.payload.index.child_index].content,
					name:action.payload.index.name
				}
			}
		},
		updateResourceStepsSettings: (state,action) =>{
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.index.parent_index].settings_data[action.payload.index.child_index] = {
					...state.resource_steps_block[action.payload.index.parent_index].settings_data[action.payload.index.child_index],
					media:{
						...state.resource_steps_block[action.payload.index.parent_index].settings_data[action.payload.index.child_index].media,
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
		},
		removeStepBlock: (state,action)=> {
			state.resource_steps_block = [...state.resource_steps_block]
	    	state.resource_steps_block.splice(action.payload, 1)
		},
		removeStepBlockOptionsCard: (state,action)=> {
			let newQuestionBlockValues = [...state.resource_steps_block]
			let _newQuestionBlockValues = [...newQuestionBlockValues[action.payload.parent_index].options_array]
		    _newQuestionBlockValues.splice(action.payload.child_index, 1)
			
			newQuestionBlockValues[action.payload.parent_index] = {
				block_info: {...newQuestionBlockValues[action.payload.parent_index].block_info },
				settings_data: [ ...newQuestionBlockValues[action.payload.parent_index].settings_data ],
				options_array: _newQuestionBlockValues
			}

			state.resource_steps_block = newQuestionBlockValues
		},
		updateResourceOptionProperties: (state,action)=>{
				if (action.payload.index.option_type==="question" && action.payload.index.property_source==="answer"){
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].question.properties[action.payload.index.child_index] = {
					...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].question.properties[action.payload.index.child_index],
					answer:{
						...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].question.properties[action.payload.index.child_index].answer,
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
			}else if (action.payload.index.option_type==="question" && action.payload.index.property_source==="audio"){
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].question.properties[action.payload.index.child_index] = {
					...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].question.properties[action.payload.index.child_index],
					audio:{
						...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].question.properties[action.payload.index.child_index].audio,
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
			}else if(action.payload.index.option_type==="answer" && action.payload.index.property_source==="answer"){
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].answer.properties[action.payload.index.child_index] = {
					...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].answer.properties[action.payload.index.child_index],
					answer:{
						...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].answer.properties[action.payload.index.child_index].answer,
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
			}else if (action.payload.index.option_type==="answer" && action.payload.index.property_source==="audio"){
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].answer.properties[action.payload.index.child_index] = {
					...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].answer.properties[action.payload.index.child_index],
					audio:{
						...state.resource_steps_block[action.payload.index.block_index].options_array[action.payload.index.option_index].answer.properties[action.payload.index.child_index].audio,
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
			}

			//update properties
			if (action.payload.index.property_source==="answer"){
				state.option_properties_data.data[action.payload.index.child_index] = {
					...state.option_properties_data.data[action.payload.index.child_index],
					answer: {
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
			}else if (action.payload.index.property_source==="audio"){
				state.option_properties_data.data[action.payload.index.child_index] = {
					...state.option_properties_data.data[action.payload.index.child_index],
					audio: {
						id:action.payload.media.id,
						file_name:action.payload.media.file_name,
						url: action.payload.media.url,
						file_type:action.payload.media.file_type,
						media_types:action.payload.media.media_types,
						file_extension:action.payload.media.file_extension,
						file_size:action.payload.media.file_size
					}
				}
			}
		},
		setInitialOptionProperties: (state,action)=> {
			let propertiesData = ""
			if (action.payload.option_type==="question"){
				propertiesData = (action.payload.selected_option.question.properties.length > 0 ) ? action.payload.selected_option.question.properties : []
			}else if (action.payload.option_type==="answer"){
				propertiesData = (action.payload.selected_option.answer.properties.length > 0 ) ? action.payload.selected_option.answer.properties : []
			}else{
				return false
			}

			state.option_properties_data = { 
				...state.option_properties_data, 
				parent_data: {
					block_index: action.payload.index.parent_index,
					option_index: action.payload.index.child_index,
					option_type: action.payload.option_type,
				},
				data:propertiesData 
			}
		},
		addResourceNewProperties: (state,action)=> {

			//add new resource option properties
			state.option_properties_data = {...state.option_properties_data, data: [...state.option_properties_data.data,initOptionProperties[0] ] }

			//Add properties into resource block options
			if (action.payload.option_type==="question"){
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index] = {
					...state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index],
					question: { 
						...state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index].question,
						properties: [...state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index].question.properties,initOptionProperties[0] ]
					}
				}
			}else if (action.payload.option_type==="answer"){
				state.resource_steps_block = [...state.resource_steps_block ]
				state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index] = {
					...state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index],
					answer: { 
						...state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index].answer,
						properties: [...state.resource_steps_block[action.payload.block_index].options_array[action.payload.option_index].answer.properties,initOptionProperties[0] ]
					}
				}
			}
		},
		removeResourcePropertyCard: (state,action)=> {

			//remove from resource properties
			let newQuestionBlockValues = {...state.option_properties_data }
			let _newQuestionBlockValues = [...newQuestionBlockValues.data ]
		    _newQuestionBlockValues.splice(action.payload.child_index, 1)
			newQuestionBlockValues = {
				...newQuestionBlockValues,
				data: _newQuestionBlockValues
			}
			state.option_properties_data = newQuestionBlockValues

			//remove from resource block
			let newQuestionBlockValues_ = [...state.resource_steps_block]

			if (action.payload.option_type==="question"){
				let _newQuestionBlockValues_ = [...newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index].question.properties ]
				_newQuestionBlockValues_.splice(action.payload.child_index, 1)
				
				newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index] = {
					...newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index],
					question: {
						...newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index].question,
						properties: _newQuestionBlockValues_
					}
				}
			}else if(action.payload.option_type==="answer"){
				let _newQuestionBlockValues_ = [...newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index].answer.properties ]
				_newQuestionBlockValues_.splice(action.payload.child_index, 1)
				
				newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index] = {
					...newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index],
					answer: {
						...newQuestionBlockValues_[action.payload.block_index].options_array[action.payload.option_index].answer,
						properties: _newQuestionBlockValues_
					}
				}
			}

			state.resource_steps_block = newQuestionBlockValues_

		},
		setOpenOptionsPropertiesModal: (state, action)=> {
			state.option_properties_data = { ...state.option_properties_data,open_properties_modal: !state.option_properties_data.open_properties_modal }
		},
		setSaveResourceProperties: (state, action)=> {
			state.option_properties_data = { ...state.option_properties_data,save_properties: !state.option_properties_data.save_properties }
		},
		removeNumberRangeForm: (state,action)=> {
			state.number_range = [...state.number_range]
	      	state.number_range.splice(action.payload, 1)
		},
		clearImageResourceForm: (state)=> {
			state.resource_steps_block = initialResourceQuestionsBlock
		},
		clearNumberRangeForm: (state)=> {
			state.number_range = initalNumberRange
		},
		clearResourceDetails: (state)=> {
			state.details_data = initialResourcesDetails
		}

	}
})

export const {
  setResourceDetails,
  addNewStepBlock,
  addNewStepsOptionBlock,
  updateNumberRangeData,
  updateNumberRangeCover,
  updateGameSettingMedia,
  updateResourceStepsQuestionsBlock,
  updateResourceStepsAnswerBlock,
  removeStepBlock,
  removeStepBlockOptionsCard,
  clearNumberRangeForm,
  clearImageResourceForm,
  clearResourceDetails, 
  setNumberRangeForm,
  updateGamesDetailsCoverImage,
  updateResourceStepsAudioBlock,
  updateResourceStepsSettings,
  setResourceStepsOptionBlock,
  setNumberUpdateData,
  updateResourceStepsTextBlock, 
  setResourceStepsBlock,
  addNewNumberRangeForm,
  removeNumberRangeForm,
  setOpenOptionsPropertiesModal,
  setInitialOptionProperties,
  updateResourceOptionProperties,
  addResourceNewProperties,
  removeResourcePropertyCard,
  setSaveResourceProperties 
} = GameResourcesSlice.actions
export default GameResourcesSlice.reducer