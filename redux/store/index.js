
import { configureStore } from '@reduxjs/toolkit'
import LearningContentReducer from '../reducers/primary/LearningContentSlice'
import GameResourcesReducer from '../reducers/primary/GameResourcesSlice'
import MediaGalleryReducer from '../reducers/MediaGallerySlice'
import ResourceFormIndexReducer from '../reducers/primary/ResourceFormIndexSlice'
import DeletePublishActionBarSlice from '../reducers/primary/DeletePublishActionBarSlice'
import LanaguagesSlice from '../reducers/LanaguagesSlice'

export default configureStore({
  	reducer: {
  		learning_content: LearningContentReducer,
  		game_resources: GameResourcesReducer, 
  		media_gallery: MediaGalleryReducer,
  		resource_form_index: ResourceFormIndexReducer,
  		delete_publish_action: DeletePublishActionBarSlice,
  		language:LanaguagesSlice
  	}
})